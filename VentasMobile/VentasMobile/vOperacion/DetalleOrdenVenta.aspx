﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetalleOrdenVenta.aspx.cs" Inherits="VentasMobile.vOperacion.DetalleOrdenVenta" %>
<asp:Content ID="DetalleCarrito" ContentPlaceHolderID="MainContent" runat="server">      

    <div  id="ModalMensaje" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered " role="document">
            <div id="DivMensaje" class="modal-content">      
            </div>
          </div>
      </div>
    <asp:UpdatePanel ID="uxUpdatePanelTabla" runat="server">
        <ContentTemplate>
            <div class="bg-white card p-3 shadow container-fluid w-carrito">               
                <div class="row">
                    <div class="col-9">
                        <h2 class="text-center mb-3 mt-3">Detalle Orden de Venta</h2>                        
                    </div>
                    <div class="col-3 text-right">
                        <asp:ImageButton ID="btnImSeguirComprando" runat="server" OnClick="btnSeguirComprando_Click" class="img-fluid btn w-85" CausesValidation="False" ImageUrl="~/images/icon-cart/carrito.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p class="font-weight-bold mb-0"><i class="fa fa-user-circle"></i> Cliente:</p>
                        <p class="text-muted">
                            <span>#</span>
                            <asp:Label ID="lblNumeroCliente" runat="server" ></asp:Label>
                            <i class="fa fa-arrow-right"></i>
                            <asp:Label ID="lblNombreCliente" runat="server"  ></asp:Label>
                        </p>
                        <p class="font-weight-bold mb-0"><i class="fa fa-address-card"></i> Domicilio embarque:</p>
                        <p class="text-muted">
                            <asp:Label ID="lblDomicilioEmbarque" runat="server"  ></asp:Label>
                        </p>
                    </div>
                    <div class="col-12 border-top pt-3">
                        <div class="row">
                            <div class="col-6">
                                <p class="font-weight-bold mb-0"> Lista de precios</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblListaPrecio" runat="server"  ></asp:Label>
                                </p>
                            </div>
                            <div class="col-6">
                                <p class="font-weight-bold mb-0"> Limite de crédito</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblLimiteCredito" Text="0" runat="server" ></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <p class="font-weight-bold mb-0"> Crédito disponible</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblCreditoDisponible" Text="0" runat="server" ></asp:Label>
                                </p>
                            </div>
                            <div class="col-6">
                                <p class="font-weight-bold mb-0"> Total pedido:</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblTotalPedido" Text="0" runat="server" ></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-right">
                        <asp:ImageButton ID="btnImgLipiarCarrito" runat="server" CssClass="eliminar img-fluid w-22 btn btn-ligth" ImageUrl="~/images/icon-cart/vaciar_carrito.png" OnClick="uxButtonLimpiarCarrito_Click" CausesValidation="False" />
                    </div>
                </div>
                <div class="contanier">
                    <div class="table-responsive">
                        <asp:GridView HorizontalAlign="Center" runat="server" ID="gvCaritoCompras" AutoGenerateColumns="false"
                            EmptyDataText="No hay nada en su carrito de compras."
                            GridLines="None" CssClass="table" CellPadding="5" ShowFooter="true" DataKeyNames="IdProducto"
                            OnRowDataBound="gvCaritoCompras_RowDataBound" OnSelectedIndexChanged="gvCaritoCompras_SelectedIndexChanged">
                            <HeaderStyle HorizontalAlign="Left" BackColor="Gray" ForeColor="AliceBlue" />
                            <FooterStyle HorizontalAlign="Right" BackColor="Gray" ForeColor="White" />
                            <AlternatingRowStyle BackColor="Azure" />
                            <Columns>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
                                <asp:TemplateField HeaderText="Cantidad">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtCantidad" Columns="5" Text='<%# Eval("Cantidad") %>' TextMode="Number" Width="60px" Height="20px" OnTextChanged="txtCantidad_TextChanged" AutoPostBack ="true" ></asp:TextBox><br />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PrecioUnitario" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                    HeaderStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Total" HeaderText="Total $" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Right"  />
                                <asp:ButtonField CommandName="Select" ControlStyle-CssClass="fas fa-trash-alt fa-lg " ControlStyle-ForeColor="Black"/>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p class=" font-weight-bold"> Orden de compra del cliente (OC):</p>
                       
                    </div>
                    <div class="col-6">
                        <asp:TextBox ID="txtCodigoCliente" runat="server" class="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtCodigoCliente" runat="server" 
                                ErrorMessage="*Campo requerido" ControlToValidate="txtCodigoCliente" CssClass="error invalid-feedback required-label campo-requerido" 
                                SetFocusOnError="True" ></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-6">
                        <%--<p class="text-bb font-weight-bold"> Orden para consigna:</p>--%>
                    </div>
                    <div class="col-6">
                        <asp:CheckBox ID="cboxConsigna" runat="server" Visible="false" />
                    </div>
                    <div class="col-6">
                        <p class="font-weight-bold"> Fecha requerida del cliente:</p>
                    </div>
                    <div class="col-6">
                        <asp:TextBox ID="txtNeedBy" TextMode="Date" runat="server" class="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtNeedBy" runat="server" 
                                ErrorMessage="*Campo requerido" ControlToValidate="txtNeedBy" CssClass="error invalid-feedback required-label campo-requerido" 
                                SetFocusOnError="True" ></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-6">
                        <p class="font-weight-bold"> Fecha de embarque:</p>
                    </div>
                    <div class="col-6">
                        <asp:TextBox ID="txtshipBy" TextMode="Date" runat="server"  class="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtshipBy" runat="server" 
                                ErrorMessage="*Campo requerido" ControlToValidate="txtshipBy" CssClass="error invalid-feedback required-label campo-requerido" 
                                SetFocusOnError="True" ></asp:RequiredFieldValidator>
                    </div>
                      <div class="col-6">
                        <p class="font-weight-bold"> Hora de embarque:</p>
                    </div>
                    <div class="col-6">    
                        <asp:TextBox ID="txtshipByTime"  runat="server" TextMode="Time"  class="form-control"></asp:TextBox>   
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtshipByTime" runat="server" 
                                ErrorMessage="*Campo requerido" ControlToValidate="txtshipByTime" CssClass="error invalid-feedback required-label campo-requerido" 
                                SetFocusOnError="True" ></asp:RequiredFieldValidator>                      
                          
                   
                    </div>
                    <div class="col-6">
                        <p class="font-weight-bold"> Comentario:</p>
                    </div>
                    <div class="col-6">
                            <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine" class="form-control" Height="96px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorComentarioOrden" runat="server" 
                                        ErrorMessage="*Campo requerido" ControlToValidate="txtComentario" CssClass="error invalid-feedback required-label campo-requerido" 
                                        SetFocusOnError="True" ></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-6">
                        <p class="font-weight-bold">Permitir que el monto de la orden exceda el credito del cliente(solo para cotización): 
                         <asp:CheckBox ID="uxCheckBoxCotizacion" runat="server" Width="110px" OnCheckedChanged="uxCheckBoxCotizacion_CheckedChanged" AutoPostBack="True" /></p>
                    </div>
                    <div class="col-6">
                    </div>

                    <div class="col-6 text-right mt-3">                        
                        <asp:ImageButton ID="btnImgFinalizarCotiza" runat="server" CssClass="eliminar img-fluid w-30 mr-5"  ImageUrl="~/images/icon-cart/cotizacion.png" OnClick="BtnFinalizarCotiza_Click"   />
                    </div>                    
                    <div class="col-6 text-left mt-3">
                        <asp:ImageButton ID="btnImgFinalizarOrden" runat="server" CssClass="eliminar img-fluid w-35 ml-5 icono-ordenventa" ImageUrl="~/images/icon-cart/ordendeventa.png"  OnClick="uxButtonFinalizarOrdenVenta_Click" />
                    </div>
                </div>
    </div>            
            <div id="ModalConfirmacionOrdenVenta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="ModalCenterTitle1">Mensaje</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="uxLabelFacturasAnteriores" 
                        CssClass="jqtransform sin_formato size_table" Text="¿Finalizar Orden de Venta?" Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="uxButtonConfirmacionOrdenVenta" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" OnClick="uxButtonConfirmacionOrdenVenta_Click"
                            OnClientClick="ModalConfirmacionOcultarOVC('ModalConfirmacionOrdenVenta')" />  
                  </div>
                </div>
              </div>
            </div>              
            <div id="ModalConfirmacionCotizacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="ModalCenterTitle2">Mensaje</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="Label1" 
                        CssClass="jqtransform sin_formato size_table" Text="¿Finalizar Cotización?" Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="uxButtonConfirmacionCotizacion" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" OnClick="uxButtonConfirmacionCotizacion_Click"
                            OnClientClick="ModalConfirmacionOcultarOVC('ModalConfirmacionCotizacion')" />  
                  </div>
                </div>
              </div>
            </div>            
            <div id="ModalConfirmacionLimpiarCarrito" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="ModalCenterTitle3">Mensaje</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="LabelMen" 
                        CssClass="jqtransform sin_formato size_table" Text="¿Vaciar carrito?" Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="ButtonVaciarC" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" OnClick="ButtonVaciarC_Click" 
                            OnClientClick="ModalConfirmacionOcultar()" />  
                  </div>
                </div>
              </div>
            </div>            
            <div id="ModalConfirmacionEliminarLinea" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="ModalCenterTitle4">Mensaje</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="Label2" 
                        CssClass="jqtransform sin_formato size_table" Text="¿Eliminar linea?" Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="uxButtonEliminarLinea" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" OnClick="uxButtonEliminarLinea_Click" 
                            OnClientClick="ModalConfirmacionOcultar()" />  
                  </div>
                </div>
              </div>
            </div>
            <div id="ModalConfirmacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="exampleModalCenterTitle">Mensaje</h5>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="Label3" 
                        CssClass="jqtransform sin_formato size_table" Text="¿Continuar? Se eliminarán los productos agregados." Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="uxButtonConfirmacionCotizacionCancelar" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white" runat="server" Text="Cancelar" 
                            OnClick="uxButtonConfirmacionCotizacionCancelar_Click" />
                        <asp:Button ID="uxButtonConfirmacionCotizacionC" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" 
                            OnClick="uxButtonConfirmacionCotizacionC_Click" />  
                  </div>
                </div>
              </div>
            </div> 
        </ContentTemplate>
             <Triggers>
                        <asp:ASyncPostBackTrigger ControlID="uxCheckBoxCotizacion" />
             </Triggers>
</asp:UpdatePanel>
</asp:Content>