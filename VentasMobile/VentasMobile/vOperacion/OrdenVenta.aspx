﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrdenVenta.aspx.cs" Inherits="VentasMobile.vOperacion.OrdenVenta" %>
<asp:Content ID="Inventario" ContentPlaceHolderID="MainContent" runat="server">     
   <div  id="ModalMensaje" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered " role="document">
        <div id="DivMensaje" class="modal-content">      
        </div>
      </div>
    </div>
   <asp:UpdatePanel ID="uxUpdatePanelFormulario" runat="server">
        <ContentTemplate>
            <div id="Inicio" class="container mb-5">
                <div id="BuscadorCliente" class="row">
                    <h4 class="mb-5 col-12"><i class="fas fa-address-card fa-lg mr-2"></i>Información de Clientes</h4>
                    <div class="col-dropbox">
                        <asp:Label ID="uxLabelGrupos" runat="server" Text="Grupo de Clientes: " Font-Bold="True"></asp:Label>
                        <asp:DropDownList ID="uxDropDownListGrupo" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListGrupo_SelectedIndexChanged" ></asp:DropDownList> 
                    </div>
                    <div class="col-dropbox">
                        <asp:Label ID="UxLabelClientes" runat="server" Text="Clientes: " Font-Bold="True"></asp:Label>
                        <asp:DropDownList ID="uxDropDownListClientes" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListClientes_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-dropbox">
                        <asp:Label ID="uxLabelDomicilios" runat="server" Text="Domicilios: " Font-Bold="True"></asp:Label>  
                        <asp:DropDownList ID="uxDropDownListDomicilio" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListDomicilio_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="col-buscarCliente">
                        <asp:Label ID="uxLabelCliente" runat="server" Text="No.Cliente: " Font-Bold="True"></asp:Label>  
                        <asp:TextBox ID="txtSearch" runat="server"  CssClass="form-control" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>
                    </div>
                    <div class="col-buscarClienteBoton">                     
                        <asp:Button ID="BtnSearch" runat="server" CausesValidation="false" Text="Buscar" class="btn btn-secondary btn-lg mt-3" OnClick="BtnSearch_Click" />
                        <asp:Button ID="ButtonCancelar" runat="server" CausesValidation="false" Text="Limpiar" class="btn btn-secondary btn-lg mt-3" OnClick="uxButtonCancelar_Click" />
                    </div>
                </div>
                <div class="row justify-content-center plr-8 mt-4">
                    <div class="card row w-labels mb-3">
                        <div class="card-body row">
                            <div class="form-group col-6 col-xl-3 text-left">
                                <asp:Label Font-Bold="true" ID="lblListaPRecioV" runat="server" Text="Lista de precios: " ></asp:Label><br/>
                                <asp:Label ID="lblListaPRecio" runat="server" ></asp:Label>           
                            </div>
                            <div class="form-group col-6 col-xl-3 text-left">
                                <asp:Label Font-Bold="true" ID="lblCredito" runat="server" Text="Limite de credito: " ></asp:Label><br/>
                                <asp:Label ID="lblLimiteCredito" Text="0" runat="server" ></asp:Label>
                            </div>
                            <div class="form-group col-6 col-xl-3 text-left">
                                <asp:Label Font-Bold="true" ID="lblDisponibl" runat="server" Text="Credito disponible: " ></asp:Label><br/>
                                <asp:Label ID="lblCreditoDisponible" Text="0" runat="server" Font-Bold="True" ></asp:Label>
                            </div>
                            <div class="form-group col-6 col-xl-3 text-left">
                                <asp:Label Font-Bold="True" ID="lblTotalP" runat="server" Text="Total pedido: " ></asp:Label><br/>
                                <asp:Label ID="lblTotalPedido" Text="0" runat="server" ></asp:Label>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>      
   <asp:UpdatePanel ID="UpdatePanelCategoria" runat="server">
        <ContentTemplate>
            <div class="container justify-content-center categoria-left">
                <asp:DataList ID="uxDataListCategoria" OnItemCommand="uxDataListCategoria_ItemCommand"
                    RepeatLayout="Flow" RepeatDirection="Vertical" DataKeyField="ClassID" HorizontalAlign="Center"  
                    runat="server" class="form-row w-form ">
                    <ItemTemplate>
                        <div id="divCategoria" class="card card-pedidos mb-4">
                            <asp:ImageButton ID="uxImageButtonCategoria" runat="server" CssClass="img-fluid img-product card-img"
                                ImageUrl='<%# Eval("FotoCategoria") %>' CommandName="Seleccionar" OnClientClick="goToDiv()"/>
                            <div class="card-img-overlay text-center p-0 f-12">
                                <asp:Label Visible="false" ID="lblClassID" runat="server" Text='<%# Bind("ClassID") %>'></asp:Label>
                                <asp:Label class="title-product text-white" ID="lblDescripcion" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList> 
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>      
   <asp:UpdatePanel ID="uxUpdatePanelProductos" runat="server">
        <ContentTemplate>
           <div id="buscarProductos"  class="container mt-4">
                <div class="row justify-content-center">
                    <div class="form-row w-form">
                          <div class="col-sm-12 col-md-6 col-lg-6">
                                <p class="font-weight-bold mb-0">Código Producto: </p>
                                <asp:TextBox ID="uxTextBoxCodigoProducto" runat="server" class="form-control"></asp:TextBox>
                            </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                                <p class="font-weight-bold mb-0">Código Barras: </p>
                                <asp:TextBox ID="uxTextBoxCodigoBarras" runat="server" class="form-control"></asp:TextBox>  
                           </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                                <p class="font-weight-bold mb-0">Descripción: </p>
                                <asp:TextBox ID="uxTextBoxDescripcion" runat="server" class="form-control" OnTextChanged="uxTextBoxDescripcion_TextChanged" ></asp:TextBox>
                                 </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                            <p class="font-weight-bold mb-0">Lista de precio: </p>
                                <asp:DropDownList ID="uxDropDownListaPrecio" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListaPrecio_SelectedIndexChanged">
                                    <asp:ListItem Value="0">Todos</asp:ListItem>
                                    <asp:ListItem Value="1">Existe en lista de precio</asp:ListItem>
                                    <asp:ListItem Value="2">No existe en lista de precio</asp:ListItem>
                                </asp:DropDownList>
                                 </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                                  <p class="font-weight-bold mb-0">Departamento: </p>
                                  <asp:DropDownList ID="uxDropDownListDepartamento" runat="server" class="form-control"  AutoPostBack=" true" OnSelectedIndexChanged="uxDropDownListDepartamento_SelectedIndexChanged">
                                  </asp:DropDownList>
                                 </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                                  <p  class="font-weight-bold mb-0">Marca: </p>
                                  <asp:DropDownList ID="uxDropDownListMarca" runat="server" class="form-control" AutoPostBack=" true" OnSelectedIndexChanged="uxDropDownListMarca_SelectedIndexChanged">
                                  </asp:DropDownList>
                             </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                                  <p  class="font-weight-bold mb-0">Linea: </p>
                                  <asp:DropDownList ID="uxDropDownListLinea" runat="server" class="form-control" AutoPostBack=" true" OnSelectedIndexChanged="uxDropDownListLinea_SelectedIndexChanged">
                                  </asp:DropDownList>
                          </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                                  <p  class="font-weight-bold mb-0">Familia: </p>
                                  <asp:DropDownList ID="uxDropDownListFamilia" runat="server" class="form-control" AutoPostBack=" true" OnSelectedIndexChanged="uxDropDownListFamilia_SelectedIndexChanged">
                                  </asp:DropDownList>
                              </div>
                      </div>  
                    <div class="form-group col-11 text-center  mt-3">
                        <asp:Button ID="uxButtonBuscarProducto" runat="server" Text="Buscar" OnClick="uxButtonBuscarProducto_Click"
                            class="btn btn-lg bg-gradient-bb text-white"/>
                        <asp:Button ID="uxButtonLimpiarFiltros" runat="server" CausesValidation="false" Text="Limpiar" class="btn btn-secondary  btn-lg" OnClick="uxButtonLimpiarFiltros_Click" />
                    </div>
                    <div class="row pull-left plr-8">
                        <div class="card row mb-3">
                            <div class="card-body row">
                                <div class="col-12 text-left">
                                    <asp:Label Font-Bold="true" ID="uxLabelCategoriaL" runat="server" Text="Categoría: " ></asp:Label>
                                    <asp:Label ID="uxLabelCategoriaV" runat="server" ></asp:Label>           
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div> 
           </div>
            <%--<div class="container">
                <div class="w-100">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">                                      
                            <%= GetPaginacion() %>
                        </ul>
                    </nav>
                </div>
            </div> --%>
           <div id="cardsProductos" class="container justify-content-center categoria-left">                
               <asp:DataList ID="uxDataLisProductos" OnItemCommand="uxDataLisProductos_ItemCommand" 
                   RepeatDirection="Vertical" DataKeyField="PartNum" RepeatLayout="Flow" 
                   HorizontalAlign="Center" runat="server" class="form-row w-form">        
                   <ItemTemplate>
                        <div  class="card shadow card-products">
                            <asp:Image ID="img" runat="server" ImageUrl='<%# Eval("Foto") %>' class="img-fluid"/>
                            <div class="card-body row f-12">
                                <p class="card-text font-weight-bold p-0 mb-0 col-12 text-center mt10">
                                    <i class="fa fa-dollar mr-1"></i>Precio: <br/>
                                    <asp:Label ID="lblBasePrice" runat="server" Text='<%# Bind("BasePrice") %>' class="d-block"></asp:Label></p>
                                <p class="card-text font-weight-bold p-0 mb-0 col-6 text-center mt10">Clave: <br/><asp:Label class="d-block mt-0" ID="lblPartNum" runat="server" Text='<%# Bind("PartNum") %>'></asp:Label></p>
                                <p class="card-text font-weight-bold p-0 mb-0 col-6 text-center mt10">C. Barras: <br/><asp:Label class="d-block mt-0" ID="lblCodigoaBarras" runat="server" Text='<%# Bind("ProdCode") %>'></asp:Label></p>
                                <p class="card-text font-weight-bold p-0 col-12 mb-0 border-top text-center mt10">Descripcion:<asp:Label ID="lblPartDescription" runat="server" Text='<%# Bind("PartDescription") %>'></asp:Label></p>
                                
                                <p class="card-text font-weight-bold p-0 col-6 text-center mt10 mb-0">Disponible <br/><asp:Label class="mt-0 d-block" ID="uxLabelDispo" runat="server" Text='<%# Bind("Disponible") %>'></asp:Label></p>
                                <p class="card-text font-weight-bold p-0 col-6 text-center mt10 mb-0">Cantidad <asp:TextBox runat="server" ID="txtCantidad" Columns="5" Text='<%# Eval("Cantidad") %>' TextMode="Number" class="form-control"></asp:TextBox></p>
                                <span id="spanBtnCarrito" class="btn bg-gradient-bb btn-lg btn-block h-15 mt10">
                                    <i class="fa fa-shopping-cart fa-lg i-btn-carrito-agregar" ></i> 
                                    <span class="span-btn-carrito-agregar" >Agregar</span>
                                    <asp:Button ID="uxButtonAgregarCarrito" runat="server"   CausesValidation="false" CommandName="Seleccionar"
                                    class="btn btn-carrito "  data-toggle="modal"/> 
                                </span>    
                            </div>         
                                <div id="msjProductoLista" >
                                    <div class=" text-center" role="alert"> 
                                <asp:Label ID="lblNoExisteEnLista" runat="server"  ForeColor="Red" Font-Bold="true"></asp:Label></div>
                                </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            <div class="container">
                <div class="w-100">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">                                      
                            <%= GetPaginacion() %>
                        </ul>
                    </nav>
                </div>
            </div>      
           </div>   
           <div id="ModalConfirmacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="exampleModalCenterTitle">Mensaje</h5>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="uxLabelFacturasAnteriores" 
                        CssClass="jqtransform sin_formato size_table" Text="El carrito de compras se eliminará ya que está a punto de cambiar la información del cliente." Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="uxButtonConfirmacionCambioClienteCancelar" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white" runat="server" Text="Cancelar" 
                            OnClick="uxButtonConfirmacionCambioClienteCancelar_Click" />
                        <asp:Button ID="uxButtonConfirmacionCambioCliente" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" 
                            OnClick="uxButtonConfirmacionCambioCliente_Click" />  
                  </div>
                </div>
              </div>
            </div> 
           <div id="uxModalValidaProductoExiste" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="exampleModalCenterTitle2">Mensaje</h5>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="uxLabelTituloMensaje" 
                        CssClass="jqtransform sin_formato size_table" Text="Ya agregó ese producto a la lista, puede cambiar la cantidad desde el detalle." Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="uxButtonCancelar" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white" runat="server" Text="Aceptar" 
                            OnClientClick="ModalConfirmacionOcultar()" />
                  </div>
                </div>
              </div>
            </div>
           <div id="centralModalSuccess" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true">
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                  <div class="modal-body">                    
                        <div class="text-center">
                          <i class="fas fa-check fa-4x mb-3 animated rotateIn agregado-carrito"></i>
                            <p class="heading lead">Agregado correctamente</p>
                        </div>
                      <asp:Button ID="uxButtonAgregarC" class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" 
                            OnClick="uxButtonAgregarC_Click" OnClientClick="ModalConfirmacionOcultar()"/>  
                  </div>
                </div>
              </div>
            </div>
           <asp:LinkButton ID="uxLinkButtonBotonArriba" runat="server"  OnClientClick="irArriba()" OnClick="uxLinkButtonBotonArriba_Click" class="ir-arriba fa-stack">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
            </asp:LinkButton>
           <asp:LinkButton ID="uxLinkButtonBotonInicioProductos" runat="server" OnClick="uxLinkButtonBotonInicioProductos_Click" OnClientClick="irBuscarProducto()" class="ir-productos fa-stack">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-search fa-stack-1x fa-inverse"></i>
            </asp:LinkButton>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>