﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="VentasMobile.vOperacion.Inicio" %>
<asp:Content ID="ContentInicio" ContentPlaceHolderID="MainContent2" runat="server">      
        <asp:UpdatePanel ID="uxUpdatePanelTablaFacturasHoy" runat="server">
            <ContentTemplate>                
                <div class="bg-default">
                    <div class="row justify-content-center">
                        <div class="">
                            <a href="OrdenVenta" class="text-decoration-none">
                                <div class="card text-bb mb-3 card-home-menu blc-transparent b-left" >
                                    <div class="card-body">
                                        <h6 class=" text-center text-black-50">Pedidos</h6>
                                        <img src="../images/png/icono_pedidos.png" class="img-fluid center-img tamañoIcono"/>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="">
                            <a href="../vCatalogo/Clientes" class="text-decoration-none">
                                <div class="card text-bb mb-3 card-home-menu">
                                    <div class="card-body">
                                        <h6 class=" text-center text-black-50 ">Clientes</h6>
                                        <img src="../images/png/clientes.png" class="img-fluid center-img tamañoIcono"/>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="">
                            <a href="../vCatalogo/Inventario" class="text-decoration-none">
                                <div class="card text-bb mb-3 card-home-menu">
                                    <div class="card-body">
                                        <h6 class=" text-center text-black-50">Inventario</h6>
                                        <img src="../images/png/icono_inventario.png" class="img-fluid center-img tamañoIcono"/>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="">
                            <a href="../vCatalogo/Reportes" class="text-decoration-none">
                                <div class="card text-bb mb-3 card-home-menu">
                                    <div class="card-body">
                                        <h6 class=" text-center text-black-50">Reportes</h6>
                                        <img src="../images/png/icono_reportes.png" width="76" height="92" class="img-fluid center-img tamañoIcono"/>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <br />     
                    <div class="embed-responsive embed-responsive-16by9" >
                        <%-- <iframe class="embed-responsive-item" id="iframeReportes" name="iframeReportes" runat="server" allowfullscreen></iframe>--%>
                    </div>
            </ContentTemplate>
       </asp:UpdatePanel>
</asp:Content>
       
