﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cNegocio;
using VentasMobile.cEntidad;
using System.Data;
using System.IO;
using System.Web.Configuration;
using System.Drawing;
using System.Web.Services;
using System.Globalization;


namespace VentasMobile.vOperacion
{
    public partial class OrdenVenta : System.Web.UI.Page
    {
     
        string strPaginacion = string.Empty;
        private enumAccion _accion = enumAccion.Ninguno;
        private enum enumAccion { Ninguno = 0, CambioGrupo = 1, CambioCliente = 2, CambioDomicilio = 3 };
        private DataTable uxTabla = new DataTable();
        public List<ProductosAlcarro> ListaP { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            string company = "";
            if (Session["CurComp"] != null)
                company = this.Session["CurComp"].ToString();
            if (!this.IsPostBack)
            {
                try
                {
                    string planta = this.Session["PlantID"].ToString();// 02082019
                    string user = this.Session["DcdUserID"].ToString();
                    string categoria = "";
                    string depto = "";
                    string marca = "";
                    string linea = "";
                    string familia = "";
                    CargarComboGrupos();
                    llenarcategorias();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Convert.ToString(Session["Categorias"]);
                        string PlantID = this.Session["PlantID"].ToString();// 02082019
                        csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                        DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                        if (catego.Rows.Count > 0)
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                           
                        }
                        else
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = "No se encontro categoría.";
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                    }
                    if (Session["Grupo"] != null)
                    {
                        uxDropDownListGrupo.SelectedValue = Convert.ToString(Session["Grupo"]);
                        CargarComboClientes();
                        if (Session["Cliente"] != null)
                        {
                            uxDropDownListClientes.SelectedValue = Convert.ToString(Session["Cliente"]);

                            CargarSaldosDisponible();

                            CargarComboDomicilios();
                            if (Session["Domicilio"] != null)
                            {
                                uxDropDownListDomicilio.SelectedValue = Convert.ToString(Session["Domicilio"]);
                                llenarDepartamentos();
                                if (Session["Categorias"] != null)
                                {
                                    categoria = Convert.ToString(Session["Categorias"]);
                                }
                            }
                        }
                        if (Session["ListaPrecios"] != null)
                            lblListaPRecio.Text = Session["ListaPrecios"].ToString();

                        if (Session["FiltroExisteEnListaPrecio"] != null)
                        {
                            uxDropDownListaPrecio.SelectedValue = Session["FiltroExisteEnListaPrecio"].ToString();
                            cargarProductosListaPrecio();
                        }
                        else
                        {
                            DataTable dt = new DataTable();
                            DataSet ds = new DataSet();
                            if (Session["Departamento"] != null)
                            {
                                uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                                depto = Convert.ToString(Session["Departamento"]);
                                Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelDepartamentoV");
                                uxLabelCategoriaVM.Text = uxDropDownListDepartamento.SelectedItem.Text;
                                Label uxLabelDepaL = (Label)this.Master.FindControl("uxLabelDepartamentoL");
                                uxLabelDepaL.Visible = true;
                            }                           
                            if (Session["Marca"] != null)
                            {
                                uxDropDownListMarca.SelectedValue = Convert.ToString(Session["Marca"]);
                                marca = Session["Marca"].ToString();
                                Label uxlabelMarcaVM = (Label)this.Master.FindControl("uxLabelMarcaV");
                                uxlabelMarcaVM.Text = uxDropDownListMarca.SelectedItem.Text;
                                Label uxLabelMarcaL = (Label)this.Master.FindControl("uxLabelMarcaL");
                                uxLabelMarcaL.Visible = true;
                            }
                            //else
                            //{
                            //    Label uxlabelMarcaVM = (Label)this.Master.FindControl("uxlabelMarcaV");
                            //    uxlabelMarcaVM.Text = "No se encontro Marca.";
                            //    Label uxlabelMarcaL = (Label)this.Master.FindControl("uxlabelMarcaL");
                            //    uxlabelMarcaL.Visible = true;
                            //}

                            if (Session["Linea"] != null)
                            {
                                uxDropDownListLinea.SelectedValue = Convert.ToString(Session["Marca"]);
                                linea = Session["Linea"].ToString();
                                Label uxlabelLineaVM = (Label)this.Master.FindControl("uxLabelLineaV");
                                uxlabelLineaVM.Text = uxDropDownListLinea.SelectedItem.Text;
                                Label uxLabelLineaL = (Label)this.Master.FindControl("uxLabelLIneaL");
                                uxLabelLineaL.Visible = true;
                            }
                            //else
                            //{
                            //    Label uxlabelLineaVM = (Label)this.Master.FindControl("uxlabelLineaV");
                            //    uxlabelLineaVM.Text = "No se encontro linea.";
                            //    Label uxlabelLineaL = (Label)this.Master.FindControl("uxlabelLineaL");
                            //    uxlabelLineaL.Visible = true;
                            //}
                            if (Session["Familia"] != null)
                            {
                                uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                                familia = Session["Familia"].ToString();
                                Label uxlabelFamiliaVM = (Label)this.Master.FindControl("uxlabelFamiliaV");
                                uxlabelFamiliaVM.Text = uxDropDownListFamilia.SelectedItem.Text;
                                Label uxLabelFamiliaL = (Label)this.Master.FindControl("uxLabelFamiliaL");
                                uxLabelFamiliaL.Visible = true;
                            }
                            //else
                            //{
                            //    Label uxlabelFamiliaVM = (Label)this.Master.FindControl("uxlabelFamiliaV");
                            //    uxlabelFamiliaVM.Text = "No se encontro Familia.";
                            //    Label uxlabelFamiliaL = (Label)this.Master.FindControl("uxlabelFamiliaL");
                            //    uxlabelFamiliaL.Visible = true;
                            //}

                            dt = ObtenerProductos(true, company, categoria, lblListaPRecio.Text, "", "", "", depto, marca, linea, familia, planta);
                            if (dt.Rows.Count > 0)
                            {
                                ds.Tables.Add(dt);
                                MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                                colorFila();
                            }
                            else
                            {

                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        DataSet ds = new DataSet();
                        llenarDepartamentos();
                        llenarMarcas(); //-----------------> 28032019
                        llenarLineas();
                        llenarFamilia();
                        if (Session["Categorias"] != null)
                        {
                            categoria = Convert.ToString(Session["Categorias"]);
                            
                        }
                        if (Session["Departamento"] != null)
                        {
                            uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                            depto = Convert.ToString(Session["Departamento"]);
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelDepartamentoV");
                            uxLabelCategoriaVM.Text = uxDropDownListDepartamento.SelectedItem.Text;
                            Label uxLabelDepaL = (Label)this.Master.FindControl("uxLabelDepartamentoL");
                            uxLabelDepaL.Visible = true;
                        }
                        if (Session["Marca"] != null)
                        {
                            uxDropDownListMarca.SelectedValue = Convert.ToString(Session["Marca"]);
                            marca = Session["Marca"].ToString();                          
                            Label uxlabelMarcaVM = (Label)this.Master.FindControl("uxLabelMarcaV");
                            uxlabelMarcaVM.Text = uxDropDownListMarca.SelectedItem.Text;
                            Label uxLabelMarcaL = (Label)this.Master.FindControl("uxLabelMarcaL");
                            uxLabelMarcaL.Visible = true;
                        }

                        if (Session["Linea"] != null)
                        {
                            uxDropDownListLinea.SelectedValue = Convert.ToString(Session["Linea"]);
                            linea = Session["Linea"].ToString();
                            Label uxlabelLineaVM = (Label)this.Master.FindControl("uxLabelLineaV");
                            uxlabelLineaVM.Text = uxDropDownListLinea.SelectedItem.Text;
                            Label uxLabelLineaL = (Label)this.Master.FindControl("uxLabelLineaL");
                            uxLabelLineaL.Visible = true;
                        }
                        if (Session["Familia"] != null)
                        {
                            uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                            familia = Session["Familia"].ToString();
                            Label uxlabelFamiliaVM = (Label)this.Master.FindControl("uxLabelFamiliaV");
                            uxlabelFamiliaVM.Text = uxDropDownListFamilia.SelectedItem.Text;
                            Label uxLabelFamiliaL = (Label)this.Master.FindControl("uxLabelFamiliaL");
                            uxLabelFamiliaL.Visible = true;
                        }
                        dt = ObtenerProductos(false, company, categoria, "", "", "", "", depto,marca, linea,familia,planta);
                        if (dt.Rows.Count > 0)
                        {
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        }
                        else
                        {

                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                        }
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                }
            }

            int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
            if (cantidadCarrito > 0)
            {
                CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
                double totalCarrito = 0;
                for (int i = 0; i < cantidadCarrito; i++)
                {
                    totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                }
                lblTotalPedido.Text = totalCarrito.ToString();
            }

            CargarSaldosDisponible();
            colorEmpresa(company);
            if (String.IsNullOrEmpty((String)Session["SessionID"]))
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("~/Acceso.aspx");
            }
        }
        private DataTable ObtenerProductos(bool filtros, string company, string categoria, string listaPRecio, string codigoBarras, string codigoProductos, string descripcion, string depto, string marca, string linea, string familia,string planta)
        {

            try
            {
                DataTable uxTable = new DataTable();
                csProductosN sRuta = new csProductosN();
                uxTable = sRuta.ObtenerProductos(filtros, company, categoria, listaPRecio, codigoBarras, codigoProductos, descripcion,depto, marca, linea, familia, planta);
                DataColumn newcol = new DataColumn("Cantidad", typeof(int));
                newcol.AllowDBNull = true;
                uxTable.Columns.Add(newcol);
                DataColumn newcol2 = new DataColumn("Foto", typeof(string));
                newcol2.AllowDBNull = true;
                uxTable.Columns.Add(newcol2);

                string rutaImagenes = "", imagenDefault = "";
                if (company == "FBD001")
                    rutaImagenes = WebConfigurationManager.AppSettings["rutaImagenesProductosFBD"];
                else if (company == "BBY002")
                    rutaImagenes = WebConfigurationManager.AppSettings["rutaImagenesProductosBBY"];
                else if (company == "ADE001")
                    rutaImagenes = WebConfigurationManager.AppSettings["rutaImagenesProductosADE"];

                imagenDefault = WebConfigurationManager.AppSettings["rutaImagenProductoDefault"];

                foreach (DataRow row in uxTable.Rows)
                {
                    string ruta = rutaImagenes + company + "-" + row["PartNum"].ToString().Replace("/","-") + ".jpg";
                    row["Cantidad"] = 1;
                    if (File.Exists(Server.MapPath(ruta)))
                    {
                        row["Foto"] = ruta;
                    }
                    else
                    {
                        row["Foto"] = imagenDefault;
                    }
                }


                return uxTable;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                return null;
            }
        }
        private void MostrarDatosConPaginacion(Control control, DataSet ds, bool NuevaPag)
        {
            try
            {

                PagedDataSource pageDataSource = new PagedDataSource();
                pageDataSource.DataSource = ds.Tables[0].DefaultView;
                pageDataSource.AllowPaging = true;
                pageDataSource.PageSize = 51;
                int CurPage;
                if (Request.QueryString["IDP"] != null)
                {
                    if (NuevaPag != true)
                    {
                        CurPage = Convert.ToInt32(Request.QueryString["IDP"]);
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pageDataSource.CurrentPageIndex = CurPage - 1;
                strPaginacion = "";

                if (!pageDataSource.IsFirstPage)
                    strPaginacion = " <li class='page-item'> <a href='OrdenVenta.aspx?IDP=" + Convert.ToString(CurPage - 1) + "' class='page-link'>Anterior</a></li>";

                if (pageDataSource.PageCount > 0)
                    strPaginacion += "<li class='page-item'>  <span class='page-link'> (Página " + CurPage.ToString() + " de " + pageDataSource.PageCount.ToString() + ")</span> </li>";

                if (pageDataSource.PageCount > 1)
                    if (!pageDataSource.IsLastPage)
                        strPaginacion += " <li class='page-item'>  <a href='OrdenVenta.aspx?IDP=" + Convert.ToString(CurPage + 1) + "' class='page-link'>Siguiente</a></li>";
                uxDataLisProductos.DataSource = pageDataSource;
                uxDataLisProductos.DataBind();
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void CargarComboGrupos()
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                string user = this.Session["DcdUserID"].ToString();
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerGrupoClientesXUser(user, company);
                DataRow row = uxTabla.NewRow();
                row["GroupDesc"] = "Seleccione Grupo";
                row["GroupCode"] = 0;
                uxTabla.Rows.InsertAt(row, 0);
                uxDropDownListGrupo.DataSource = uxTabla;
                uxDropDownListGrupo.DataTextField = "GroupDesc";
                uxDropDownListGrupo.DataValueField = "GroupCode";
                uxDropDownListGrupo.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarComboClientes()
        {
            try
            {

                // aqui se cambia CustumerID por CustNum
                string company = this.Session["CurComp"].ToString();
                string grupo = uxDropDownListGrupo.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerClientesxGrupoXUser(user, grupo, company);
                DataRow row = uxTabla.NewRow();
                row["Name"] = "Seleccione cliente";
                row["CustNum"] = 0;
                uxTabla.Rows.InsertAt(row, 0);
                uxDropDownListClientes.DataSource = uxTabla;
                uxDropDownListClientes.DataTextField = "Name";
                uxDropDownListClientes.DataValueField = "CustNum";
                uxDropDownListClientes.DataBind();

                uxDropDownListDomicilio.Items.Clear();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarComboDomicilios()
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                string cliente = uxDropDownListClientes.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerDomicilioCliente(user, cliente, company);
                DataRow row = uxTabla.NewRow();
                row["Name"] = "Seleccione domicilio";
                row["ShipToNum"] = 0;
                uxTabla.Rows.InsertAt(row, 0);
                uxDropDownListDomicilio.DataSource = uxTabla;
                uxDropDownListDomicilio.DataTextField = "Name";
                uxDropDownListDomicilio.DataValueField = "ShipToNum";
                uxDropDownListDomicilio.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void llenarcategorias()
        {
            try
            {

                string usr = Session["DcdUserID"].ToString();
                string company = Session["CurComp"].ToString();
                string PlantID = Session["PlantID"].ToString();// 20180802
                DataTable uxTable = new DataTable();
                csClaseCategoriaN cClaseCategoria = new csClaseCategoriaN();
                uxTable = cClaseCategoria.ObtenerCategorias(company, PlantID);
                if (uxTable.Rows.Count > 0)
                {

                    DataColumn newcol2 = new DataColumn("FotoCategoria", typeof(string));
                    newcol2.AllowDBNull = true;
                    uxTable.Columns.Add(newcol2);

                    string rutaImagenes = "", imagenDefault = "";
                    if (company == "FBD001")
                        rutaImagenes = WebConfigurationManager.AppSettings["rutaIconosCategoriaFBD"];
                    else if (company == "BBY002")
                        rutaImagenes = WebConfigurationManager.AppSettings["rutaIconosCategoriaBBY"];
                    else if (company == "ADE001")
                        rutaImagenes = WebConfigurationManager.AppSettings["rutaIconosCategoriaADE"];

                    imagenDefault = WebConfigurationManager.AppSettings["rutaImagenProductoDefault"];

                    foreach (DataRow row in uxTable.Rows)
                    {
                        string ruta = rutaImagenes + company + "-" + row["ClassID"] + ".png";
                        if (File.Exists(Server.MapPath(ruta)))
                        {
                            row["FotoCategoria"] = ruta;
                        }
                        else
                        {
                            row["FotoCategoria"] = imagenDefault;
                        }
                    }

                    uxDataListCategoria.DataSource = uxTable;
                    uxDataListCategoria.DataBind();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void colorFila()
        {
            try
            {

                for (int i = 0; i < this.uxDataLisProductos.Items.Count; i++)
                {
                    double precio = Convert.ToDouble(((Label)this.uxDataLisProductos.Items[i].FindControl("lblBasePrice")).Text);
                    int cantidad = int.Parse(((TextBox)this.uxDataLisProductos.Items[i].FindControl("txtCantidad")).Text);

                    if (precio == 0.0)
                    {
                        ((Label)this.uxDataLisProductos.Items[i].FindControl("lblNoExisteEnLista")).Text = "No existe en lista de precios.";
                        //((Button)this.uxDataLisProductos.Items[i].FindControl("uxButtonAgregarCarrito")).Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void actualizaCarrito()
        {
            try
            {
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                Label lblCarrito = (Label)this.Master.FindControl("lblCantidadCarrito");
                lblCarrito.Text = cantidadCarrito.ToString();
                
                if (cantidadCarrito > 0)
                {
                    CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
                    double totalCarrito = 0;
                    for (int i = 0; i < cantidadCarrito; i++)
                    {
                        totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                    }
                    lblTotalPedido.Text = totalCarrito.ToString();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void ValidarCambio()
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                string company = this.Session["CurComp"].ToString();
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                string listaPrecio = "", nombreCliente = "", categoria = "";
                DataTable dt = new DataTable();
                lblListaPRecio.Text = "";
                if (_accion == enumAccion.CambioGrupo)
                {
                    Session["Grupo"] = uxDropDownListGrupo.SelectedValue;
                    Session["ASPCarroDeCompras"] = null;
                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Departamento"] = null;
                    Session["Marca"] = null;
                    Session["Linea"] = null;
                    Session["Familia"] = null;
                    uxDropDownListClientes.Items.Clear();
                    uxDropDownListDomicilio.Items.Clear();

                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();

                    CargarComboClientes();
                    uxDropDownListDomicilio.Items.Clear();
                    lblListaPRecio.Text = "";
                    lblLimiteCredito.Text = "0";
                    lblCreditoDisponible.Text = "0";
                    lblCreditoDisponible.ForeColor = Color.Black;
                    lblTotalPedido.Text = "0";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    uxTabla = ObtenerProductos(true, company, categoria, "", "", "","","", "", "", "", planta);
                    if (uxTabla.Rows.Count > 0)
                    {
                        uxDataLisProductos.DataSource = uxTabla;
                        uxDataLisProductos.DataBind();
                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }

                }
                else if (_accion == enumAccion.CambioCliente)
                {
                    Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                    Session["ASPCarroDeCompras"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;
                    Session["Marca"] = null;
                    Session["Linea"] = null;
                    Session["Familia"] = null;
                    uxDropDownListDomicilio.Items.Clear();


                    // obtener erp.CustomerPriceLst el precio de lista por cliente ---->
                    csListaPreciosN listp = new csListaPreciosN();
                    string custnum = uxDropDownListClientes.SelectedValue;
                    string grupo = uxDropDownListGrupo.SelectedValue;
                    dt = listp.ObtenerListaPreciosByClienet(company, custnum, grupo);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow rows in dt.Rows)
                        {
                            listaPrecio = rows["ListCode"].ToString();
                            nombreCliente = rows["Name"].ToString();
                            break;
                        }
                        Session["ListaPrecios"] = listaPrecio;
                        Session["NombreCliente"] = nombreCliente;
                        lblListaPRecio.Text = listaPrecio;
                    }
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    //if (Session["Planta"] != null)
                    //{
                    //    planta = Session["Planta"].ToString();
                    //}
                    dt = new DataTable();
                    dt = ObtenerProductos(true, company, categoria, listaPrecio, "", "", "", "", "", "", "", planta);
                    if (dt.Rows.Count > 0)
                    {

                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        colorFila();
                    }


                    string cliente = uxDropDownListClientes.SelectedValue;
                    csSaldosN sSaldo = new csSaldosN();
                    uxTabla = sSaldo.ObtenerSaldosDisponible(cliente, company);
                    if (uxTabla.Rows.Count != 0)
                    {
                        double saldo = Convert.ToDouble(uxTabla.Rows[0]["SaldoDisponible"].ToString());
                        double limite = Convert.ToDouble(uxTabla.Rows[0]["limite"].ToString());
                        double creditoDisponible = 0;

                        if (limite > 0)
                            creditoDisponible = limite - saldo;

                        lblLimiteCredito.Text = limite.ToString();
                        lblCreditoDisponible.Text = creditoDisponible.ToString();
                        if (cantidadCarrito > 0)
                        {
                            if (Session["TotalPedido"] != null)
                                lblTotalPedido.Text = Session["TotalPedido"].ToString();
                            else
                                lblTotalPedido.Text = "0";
                        }
                        else
                            lblTotalPedido.Text = "0";
                    }
                    else
                    {
                        lblLimiteCredito.Text = "0";
                        lblCreditoDisponible.Text = "0";
                        lblCreditoDisponible.ForeColor = Color.Black;
                        lblTotalPedido.Text = "0";
                    }

                    CargarComboDomicilios();

                }
                else if (_accion == enumAccion.CambioDomicilio)
                {
                    Session["Domicilio"] = uxDropDownListDomicilio.SelectedValue.ToString();
                    Session["DomicilioE"] = uxDropDownListDomicilio.SelectedItem.ToString();

                    Session["ASPCarroDeCompras"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;
                    Session["Departamento"] = null;
                    Session["Marca"] = null;
                    Session["Linea"] = null;
                    Session["Familia"] = null;
                    // obtener erp.CustomerPriceLst el precio de lista por cliente ---->
                    csListaPreciosN listp = new csListaPreciosN();
                    string custnum = uxDropDownListClientes.SelectedValue;
                    string domicilio = uxDropDownListDomicilio.SelectedValue;
                    dt = listp.ObtenerListaPreciosByDomicilio(company, custnum, domicilio);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow rows in dt.Rows)
                        {
                            listaPrecio = rows["ListCode"].ToString();
                            nombreCliente = rows["Name"].ToString();
                            break;
                        }
                        Session["ListaPrecios"] = listaPrecio;
                        Session["NombreCliente"] = nombreCliente;
                        lblListaPRecio.Text = listaPrecio;
                    }
                    else
                    {
                        listaPrecio = lblListaPRecio.Text;
                    }
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }

                    dt = new DataTable();
                    dt = ObtenerProductos(true, company, categoria, listaPrecio, "", "", "", "", "", "", "", planta);
                    if (dt.Rows.Count > 0)
                    {

                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        colorFila();
                    }
                }

                _accion = enumAccion.Ninguno;
                Response.Redirect("OrdenVenta");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
        private void CargarSaldosDisponible()
        {
            try
            {
                string cliente = uxDropDownListClientes.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csSaldosN sSaldo = new csSaldosN();
                uxTabla = sSaldo.ObtenerSaldosDisponible(cliente, company);
                if (uxTabla.Rows.Count != 0)
                {
                    double saldo = Convert.ToDouble(uxTabla.Rows[0]["SaldoDisponible"].ToString());
                    double limite = Convert.ToDouble(uxTabla.Rows[0]["limite"].ToString());
                    double creditoDisponible = Convert.ToDouble(uxTabla.Rows[0]["creditoDisponible"].ToString());

                    string CreditHold = uxTabla.Rows[0]["CreditHold"].ToString();
                    if (CreditHold == "False")
                    {
                        if (creditoDisponible > 0)
                        {
                            lblCreditoDisponible.Text = creditoDisponible.ToString("C");

                        }
                        else
                        {
                            lblCreditoDisponible.Text = "0";
                        }
                        lblCreditoDisponible.ForeColor = Color.Black;
                    }
                    else
                    {
                        lblCreditoDisponible.Text = "Credito bloqueado";
                        lblCreditoDisponible.ForeColor = Color.Red;

                    }
                    lblLimiteCredito.Text = limite.ToString("C");
                }
                else
                {
                    lblLimiteCredito.Text = "0";
                    lblCreditoDisponible.Text = "0";
                }


                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {

                    CarroDeCompras carrito = CarroDeCompras.CapturarProducto();

                    double totalCarrito = 0;

                    for (int i = 0; i < cantidadCarrito; i++)
                    {
                        totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                    }

                    lblTotalPedido.Text = totalCarrito.ToString("C");

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected string GetPaginacion()
        {
            return (strPaginacion);
        }
        private void cargarProductosListaPrecio()
        {
            try
            {
                if (uxDropDownListaPrecio.SelectedValue.ToString() == "0")
                {
                    if (uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0
                    && uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                    {
                        
                        string planta = this.Session["PlantID"].ToString();// 02082019
                        string categoria = "";
                        string company = this.Session["CurComp"].ToString();
                        if (Session["Categorias"] != null)
                        {
                            categoria = Session["Categorias"].ToString();
                        }
                        string listaPrecios = "";
                        if (Session["ListaPrecios"] != null)
                        {
                            listaPrecios = Session["ListaPrecios"].ToString();
                        }
                        //  string Planta = this.Session["Planta"].ToString();
                        //if (Session["Planta"] != null)
                        //{
                        //    Planta = Session["  string Planta"].ToString();
                        //}
                        DataTable dt = new DataTable();
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, "", "", "", "", planta);
                        if (dt.Rows.Count > 0)
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                            colorFila();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                        }
                    }
                    else
                    {
                        string planta = this.Session["PlantID"].ToString();// 02082019
                        string categoria = "";
                        string company = this.Session["CurComp"].ToString();
                        if (Session["Categorias"] != null)
                        {
                            categoria = Session["Categorias"].ToString();
                        }

                        DataTable dt = new DataTable();
                        dt = ObtenerProductos(false, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, "", "", "", "", planta);
                        if (dt.Rows.Count > 0)
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                            colorFila();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                        }
                    }
                }
                else if (uxDropDownListaPrecio.SelectedValue.ToString() == "1")
                {
                    if (lblListaPRecio.Text != "")
                    {
                        string categoria = "";
                        string company = this.Session["CurComp"].ToString();
                        if (Session["Categorias"] != null)
                        {
                            categoria = Session["Categorias"].ToString();
                        }
                        string listaPrecios = "";
                        if (Session["ListaPrecios"] != null)
                        {
                            listaPrecios = Session["ListaPrecios"].ToString();
                        }
                        string planta = this.Session["PlantID"].ToString();// 02082019
                        DataTable dt = new DataTable();
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, "", "", "", "", planta);
                        if (dt.Rows.Count > 0)
                        {
                            IEnumerable<DataRow> query =
                                from order in dt.AsEnumerable()
                                where order.Field<string>("BasePrice") != "0.000000"
                                select order;
                            if (query.Any())
                            {
                                dt = query.CopyToDataTable();
                                DataSet ds = new DataSet();
                                ds.Tables.Add(dt);
                                MostrarDatosConPaginacion(uxDataLisProductos, ds, false);

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                        }

                    }
                    else
                    {
                        uxDropDownListaPrecio.SelectedIndex = 0;
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('9', 'precaucion', true,false,'es-mx');", true);
                    }
                }
                else if (uxDropDownListaPrecio.SelectedValue.ToString() == "2")
                {
                    string categoria = "";
                    string company = this.Session["CurComp"].ToString();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string listaPrecios = "";
                    if (Session["ListaPrecios"] != null)
                    {
                        listaPrecios = Session["ListaPrecios"].ToString();
                    }
                    string planta = this.Session["PlantID"].ToString();// 02082019
                    DataTable dt = new DataTable();
                    dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, "", "", "", "", planta);
                    if (dt.Rows.Count > 0)
                    {
                        IEnumerable<DataRow> query =
                            from order in dt.AsEnumerable()
                            where order.Field<string>("BasePrice") == "0.000000"
                            select order;
                        if (query.Any())
                        {
                            dt = query.CopyToDataTable();

                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                            colorFila();

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }       
        public void colorEmpresa(string company)
        {
            try
            {
                if (company == "ADE001")
                {

                }
                else if (company == "FBD001")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "colorCompany", "cambiaColorEmpresa('" + company + "');", true);
                }
                else if (company == "BBY002")
                {

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void llenarMarcas()
        {
            try
            {
                string company = Session["CurComp"].ToString();
                string categoria = "";
                if (Session["Categorias"] != null)
                {
                    categoria = Session["Categorias"].ToString();
                }
                DataTable uxtablaDep = new DataTable();
                csDepartamentoN Depto = new csDepartamentoN();
                uxtablaDep = Depto.ObtenerMarca(company, categoria);
                if (uxtablaDep.Rows.Count > 0)
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Marca_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    //Indicamos cúales van a ser los datos a asociar
                    uxDropDownListMarca.DataSource = uxtablaDep;
                    uxDropDownListMarca.DataValueField = "Marca_c";
                    uxDropDownListMarca.DataTextField = "Character01";
                    uxDropDownListMarca.DataBind();
                }
                else
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Marca_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    uxDropDownListMarca.DataSource = uxtablaDep;
                    uxDropDownListMarca.DataValueField = "Marca_c";
                    uxDropDownListMarca.DataTextField = "Character01";
                    uxDropDownListMarca.DataBind();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void llenarLineas()
        {
            try
            {
                string company = Session["CurComp"].ToString();
                string categoria = "";
                if (Session["Categorias"] != null)
                {
                    categoria = Session["Categorias"].ToString();
                }

                DataTable uxtablaDep = new DataTable();
                csDepartamentoN Depto = new csDepartamentoN();
                uxtablaDep = Depto.ObtenerLinea(company, categoria);
                if (uxtablaDep.Rows.Count > 0)
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Linea_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    //Indicamos cúales van a ser los datos a asociar
                    uxDropDownListLinea.DataSource = uxtablaDep;
                    uxDropDownListLinea.DataValueField = "Linea_c";
                    uxDropDownListLinea.DataTextField = "Character01";
                    uxDropDownListLinea.DataBind();
                }
                else
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Linea_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    uxDropDownListLinea.DataSource = uxtablaDep;
                    uxDropDownListLinea.DataValueField = "Linea_c";
                    uxDropDownListLinea.DataTextField = "Character01";
                    uxDropDownListLinea.DataBind();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        } 
        public void llenarFamilia()
        {
            try
            {
                string company = Session["CurComp"].ToString();
                string categoria = "";
                if (Session["Categorias"] != null)
                {
                    categoria = Session["Categorias"].ToString();
                }

                DataTable uxtablaDep = new DataTable();
                csDepartamentoN Depto = new csDepartamentoN();
                uxtablaDep = Depto.ObtenerFamilia(company, categoria);
                if (uxtablaDep.Rows.Count > 0)
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Familia_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    //Indicamos cúales van a ser los datos a asociar
                    uxDropDownListFamilia.DataSource = uxtablaDep;
                    uxDropDownListFamilia.DataValueField = "Familia_c";
                    uxDropDownListFamilia.DataTextField = "Character01";
                    uxDropDownListFamilia.DataBind();
                }
                else
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Familia_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    uxDropDownListFamilia.DataSource = uxtablaDep;
                    uxDropDownListFamilia.DataValueField = "Familia_c";
                    uxDropDownListFamilia.DataTextField = "Character01";
                    uxDropDownListFamilia.DataBind();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void llenarDepartamentos()
        {
            try
            {
                string company = Session["CurComp"].ToString();
                string categoria = "";
                if (Session["Categorias"] != null)
                {
                    categoria = Session["Categorias"].ToString();
                }

                DataTable uxtablaDep = new DataTable();
                csDepartamentoN Depto = new csDepartamentoN();
                uxtablaDep = Depto.ObtenerDepartamentos(company, categoria);
                if (uxtablaDep.Rows.Count > 0)
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Departamento_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    //Indicamos cúales van a ser los datos a asociar
                    uxDropDownListDepartamento.DataSource = uxtablaDep;
                    uxDropDownListDepartamento.DataValueField = "Departamento_c";
                    uxDropDownListDepartamento.DataTextField = "Character01";
                    uxDropDownListDepartamento.DataBind();
                }
                else
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Departamento_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    uxDropDownListDepartamento.DataSource = uxtablaDep;
                    uxDropDownListDepartamento.DataValueField = "Departamento_c";
                    uxDropDownListDepartamento.DataTextField = "Character01";
                    uxDropDownListDepartamento.DataBind();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        [WebMethod]
        public List<string> GetEmployeeName(string empName)
        {
            try
            {
                List<string> empResult = new List<string>();
                List<object> lis = new List<object>();
                DataTable ds = new DataTable();
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csClientesN cClientes = new csClientesN();
                ds = cClientes.ObtenerDatosClienteAuto(company, user, empName);
                if (ds.Rows.Count > 0)
                {
                    DataRow row = ds.NewRow();
                    row["Name"] = "Seleccione cliente";
                    row["CustNum"] = 0;
                    ds.Rows.InsertAt(row, 0);
                    uxDropDownListClientes.DataSource = ds;
                    uxDropDownListClientes.DataTextField = "Name";
                    uxDropDownListClientes.DataValueField = "CustNum";
                    uxDropDownListClientes.DataBind();

                    foreach (DataRow row2 in ds.Rows)
                    {
                        foreach (DataColumn col in ds.Columns)
                        {

                            empResult.Add(Convert.ToString(row2[col]));
                        }
                    }

                    return empResult;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    List<string> empResult3 = new List<string>();
                    return empResult3;

                }

            }
            catch (Exception ex)
            {
                List<string> empResult2 = new List<string>();
                return empResult2;
            }
        }       
        private void CargarComboGrupoByCust()
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                string UserId = this.Session["DcdUserID"].ToString();
                string custId = uxDropDownListClientes.SelectedValue;
                Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerClienteXgrupXCust(UserId, custId, company);
                if (uxTabla.Rows.Count > 0)
                {
                    uxDropDownListGrupo.SelectedValue = uxTabla.Rows[0]["GroupCode"].ToString();
                    Session["Grupo"] = uxDropDownListGrupo.SelectedValue;
                }
                else
                {
                    CargarComboGrupos();

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxDataListCategoria_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                uxDataListCategoria.SelectedIndex = e.Item.ItemIndex;
                string planta = this.Session["PlantID"].ToString();// 02082019
                string company = this.Session["CurComp"].ToString();
                uxDropDownListaPrecio.SelectedIndex = 0;
                if (e.CommandName == "Seleccionar")
                {
                    string classID = ((Label)this.uxDataListCategoria.SelectedItem.FindControl("lblClassID")).Text;
                    string descripcion = ((Label)this.uxDataListCategoria.SelectedItem.FindControl("lblDescripcion")).Text;

                    Session["Categorias"] = classID;
                    if (Session["Categorias"] != null)
                    {
                        classID = Convert.ToString(Session["Categorias"]);
                        string PlantID = this.Session["PlantID"].ToString();// 02082019
                        csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                        DataTable catego = cCategoria.ObtenerCategoriaID(company, classID, PlantID);
                        if (catego.Rows.Count > 0)
                        {
                            uxLabelCategoriaV.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                        else
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = "No se encontro categoría.";
                            uxLabelCategoriaV.Text = "No se encontro categoría.";
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                    }
                

                    string listaPrecios = "";
                    if (Session["ListaPrecios"] != null)
                    {
                        listaPrecios = Session["ListaPrecios"].ToString();
                        lblListaPRecio.Text = listaPrecios;
                    }
                    DataTable dt = new DataTable();
                    if (Session["Categorias"] == null || classID == "")
                        dt = ObtenerProductos(false, company, "", listaPrecios, "", "", "", "", "", "", "", planta);
                    else
                        dt = ObtenerProductos(true, company, classID, listaPrecios, "", "", "", "", "", "", "", planta);
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, true);
                        colorFila();
                    }
                    else
                    {
                        dt = new DataTable();
                        dt = ObtenerProductos(true, company, "", listaPrecios, "", "", "", "", "", "", "", planta);
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, true);
                        colorFila();
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                    llenarDepartamentos();
                    colorEmpresa(company);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDataLisProductos_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                uxDataLisProductos.SelectedIndex = e.Item.ItemIndex;

                string company = this.Session["CurComp"].ToString();

                if (e.CommandName == "Seleccionar")
                {
                    if (uxDropDownListGrupo.SelectedIndex != -1 && uxDropDownListGrupo.SelectedIndex != 0
                        && uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0
                        && uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                    {

                        if (lblCreditoDisponible.Text != "Credito bloqueado")
                        {
                            CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
                            bool seEncontroRegistro = false;
                            string clienteId = uxDropDownListClientes.SelectedValue;
                            string partNum = ((Label)this.uxDataLisProductos.SelectedItem.FindControl("lblPartNum")).Text;
                            double precio = Convert.ToDouble(((Label)this.uxDataLisProductos.SelectedItem.FindControl("lblBasePrice")).Text);
                            int disponible = Convert.ToInt32(((Label)this.uxDataLisProductos.SelectedItem.FindControl("uxLabelDispo")).Text);
                            string descripcion = ((Label)this.uxDataLisProductos.SelectedItem.FindControl("lblPartDescription")).Text;
                            int Cantidad = int.Parse(((TextBox)this.uxDataLisProductos.SelectedItem.FindControl("txtCantidad")).Text);

                            if (precio > 0)
                            {
                                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                                string limiteCredito = lblLimiteCredito.Text.Replace("$", "").Replace(",", "");
                                if (Convert.ToDouble(limiteCredito) == 0)
                                {
                                    ListaP = CarroDeCompras.CapturarProducto().ListaProductos;
                                    foreach (cEntidad.ProductosAlcarro item in ListaP)
                                    {
                                        if (item.IdProducto == partNum)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('uxModalValidaProductoExiste');", true);
                                        }
                                        else
                                        {
                                            seEncontroRegistro = false;
                                        }
                                    }
                                    if (seEncontroRegistro == false)
                                    {
                                        carrito.Agregar(partNum, precio, descripcion, Cantidad);
                                        ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('centralModalSuccess');", true);
                                    }
                                }
                                else
                                {
                                    double totalCarrito = 0;
                                    for (int i = 0; i < cantidadCarrito; i++)
                                    {
                                        totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                                    }

                                    double totalPrecioProducto = precio * Cantidad;
                                    double totalMasProducto = totalCarrito + totalPrecioProducto;
                                    //-------------------->

                                    int vcot = Convert.ToInt32(Session["EsCotizacion"]);
                                    if (Convert.ToInt32( Session["EsCotizacion"]) != 1)
                                    {
                                        if (totalMasProducto <= Convert.ToDouble(limiteCredito))
                                        {
                                            ListaP = CarroDeCompras.CapturarProducto().ListaProductos;
                                            foreach (cEntidad.ProductosAlcarro item in ListaP)
                                            {
                                                if (item.IdProducto == partNum)
                                                {
                                                    seEncontroRegistro = true;
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('uxModalValidaProductoExiste');", true);
                                                    break;
                                                }
                                                else
                                                {
                                                    seEncontroRegistro = false;
                                                }
                                            }
                                            if (seEncontroRegistro == false)
                                            {
                                                carrito.Agregar(partNum, precio, descripcion, Cantidad);
                                                ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('centralModalSuccess');", true);
                                            }
                                        }
                                        else
                                        {
                                            CarroDeCompras.CapturarProducto().EliminarProductos(partNum, precio, descripcion);
                                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('8', 'precaucion', true,false,'es-mx');", true);
                                        }
                                    }
                                    else
                                    {
                                        ListaP = CarroDeCompras.CapturarProducto().ListaProductos;
                                        foreach (cEntidad.ProductosAlcarro item in ListaP)
                                        {
                                            if (item.IdProducto == partNum)
                                            {
                                                seEncontroRegistro = true;
                                                ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('uxModalValidaProductoExiste');", true);
                                                break;
                                            }
                                            else
                                            {
                                                seEncontroRegistro = false;
                                            }
                                        }
                                        if (seEncontroRegistro == false)
                                        {
                                            carrito.Agregar(partNum, precio, descripcion, Cantidad);
                                            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('centralModalSuccess');", true);
                                        }
                                    }
                                   
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('4', 'precaucion', true,false,'es-mx');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('11', 'precaucion', true,false,'es-mx');", true);
                        }                        
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('10', 'precaucion', true,false,'es-mx');", true);
                    }
                }
                actualizaCarrito();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxLinkButtonBotonArriba_Click(object sender, EventArgs e)
        {
            string company = "";
            if (Session["CurComp"] != null)
                company = this.Session["CurComp"].ToString();
            string PlantID = this.Session["PlantID"].ToString();// 02082019
            if (!this.IsPostBack)
            {
                try
                {
                    string user = this.Session["DcdUserID"].ToString();
                    string categoria = "";
                    string depto = "";
                    string marca = "";
                    string linea = "";
                    string familia = "";
                    CargarComboGrupos();
                    llenarcategorias();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Convert.ToString(Session["Categorias"]);
                       
                        csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                        DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                        if (catego.Rows.Count > 0)
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                        else
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = "No se encontro categoría.";
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                    }
                    if (Session["Grupo"] != null)
                    {
                        uxDropDownListGrupo.SelectedValue = Convert.ToString(Session["Grupo"]);
                        CargarComboClientes();
                        if (Session["Cliente"] != null)
                        {
                            uxDropDownListClientes.SelectedValue = Convert.ToString(Session["Cliente"]);

                            CargarSaldosDisponible();

                            CargarComboDomicilios();
                            if (Session["Domicilio"] != null)
                            {
                                uxDropDownListDomicilio.SelectedValue = Convert.ToString(Session["Domicilio"]);
                                llenarDepartamentos();
                                if (Session["Categorias"] != null)
                                {
                                    categoria = Convert.ToString(Session["Categorias"]);
                                    ////---------cargar deptos si se ha seleccionado alguna
                                    //if (Session["Departamento"] != null)
                                    //{
                                    //    uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                                    //    depto = Convert.ToString(Session["Departamento"]);
                                    //}
                                }
                            }
                        }
                        if (Session["ListaPrecios"] != null)
                            lblListaPRecio.Text = Session["ListaPrecios"].ToString();

                        if (Session["FiltroExisteEnListaPrecio"] != null)
                        {
                            uxDropDownListaPrecio.SelectedValue = Session["FiltroExisteEnListaPrecio"].ToString();
                            cargarProductosListaPrecio();
                        }
                        else
                        {
                           
                            DataTable dt = new DataTable();
                            DataSet ds = new DataSet();
                            if (Session["Departamento"] != null)
                            {
                                uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                                depto = Convert.ToString(Session["Departamento"]);
                            }
                            if (Session["Marca"] != null)
                            {
                                uxDropDownListMarca.SelectedValue = Convert.ToString(Session["Marca"]);
                                marca = Session["Marca"].ToString();
                            }

                            if (Session["Linea"] != null)
                            {
                                uxDropDownListLinea.SelectedValue = Convert.ToString(Session["Marca"]);
                                linea = Session["Linea"].ToString();
                            }
                            if (Session["Familia"] != null)
                            {
                                uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                                familia = Session["Familia"].ToString();
                            }

                            dt = ObtenerProductos(true, company, categoria, lblListaPRecio.Text, "", "", "", depto, marca, linea, familia,  PlantID);
                            if (dt.Rows.Count > 0)
                            {
                                ds.Tables.Add(dt);
                                MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                                colorFila();
                            }
                            else
                            {

                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        DataSet ds = new DataSet();
                        llenarDepartamentos();
                        llenarMarcas(); //-----------------> 28032019
                        llenarLineas();
                        llenarFamilia();
                        if (Session["Categorias"] != null)
                        {
                            categoria = Convert.ToString(Session["Categorias"]);

                        }
                        if (Session["Departamento"] != null)
                        {
                            uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                            depto = Convert.ToString(Session["Departamento"]);
                        }
                        if (Session["Marca"] != null)
                        {
                            uxDropDownListMarca.SelectedValue = Convert.ToString(Session["Marca"]);
                            marca = Session["Marca"].ToString();
                        }

                        if (Session["Linea"] != null)
                        {
                            uxDropDownListLinea.SelectedValue = Convert.ToString(Session["Marca"]);
                            linea = Session["Linea"].ToString();
                        }
                        if (Session["Familia"] != null)
                        {
                            uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                            familia = Session["Familia"].ToString();
                        }
                        dt = ObtenerProductos(false, company, categoria, "", "", "", "", depto, marca, linea, familia, PlantID);
                        if (dt.Rows.Count > 0)
                        {
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        }
                        else
                        {

                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                        }
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                }
            }
            colorEmpresa(company);
            
        }
        protected void uxLinkButtonBotonInicioProductos_Click(object sender, EventArgs e)
        {
            string company = "";
            if (Session["CurComp"] != null)
                company = this.Session["CurComp"].ToString();
            string planta = this.Session["PlantID"].ToString();// 02082019
            if (!this.IsPostBack)
            {
                try
                {
                    string user = this.Session["DcdUserID"].ToString();
                    string categoria = "";
                    string depto = "";
                    string marca = "";
                    string linea = "";
                    string familia = "";
                    CargarComboGrupos();
                    llenarcategorias();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Convert.ToString(Session["Categorias"]);
                        string PlantID = this.Session["PlantID"].ToString();// 02082019
                        csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                        DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                        if (catego.Rows.Count > 0)
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                        else
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = "No se encontro categoría.";
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                    }
                    if (Session["Grupo"] != null)
                    {
                        uxDropDownListGrupo.SelectedValue = Convert.ToString(Session["Grupo"]);
                        CargarComboClientes();
                        if (Session["Cliente"] != null)
                        {
                            uxDropDownListClientes.SelectedValue = Convert.ToString(Session["Cliente"]);

                            CargarSaldosDisponible();

                            CargarComboDomicilios();
                            if (Session["Domicilio"] != null)
                            {
                                uxDropDownListDomicilio.SelectedValue = Convert.ToString(Session["Domicilio"]);
                                llenarDepartamentos();
                                if (Session["Categorias"] != null)
                                {
                                    categoria = Convert.ToString(Session["Categorias"]);
                                    ////---------cargar deptos si se ha seleccionado alguna
                                    //if (Session["Departamento"] != null)
                                    //{
                                    //    uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                                    //    depto = Convert.ToString(Session["Departamento"]);
                                    //}
                                }
                            }
                        }
                        if (Session["ListaPrecios"] != null)
                            lblListaPRecio.Text = Session["ListaPrecios"].ToString();

                        if (Session["FiltroExisteEnListaPrecio"] != null)
                        {
                            uxDropDownListaPrecio.SelectedValue = Session["FiltroExisteEnListaPrecio"].ToString();
                            cargarProductosListaPrecio();
                        }
                        else
                        {
                            DataTable dt = new DataTable();
                            DataSet ds = new DataSet();
                            if (Session["Departamento"] != null)
                            {
                                uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                                depto = Convert.ToString(Session["Departamento"]);
                            }
                            if (Session["Marca"] != null)
                            {
                                uxDropDownListMarca.SelectedValue = Convert.ToString(Session["Marca"]);
                                marca = Session["Marca"].ToString();
                            }

                            if (Session["Linea"] != null)
                            {
                                uxDropDownListLinea.SelectedValue = Convert.ToString(Session["Marca"]);
                                linea = Session["Linea"].ToString();
                            }
                            if (Session["Familia"] != null)
                            {
                                uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                                familia = Session["Familia"].ToString();
                            }

                            dt = ObtenerProductos(true, company, categoria, lblListaPRecio.Text, "", "", "", depto, marca, linea, familia, planta);
                            if (dt.Rows.Count > 0)
                            {
                                ds.Tables.Add(dt);
                                MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                                colorFila();
                            }
                            else
                            {

                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        DataSet ds = new DataSet();
                        llenarDepartamentos();
                        llenarMarcas(); //-----------------> 28032019
                        llenarLineas();
                        llenarFamilia();
                        if (Session["Categorias"] != null)
                        {
                            categoria = Convert.ToString(Session["Categorias"]);

                        }
                        if (Session["Departamento"] != null)
                        {
                            uxDropDownListDepartamento.SelectedValue = Convert.ToString(Session["Departamento"]);
                            depto = Convert.ToString(Session["Departamento"]);
                        }
                        if (Session["Marca"] != null)
                        {
                            uxDropDownListMarca.SelectedValue = Convert.ToString(Session["Marca"]);
                            marca = Session["Marca"].ToString();
                        }

                        if (Session["Linea"] != null)
                        {
                            uxDropDownListLinea.SelectedValue = Convert.ToString(Session["Marca"]);
                            linea = Session["Linea"].ToString();
                        }
                        if (Session["Familia"] != null)
                        {
                            uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                            familia = Session["Familia"].ToString();
                        }
                        dt = ObtenerProductos(false, company, categoria, "", "", "", "", depto, marca, linea, familia, planta);
                        if (dt.Rows.Count > 0)
                        {
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        }
                        else
                        {

                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                        }
                    }

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                }
            }
            colorEmpresa(company);
        }

        protected void uxDropDownListDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                string company = this.Session["CurComp"].ToString();
                if (uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0
                    && uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                {
                    string categoria = "";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string listaPrecios = ""; // 25022019-----lista de precios se enviaba vacio
                    if (Session["ListaPrecios"] != null)
                    {
                        listaPrecios = Session["ListaPrecios"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        Session["Departamento"] = null;  // deje en null si ya tenia una sesion
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        marca = Session["Marca"].ToString();
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                        linea = Session["Linea"].ToString();
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        familia = Session["Familia"].ToString();
                    }


                    DataTable dt = new DataTable();
                    if (uxDropDownListDepartamento.SelectedIndex == 0)
                    {

                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["Departamento"] = uxDropDownListDepartamento.SelectedValue;
                        depto = uxDropDownListDepartamento.SelectedValue;
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --uxDropDownListDepartamento.seelctvalue
                    }
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);

                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                }
                else
                {
                    string categoria = "";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        Session["Departamento"] = null;  // deje en null si ya tenia una cesion
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        marca = Session["Marca"].ToString();
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                        linea = Session["Linea"].ToString();
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        familia = Session["Familia"].ToString();
                    }
                    DataTable dt = new DataTable();
                    if (uxDropDownListDepartamento.SelectedIndex == 0)
                    {

                        dt = ObtenerProductos(true, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["Departamento"] = uxDropDownListDepartamento.SelectedValue;
                        depto = uxDropDownListDepartamento.SelectedValue;

                        dt = ObtenerProductos(false, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);

                    }
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }

                }

                Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelDepartamentoV");
                uxLabelCategoriaVM.Text = uxDropDownListDepartamento.SelectedItem.Text;
                Label uxLabelDepaL = (Label)this.Master.FindControl("uxLabelDepartamentoL");
                uxLabelDepaL.Visible = true;
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                if (uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0
                    && uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                {
                    string categoria = "";
                    string company = this.Session["CurComp"].ToString();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string listaPrecios = ""; // 25022019-----lista de precios se enviaba vacio
                    if (Session["ListaPrecios"] != null)
                    {
                        listaPrecios = Session["ListaPrecios"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        Session["Marca"] = null; // deje en null si ya tenia una cesion
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                        linea = Session["Linea"].ToString();
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        familia = Session["Familia"].ToString();
                    }
                    DataTable dt = new DataTable();
                    if (uxDropDownListMarca.SelectedIndex == 0)
                    {
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["Marca"] = uxDropDownListMarca.SelectedValue;
                        marca = uxDropDownListMarca.SelectedValue;
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --uxDropDownListDepartamento.seelctvalue
                        Label uxlabelMarcaVM = (Label)this.Master.FindControl("uxLabelMarcaV");
                        uxlabelMarcaVM.Text = uxDropDownListMarca.SelectedItem.Text;
                        Label uxLabelMarcaL = (Label)this.Master.FindControl("uxLabelMarcaL");
                        uxLabelMarcaL.Visible = true;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                }
                else
                {
                    string categoria = "";
                    string company = this.Session["CurComp"].ToString();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        Session["Marca"] = null; // deje en null si ya tenia una cesion
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                        linea = Session["Linea"].ToString();
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        familia = Session["Familia"].ToString();
                    }
                    DataTable dt = new DataTable();
                    if (uxDropDownListMarca.SelectedIndex == 0)
                    {
                        dt = ObtenerProductos(true, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["Marca"] = uxDropDownListMarca.SelectedValue;
                        marca = uxDropDownListMarca.SelectedValue;
                        dt = ObtenerProductos(false, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);
                        Label uxlabelMarcaVM = (Label)this.Master.FindControl("uxLabelMarcaV");
                        uxlabelMarcaVM.Text = uxDropDownListMarca.SelectedItem.Text;
                        Label uxLabelMarcaL = (Label)this.Master.FindControl("uxLabelMarcaL");
                        uxLabelMarcaL.Visible = true;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);

                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }

                }
            
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListLinea_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                if (uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0
                    && uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                {
                    string categoria = "";
                    string company = this.Session["CurComp"].ToString();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string listaPrecios = ""; // 25022019-----lista de precios se enviaba vacio
                    if (Session["ListaPrecios"] != null)
                    {
                        listaPrecios = Session["ListaPrecios"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        marca = Session["Marca"].ToString();
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                        Session["Linea"] = null;
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        familia = Session["Familia"].ToString();
                    }
                    DataTable dt = new DataTable();
                    if (uxDropDownListLinea.SelectedIndex == 0)
                    {
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["Linea"] = uxDropDownListLinea.SelectedValue;
                        linea = uxDropDownListLinea.SelectedValue;
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);
                     
                        Label uxlabelLineaVM = (Label)this.Master.FindControl("uxLabelLineaV");
                        uxlabelLineaVM.Text = uxDropDownListLinea.SelectedItem.Text;
                        Label uxLabelLineaL = (Label)this.Master.FindControl("uxLabelLIneaL");
                        uxLabelLineaL.Visible = true;

                    }
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                }
                else
                {
                    string categoria = "";
                    string company = this.Session["CurComp"].ToString();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        marca = Session["Marca"].ToString();
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                      Session["Linea"] = null;
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        familia = Session["Familia"].ToString();
                    }
                    DataTable dt = new DataTable();
                    if (uxDropDownListLinea.SelectedIndex == 0)
                    {
                        dt = ObtenerProductos(true, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["linea"] = uxDropDownListLinea.SelectedValue;
                        linea = uxDropDownListLinea.SelectedValue;                      
                        dt = ObtenerProductos(false, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);
                        Label uxlabelLineaVM = (Label)this.Master.FindControl("uxLabelLineaV");
                        uxlabelLineaVM.Text = uxDropDownListLinea.SelectedItem.Text;
                        Label uxLabelLineaL = (Label)this.Master.FindControl("uxLabelLIneaL");
                        uxLabelLineaL.Visible = true;
                    }
                    if (dt.Rows.Count > 0)
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);

                        }
                        else
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListFamilia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                if (uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0
                    && uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                {
                    string categoria = "";
                    string company = this.Session["CurComp"].ToString();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string listaPrecios = ""; // 25022019-----lista de precios se enviaba vacio
                    if (Session["ListaPrecios"] != null)
                    {
                        listaPrecios = Session["ListaPrecios"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        marca = Session["Marca"].ToString();
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                        linea = Session["Linea"].ToString();
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        Session["Familia"] = null;
                    }
                    DataTable dt = new DataTable();
                    if (uxDropDownListFamilia.SelectedIndex == 0)
                    {
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["Familia"] = uxDropDownListFamilia.SelectedValue;
                        familia = uxDropDownListFamilia.SelectedValue;
                        dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --uxDropDownListDepartamento.seelctvalue
                        uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                        familia = Session["Familia"].ToString();
                        Label uxlabelFamiliaVM = (Label)this.Master.FindControl("uxLabelFamiliaV");
                        uxlabelFamiliaVM.Text = uxDropDownListFamilia.SelectedItem.Text;
                        Label uxLabelFamiliaL = (Label)this.Master.FindControl("uxLabelFamiliaL");
                        uxLabelFamiliaL.Visible = true;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                }
                else
                {
                    string categoria = "";
                    string company = this.Session["CurComp"].ToString();
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }
                    string marca = "";
                    if (Session["Marca"] != null)
                    {
                        marca = Session["Marca"].ToString();
                    }
                    string linea = "";
                    if (Session["Linea"] != null)
                    {
                        linea = Session["Linea"].ToString();
                    }
                    string familia = "";
                    if (Session["Familia"] != null)
                    {
                        Session["Familia"] = null;
                    }
                    DataTable dt = new DataTable();
                    if (uxDropDownListFamilia.SelectedIndex == 0)
                    {
                        dt = ObtenerProductos(true, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);// --
                    }
                    else
                    {
                        Session["Familia"] = uxDropDownListFamilia.SelectedValue;
                        familia = uxDropDownListFamilia.SelectedValue;
                        dt = ObtenerProductos(false, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text, depto, marca, linea, familia, planta);
                        uxDropDownListFamilia.SelectedValue = Convert.ToString(Session["Familia"]);
                        familia = Session["Familia"].ToString();
                        Label uxlabelFamiliaVM = (Label)this.Master.FindControl("uxLabelFamiliaV");
                        uxlabelFamiliaVM.Text = uxDropDownListFamilia.SelectedItem.Text;
                        Label uxLabelFamiliaL = (Label)this.Master.FindControl("uxLabelFamiliaL");
                        uxLabelFamiliaL.Visible = true;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);

                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListaPrecio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session["FiltroExisteEnListaPrecio"] = uxDropDownListaPrecio.SelectedValue;
                cargarProductosListaPrecio();
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                string company = this.Session["CurComp"].ToString();
                    DataTable dt = new DataTable();

                    int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                    if (cantidadCarrito > 0)
                    {
                        Session["Cambio"] = "1";
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                    }
                    else
                    {
                        Session["Grupo"] = uxDropDownListGrupo.SelectedValue;
                        CargarComboClientes();
                        uxDropDownListDomicilio.Items.Clear();
                        lblListaPRecio.Text = "";
                        lblLimiteCredito.Text = "0";
                        lblCreditoDisponible.Text = "0";
                        lblCreditoDisponible.ForeColor = Color.Black;
                        lblTotalPedido.Text = "0";
                        string categoria = "";

                        if (Session["Categorias"] != null)
                        {
                            categoria = Session["Categorias"].ToString();
                        }
                        dt = ObtenerProductos(true, company, categoria, "", "", "", "", "", "", "", "", planta);
                        if (dt.Rows.Count > 0)
                        {

                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                        }
                    }

                    colorEmpresa(company);
               
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["Grupo"] != null)
                {
                    string company = this.Session["CurComp"].ToString();
                    DataTable dt = new DataTable();
                    lblListaPRecio.Text = "";
                    int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                    if (cantidadCarrito > 0)
                    {
                        Session["Cambio"] = "2";
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                    }
                    else
                    {
                        var clienteID = uxDropDownListClientes.SelectedItem.Text.ToString().Split('-');
                        Session["custID"] = clienteID[0].ToString().Trim();
                        Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                        
                        string UserId = this.Session["DcdUserID"].ToString();
                        string custId = uxDropDownListClientes.SelectedValue;
                        Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                        csGrupoClientesN sRuta = new csGrupoClientesN();
                        uxTabla = sRuta.ObtenerClienteXgrupXCust(UserId, custId, company);
                        if (uxTabla.Rows.Count > 0)
                        {
                            uxDropDownListGrupo.SelectedValue = uxTabla.Rows[0]["GroupCode"].ToString();
                            Session["Grupo"] = uxDropDownListGrupo.SelectedValue;
                        }

                        string cliente = uxDropDownListClientes.SelectedValue;
                        csSaldosN sSaldo = new csSaldosN();
                        uxTabla = sSaldo.ObtenerSaldosDisponible(cliente, company);
                        if (uxTabla.Rows.Count != 0)
                        {
                            double saldo = Convert.ToDouble(uxTabla.Rows[0]["SaldoDisponible"].ToString());
                            double limite = Convert.ToDouble(uxTabla.Rows[0]["limite"].ToString());
                            double creditoDisponible = 0;

                            if (limite > 0)
                                creditoDisponible = limite - saldo;

                            lblLimiteCredito.Text = limite.ToString();
                            lblCreditoDisponible.Text = creditoDisponible.ToString();
                            if (cantidadCarrito > 0)
                            {
                                if (Session["TotalPedido"] != null)
                                    lblTotalPedido.Text = Session["TotalPedido"].ToString();
                                else
                                    lblTotalPedido.Text = "0";
                            }
                            else
                                lblTotalPedido.Text = "0";
                        }
                        else
                        {
                            lblLimiteCredito.Text = "0";
                            lblCreditoDisponible.Text = "0";
                            lblCreditoDisponible.ForeColor = Color.Black;
                            lblTotalPedido.Text = "0";
                        }
                        CargarSaldosDisponible();
                        CargarComboDomicilios();
                    }
                    colorEmpresa(company);
                }
                else
                {
                    CargarComboGrupoByCust();
                    CargarComboDomicilios();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListDomicilio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                string listaPrecio = "", nombreCliente = "", categoria = "";
                string company = this.Session["CurComp"].ToString();
                DataTable dt = new DataTable();

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "3";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                }
                else
                {
                    Session["Domicilio"] = uxDropDownListDomicilio.SelectedValue.ToString();
                    Session["DomicilioE"] = uxDropDownListDomicilio.SelectedItem.ToString();
                    // obtener erp.CustomerPriceLst el precio de lista por cliente ---->
                    csListaPreciosN listp = new csListaPreciosN();
                    string custnum = uxDropDownListClientes.SelectedValue;
                    string domicilio = uxDropDownListDomicilio.SelectedValue;
                    dt = listp.ObtenerListaPreciosByDomicilio(company, custnum, domicilio);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow rows in dt.Rows)
                        {
                            listaPrecio = rows["ListCode"].ToString();
                            nombreCliente = rows["Name"].ToString();
                            break;
                        }
                        Session["ListaPrecios"] = listaPrecio;
                        Session["NombreCliente"] = nombreCliente;
                        lblListaPRecio.Text = listaPrecio;
                    }
                    else
                    {
                        listaPrecio = lblListaPRecio.Text;
                    }
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }

                    dt = new DataTable();
                    dt = ObtenerProductos(true, company, categoria, listaPrecio, "", "","","", "", "", "", planta);
                    if (dt.Rows.Count > 0)
                    {

                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                    }
                    colorFila();
                    colorEmpresa(company);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxTextBoxDescripcion_TextChanged(object sender, EventArgs e)
        {
            Session["Categorias"] = null;
            Session["Departamento"] = null;
            Session["Marca"] = null;
            Session["Linea"] = null;
            Session["Familia"] = null;
        }
        protected void txtCantidad_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                TextBox txt = sender as TextBox;
                DataListItem row = txt.NamingContainer as DataListItem;
                uxDataLisProductos.SelectedIndex = row.ItemIndex;

                if (uxDataLisProductos.SelectedIndex != -1)
                {
                    int disponible = Convert.ToInt32(((Label)this.uxDataLisProductos.SelectedItem.FindControl("uxLabelDispo")).Text);
                    int Cantidad = int.Parse(((TextBox)this.uxDataLisProductos.SelectedItem.FindControl("txtCantidad")).Text);
                    if (Cantidad > disponible)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('6', 'precaucion', true,false,'es-mx');", true);
                        ((TextBox)this.uxDataLisProductos.SelectedItem.FindControl("txtCantidad")).Text = "1";
                        ((TextBox)this.uxDataLisProductos.SelectedItem.FindControl("txtCantidad")).Focus();
                    }
                }

                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "2";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                }
                else
                {
                    GetEmployeeName(txtSearch.Text);
                    DataTable dtvacio = new DataTable();
                    uxDropDownListDomicilio.DataSource = dtvacio;
                    uxDropDownListDomicilio.DataBind();
                    lblLimiteCredito.Text = "0";
                    lblCreditoDisponible.Text = "0";
                    lblListaPRecio.Text = "";
                    CargarComboGrupos();

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonCancelar_Click(object sender, EventArgs e)
        {
            try
            {

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "4";
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "ModalConfirmacion();", true);
                }
                else
                {
                    Session["Grupo"] = null;
                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ASPCarroDeCompras"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;

                    CargarComboGrupos();
                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();

                    Response.Redirect("OrdenVenta");
                }


                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxButtonLimpiarFiltros_Click(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                Session["Categorias"] = null;
                Session["Departamento"] = null;
                Session["Marca"] = null;
                Session["Linea"] = null;
                Session["Familia"] = null;
                uxTextBoxCodigoBarras.Text = "";
                uxTextBoxCodigoProducto.Text = "";
                uxTextBoxDescripcion.Text = "";

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                llenarDepartamentos();
                llenarMarcas(); 
                llenarLineas();
                llenarFamilia();

                string company = this.Session["CurComp"].ToString();
                dt = ObtenerProductos(false, company, "", "", "", "", "", "", "", "", "", planta);
                if (dt.Rows.Count > 0)
                {
                    ds.Tables.Add(dt);
                    MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                }
                else
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                }
                colorFila();

                uxLabelCategoriaV.Text = "";
                Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                uxLabelCategoriaVM.Text = "";
                Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                uxLabelCategoriaL.Visible = false;
                Label uxLabelDepartamentoV = (Label)this.Master.FindControl("uxLabelDepartamentoV");
                uxLabelDepartamentoV.Text = "";
                Label uxLabelDepaL = (Label)this.Master.FindControl("uxLabelDepartamentoL");
                uxLabelDepaL.Visible = false;
                Label uxlabelMarcaVM = (Label)this.Master.FindControl("uxLabelMarcaV");
                uxlabelMarcaVM.Text = "";
                Label uxLabelMarcaL = (Label)this.Master.FindControl("uxLabelMarcaL");
                uxLabelMarcaL.Visible = false;
                Label uxlabelLineaVM = (Label)this.Master.FindControl("uxLabelLineaV");
                uxlabelLineaVM.Text = "";
                Label uxLabelLineaL = (Label)this.Master.FindControl("uxLabelLineaL");
                uxLabelLineaL.Visible = false;
                Label uxlabelFamiliaVM = (Label)this.Master.FindControl("uxLabelFamiliaV");
                uxlabelFamiliaVM.Text = "";
                Label uxLabelFamiliaL = (Label)this.Master.FindControl("uxLabelFamiliaL");
                uxLabelFamiliaL.Visible = false;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonConfirmacionCambioCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["Cambio"] != null)
                {
                    if (Session["Cambio"].ToString() == "1")
                        _accion = enumAccion.CambioGrupo;
                    else
                    if (Session["Cambio"].ToString() == "2")
                        _accion = enumAccion.CambioCliente;
                    else
                    if (Session["Cambio"].ToString() == "3")
                        _accion = enumAccion.CambioDomicilio;
                }

                ValidarCambio();

                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonConfirmacionCambioClienteCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["Grupo"] != null)
                {
                    uxDropDownListGrupo.SelectedValue = Convert.ToString(Session["Grupo"]);
                }
                if (Session["Cliente"] != null)
                {
                    uxDropDownListClientes.SelectedValue = Convert.ToString(Session["Cliente"]);
                }
                if (Session["Domicilio"] != null)
                {
                    uxDropDownListDomicilio.SelectedValue = Convert.ToString(Session["Domicilio"]);
                }

                Response.Redirect("OrdenVenta.aspx");

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonAgregarC_Click(object sender, EventArgs e)
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                actualizaCarrito();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonBuscarProducto_Click(object sender, EventArgs e)
        {
            try
            {
                string planta = this.Session["PlantID"].ToString();// 02082019
                string company = this.Session["CurComp"].ToString();
                if (uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0
                    && uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                {
                    string categoria = "";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string listaPrecios = ""; 
                    if (Session["ListaPrecios"] != null)
                    {
                        listaPrecios = Session["ListaPrecios"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }

                    DataTable dt = new DataTable();
                    dt = ObtenerProductos(true, company, categoria, listaPrecios, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text,depto, "", "", "", planta);
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                }
                else
                {
                    string categoria = "";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Session["Categorias"].ToString();
                    }
                    string depto = "";
                    if (Session["Departamento"] != null)
                    {
                        depto = Session["Departamento"].ToString();
                    }

                    DataTable dt = new DataTable();
                    dt = ObtenerProductos(false, company, categoria, "", uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text,depto, "", "", "", planta);
                    if (dt.Rows.Count > 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        MostrarDatosConPaginacion(uxDataLisProductos, ds, false);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }
                }
                colorFila();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "2";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                }
                else
                {
                    GetEmployeeName(txtSearch.Text);
                    DataTable dtvacio = new DataTable();
                    uxDropDownListDomicilio.DataSource = dtvacio;
                    uxDropDownListDomicilio.DataBind();
                    lblLimiteCredito.Text = "0";
                    lblCreditoDisponible.Text = "0";
                    lblListaPRecio.Text = "";
                    CargarComboGrupos();

                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        
    }
}