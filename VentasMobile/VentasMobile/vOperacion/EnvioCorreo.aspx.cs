﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cEntidad;
using VentasMobile.cNegocio;
using Ice.Core;
using Erp.Common;
using Erp.BO;
using Erp.Adapters;
using Ice.Lib.Framework;
using System.Web.Configuration;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

namespace VentasMobile.vOperacion
{
    public partial class EnvioCorreo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                    string company = "";
                    if (Session["CurComp"] != null)
                        company = this.Session["CurComp"].ToString();
                    string categoria = "";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Convert.ToString(Session["Categorias"]);
                        string PlantID = this.Session["PlantID"].ToString();// 02082019
                        csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                        DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                        if (catego.Rows.Count > 0)
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                        else
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = "No se encontro categoría.";
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                    }
                    if (Session["EsOrden"] != null)
                    {
                        if (Session["ListaPrecios"] != null)
                            lblListaPrecio.Text = this.Session["ListaPrecios"].ToString();
                        if (Session["DomicilioE"] != null)
                            lblDomicilioEmbarque.Text = this.Session["DomicilioE"].ToString();
                        if (Session["Cliente"] != null)
                        {
                            lblNumeroCliente.Text = this.Session["Cliente"].ToString();
                            CargarSaldosDisponible();
                        }
                        if (Session["NombreCliente"] != null)
                            lblNombreCliente.Text = this.Session["NombreCliente"].ToString();

                        if (Session["NumeroFolio"] != null)
                        {
                            lblNumeroOrden.Text = this.Session["NumeroFolio"].ToString();
                            CargarCorreos();
                            CargarDetalle();
                        }
                    }
                    else if (Session["EsCotizacion"] != null)
                    {
                        if (Session["ListaPrecios"] != null)
                            lblListaPrecio.Text = this.Session["ListaPrecios"].ToString();
                        if (Session["DomicilioE"] != null)
                            lblDomicilioEmbarque.Text = this.Session["DomicilioE"].ToString();
                        if (Session["Cliente"] != null)
                        {
                            lblNumeroCliente.Text = this.Session["Cliente"].ToString();
                            CargarSaldosDisponible();
                        }
                        if (Session["NombreCliente"] != null)
                            lblNombreCliente.Text = this.Session["NombreCliente"].ToString();

                        if (Session["NumeroFolio"] != null)
                        {
                            lblNumeroOrden.Text = this.Session["NumeroFolio"].ToString();
                            CargarCorreos();
                            CargarDetalle();
                        }
                    }
                    else
                    {
                        Response.Redirect("ordenVenta");
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                }

            }

            if (String.IsNullOrEmpty((String)Session["SessionID"]))
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("~/Acceso.aspx");
            }
        }
        private void CargarDetalle()
        {
            try
            {

                string company = Session["CurComp"].ToString();
                int numeroFolio = Convert.ToInt32(Session["NumeroFolio"].ToString());
                DataTable datos = new DataTable();
                if (Session["EsOrden"] != null)
                    if (Convert.ToBoolean(Session["EsOrden"]) == true)
                    {
                        csOrdenVentaN cOrden = new csOrdenVentaN();                        
                        datos=cOrden.ObtenerOrdenVentaPDF(this.Session["CurComp"].ToString(), Convert.ToInt32(Session["NumeroFolio"].ToString()));
                        if(datos.Rows.Count>0)
                        {
                            foreach(DataRow row in datos.Rows)
                            {
                                txtCodigoCliente.Text = row["PONum"].ToString();
                                txtComentario.Text = row["OrderComment"].ToString();
                                if (row["Consigna_c"].ToString() == "True")
                                    cboxConsigna.Checked = true;
                                txtNeedBy.Text = row["NeedByDate"].ToString();
                                txtshipBy.Text = row["RequestDate"].ToString();
                                txtshipByTime.Text = row["ShipByTime"].ToString();
                                break;
                            }
                        }
                    }
                    else
                    {
                        csCotizacionN cCotizacion = new csCotizacionN();
                        datos=cCotizacion.ObtenerCotizacion(this.Session["CurComp"].ToString(), Convert.ToInt32(Session["NumeroFolio"].ToString()));
                        if (datos.Rows.Count > 0)
                        {
                            foreach (DataRow row in datos.Rows)
                            {
                                txtCodigoCliente.Text = row["PONum"].ToString();
                                txtComentario.Text = row["QuoteComment"].ToString();
                                break;
                            }
                        }
                    }

                gvCaritoCompras.DataSource = CarroDeCompras.CapturarProducto().ListaProductos;
                gvCaritoCompras.DataBind();


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarCorreos()
        {
            try
            {
                if (this.Session["NumeroFolio"] != null)
                {

                    int numeroOrden = Convert.ToInt32(this.Session["NumeroFolio"].ToString());
                    csClientesN cCliente = new csClientesN();
                    DataTable correos = new DataTable();

                    string domicilio = Session["Domicilio"].ToString();
                    string company = Session["CurComp"].ToString();
                    string custNum = Session["Cliente"].ToString();

                    correos = cCliente.ObtenerCorreosClienteDocimicilio(custNum, domicilio, company);

                    string correoClientePrincipal = "", correosClienteContacto = "";
                    string correoDomicilioPrincipal = "", correosDomicilioContacto = "";

                    foreach (DataRow row in correos.Rows)
                    {
                        if (row["ShipToNum"].ToString() == "" && correoClientePrincipal == "")
                        {
                            correoClientePrincipal = row["Principal"].ToString() + ",";
                            if (row["Contacto"].ToString() != "")
                                correosClienteContacto = row["Contacto"].ToString() + ",";
                        }
                        else if (row["ShipToNum"].ToString() == "" && correoClientePrincipal != "")
                        {
                            correosClienteContacto += row["Contacto"].ToString() + ",";
                        }

                        if (row["ShipToNum"].ToString() != "" && row["Contacto"].ToString() != "" && correoDomicilioPrincipal == "")
                        {
                            correoDomicilioPrincipal = row["Principal"].ToString() + ",";
                            if (row["Contacto"].ToString() != "")
                                correosDomicilioContacto = row["Contacto"].ToString() + ",";
                        }
                        else if (row["ShipToNum"].ToString() != "" && correoDomicilioPrincipal != "")
                        {
                            correosClienteContacto += row["Contacto"].ToString() + ",";
                        }

                    }

                    uxTextBoxCorreos.Text = correoClientePrincipal + correosClienteContacto + correoDomicilioPrincipal + correosDomicilioContacto;

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarSaldosDisponible()
        {
            try
            {
                string cliente = lblNumeroCliente.Text;
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csSaldosN sSaldo = new csSaldosN();
                DataTable uxTabla = sSaldo.ObtenerSaldosDisponible(cliente, company);
                if (uxTabla.Rows.Count != 0)
                {
                    double saldo = Convert.ToDouble(uxTabla.Rows[0]["SaldoDisponible"].ToString());
                    double limite = Convert.ToDouble(uxTabla.Rows[0]["limite"].ToString());
                    double creditoDisponible = Convert.ToDouble(uxTabla.Rows[0]["creditoDisponible"].ToString());


                    string CreditHold = uxTabla.Rows[0]["CreditHold"].ToString();
                    if (CreditHold == "False")
                    {
                        if (creditoDisponible != 0)
                        {
                            lblCreditoDisponible.Text = creditoDisponible.ToString();

                        }
                        else
                        {
                            lblCreditoDisponible.Text = "0";
                        }
                        lblCreditoDisponible.ForeColor = Color.Black;
                    }
                    else
                    {
                        lblCreditoDisponible.Text = "Credito bloqueado";
                        lblCreditoDisponible.ForeColor = Color.Red;

                    }
                    lblLimiteCredito.Text = limite.ToString();
                }
                else
                {
                    lblLimiteCredito.Text = "0";
                    lblCreditoDisponible.Text = "0";
                }
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {
                    CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
                    double totalCarrito = 0;
                    for (int i = 0; i < cantidadCarrito; i++)
                    {
                        totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                    }
                    lblTotalPedido.Text = totalCarrito.ToString();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void colorEmpresa(string company)
        {
            try
            {
                if (company == "ADE001")
                {

                }
                else if (company == "FBD001")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "colorCompany", "cambiaColorEmpresa('" + company + "');", true);
                }
                else if (company == "BBY002")
                {

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void gvCaritoCompras_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[3].Text = "Total: " + CarroDeCompras.CapturarProducto().SubTotal().ToString("C");
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void btnEnviaCorreo_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacionEnvioCorreo');", true);
            string company = this.Session["CurComp"].ToString();
            colorEmpresa(company);
        }
        protected void uxButtonConfirmacionEnvioCorreo_Click(object sender, EventArgs e)
        {
            try
            {
                string company = Session["CurComp"].ToString();
                csGenerales generales = new csGenerales();
                if (gvCaritoCompras.Rows.Count > 0)
                {
                    int numeroFolio = Convert.ToInt32(Session["NumeroFolio"].ToString());

                    if (Session["EsOrden"] != null)
                    {
                        if (Convert.ToBoolean(Session["EsOrden"]) == true)
                        {
                            if (generales.To_pdf(numeroFolio, company)==true)
                            {
                                Session["EsOrden"] = null;
                                Session["ASPCarroDeCompras"] = null;

                                ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('centralModalSuccess');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('No se pudo generar correctamente el PDF, favor contacte al administrador del sitio.', 'error', true,false,'es-mx');", true);
                            }
                        }
                        else
                        {
                            if (generales.To_pdf(numeroFolio, company)==true)
                            {
                                Session["EsOrden"] = null;
                                Session["ASPCarroDeCompras"] = null;
                                ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('centralModalSuccess');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('No se pudo generar correctamente el PDF, favor contacte al administrador del sitio.', 'error', true,false,'es-mx');", true);
                            }
                        }

                    }
                }
                else if (Session["EsOrden"] != null | Session["EsCotizacion"] != null & Session["NumeroFolio"] != null) // 08082019
                {
                    int numeroFolio = Convert.ToInt32(Session["NumeroFolio"].ToString());
                    if (Convert.ToBoolean(Session["EsOrden"]) == true)
                    {
                        if (generales.To_pdf(numeroFolio, company)==true)
                        {
                            Session["EsOrden"] = null;
                            Session["ASPCarroDeCompras"] = null;

                            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('centralModalSuccess');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('No se pudo generar correctamente el PDF, favor contacte al administrador del sitio.', 'error', true,false,'es-mx');", true);
                        }
                    }
                    else
                    {
                        
                        if (generales.To_pdfCot(numeroFolio, company, Session["PlantID"].ToString()) ==true)
                        {
                            Session["EsOrden"] = null;
                            Session["ASPCarroDeCompras"] = null;
                            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('centralModalSuccess');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('No se pudo generar correctamente el PDF, favor contacte al administrador del sitio.', 'error', true,false,'es-mx');", true);
                        }
                    }

                }

            
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('Error al generar PDF, verifique los datos.', 'error', true,false,'es-mx');", true);
                }
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonAgregarC_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("OrdenVenta");
        }
        protected void ImgBtnTerminar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Session["EsOrden"] = null;
                Session["ASPCarroDeCompras"] = null;

                //string company = Session["CurComp"].ToString();
                //int numeroFolio = Convert.ToInt32(Session["NumeroFolio"].ToString());
                //To_pdfSinEnvio(numeroFolio, company);

                //string rutaPDF = WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Orden de venta " + company + " " + numeroFolio + ".pdf";
                //if (File.Exists(rutaPDF))
                //{
                //    Process p = new Process();
                //    ProcessStartInfo s = new ProcessStartInfo(rutaPDF);
                //    p.StartInfo = s;
                //    p.Start();
                //}

                this.Response.Redirect("OrdenVenta");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
    }
}