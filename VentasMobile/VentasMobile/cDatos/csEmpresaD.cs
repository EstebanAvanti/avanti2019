﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csEmpresaD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csEmpresaD()
        {
            conexion = new csConexionD();
        }
        #endregion

        #region Metodos

        /// <summary>
        /// Metodo Obetiene Choferes
        /// </summary>
        /// jjcome
        /// <returns></returns>
        public DataTable ObtenerEmpresa(string UserId)
        {
            try
            {
                string consulta = "select complist from Erp.UserFile where DcdUserID =  @UserId ";
                SqlParameter[] param ={                                                    
                                                    new SqlParameter("@UserId",UserId)

                                      };
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}