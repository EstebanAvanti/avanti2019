﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csDepartamentoD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csDepartamentoD()
        {
            conexion = new csConexionD();
        }
        #endregion
        public DataTable ObtenerDepartamentos(string company, string categoria)
        {
            try
            {
                if (categoria == "")
                {
                    string consulta = " select distinct p.Departamento_c, i.Character01 " +
                                      " from dbo.Part as p " +
                                      " inner join  ice.UD02 as i on p.Departamento_c = i.Key1 " +
                                      " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                      " where i.Company = @company ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    //new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
                else
                {
                    string consulta = " select distinct p.Departamento_c, i.Character01 " +
                                 " from dbo.Part as p " +
                                 " inner join  ice.UD02 as i on p.Departamento_c = i.Key1 " +
                                 " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                 " where i.Company = @company " +
                                 " AND  p.ClassID= @categoria ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerMarca(string company, string categoria)
        {
            try
            {
                if (categoria == "")
                {
                    string consulta = " select distinct p.Marca_c, i.Character01 " +
                                      " from dbo.Part as p " +
                                      " inner join  ice.UD03 as i on p.Marca_c = i.Key1 AND p.Company = i.Company " +
                                      " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                      " where i.Company = @company ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    //new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
                else
                {
                    string consulta = " select distinct p.Marca_c, i.Character01 " +
                                 " from dbo.Part as p " +
                                 " inner join  ice.UD03 as i on p.Marca_c = i.Key1 AND p.Company = i.Company " +
                                 " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                 " where i.Company = @company " +
                                 " AND  p.ClassID= @categoria ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerLinea(string company, string categoria)
        {
            try
            {
                if (categoria == "")
                {
                    string consulta = " select distinct p.linea_c, i.Character01 " +
                                      " from dbo.Part as p " +
                                      " inner join  ice.UD04 as i on p.linea_c = i.Key1 AND p.Company = i.Company " +
                                      " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                      " where i.Company = @company ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    //new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
                else
                {
                    string consulta = " select distinct p.linea_c, i.Character01 " +
                                 " from dbo.Part as p " +
                                 " inner join  ice.UD04 as i on p.linea_c = i.Key1 AND p.Company = i.Company " +
                                 " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                 " where i.Company = @company " +
                                 " AND  p.ClassID= @categoria ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerFamilia(string company, string categoria)
        {
            try
            {
                if (categoria == "")
                {
                    string consulta = " select distinct p.familia_c, i.Character01 " +
                                      " from dbo.Part as p " +
                                      " inner join  ice.UD05 as i on p.familia_c = i.Key1 AND p.Company = i.Company " +
                                      " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                      " where i.Company = @company ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    //new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
                else
                {
                    string consulta = " select distinct p.familia_c, i.Character01 " +
                                 " from dbo.Part as p " +
                                 " inner join  ice.UD05 as i on p.familia_c = i.Key1 AND p.Company = i.Company " +
                                 " inner join  Erp.PartClass as pc   on pc.Company = p.Company    and pc.ClassID = p.ClassID " +
                                 " where i.Company = @company " +
                                 " AND  p.ClassID= @categoria ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@categoria",categoria),
                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
    }
}