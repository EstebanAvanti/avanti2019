﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csClientesD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csClientesD()
        {
            conexion = new csConexionD();
        }
        #endregion

        public DataTable ObtenerDatosCliente(string company, string grup, string custNum, string user, string shipToNum)
        {
            try
            {

                string consulta = "select st.SalesRepCode,cu.TaxRegionCode,cu.TermsCode,st.ShipToNum,st.CustNum, "
                           + "cu.CustID,cu.TerritoryID,cu.Name,cu.Address1, cu.ResaleID  from erp.ShipTo as st "
                           + " left join erp.Customer as cu on cu.Company = st.Company and cu.CustNum = st.CustNum "
                           + " where st.company = @company "
                           + " and st.CustNum = @custNum "
                           + " and st.ShipToNum = @shipToNum "
                           + " and st.SalesRepCode in "
                             + " (Select SalesRepCode from erp.SaleAuth as sa "
                             + " left join erp.UserFile as uf "
                             + " on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp "
                             + " where sa.DcdUserID = @user and Company = @company)  ";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@custNum",custNum),
                                                    new SqlParameter("@shipToNum",shipToNum),
                                                    new SqlParameter("@user",user),
                                                    new SqlParameter("@grup",grup)
                                               };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerHistorialCompraPorClienteDomicilio(string custNum, string shipToNum, string company)
        {
            try
            {
                if (shipToNum != "NA")
                {
                    string consulta = "  Select 0 as secuencia, f.CustID as ClienteID, f.Cliente,f.ShipToNum as DomicilioID," +
                        "f.DomEmb as DomicilioEmbarque, f.InvoiceNum as NumeroFactura,FORMAT(f.InvoiceDate, 'yyyy-MM-dd') as InvoiceDate," +
                        "f.InvoiceAmt as ImporteFactura, f.InvoiceBal as SaldoFactura,FORMAT(f.DueDate, 'yyyy-MM-dd') as DueDate "
                            + " From( "
                                + " Select id.Company, id.CustNum, c.CustID, c.Name as Cliente, id.ShipToNum, st.Name as DomEmb, ih.OpenInvoice, ih.CreditMemo, ih.Posted, ih.InvoiceType, ih.TranDocTypeID, ih.InvoiceDate, "
                                        + " id.InvoiceNum, ih.LegalNumber, ih.InvoiceAmt, ih.DocInvoiceAmt, ih.InvoiceBal, ih.DocInvoiceBal, "
                                        + " id.InvoiceLine, id.SellingShipQty, id.PartNum, p.PartDescription, id.IUM, id.SalesUM, pc.ClassID, pc.Description as Clase, id.UnitPrice, id.DocUnitPrice, id.ExtPrice, id.DocExtPrice, "
                                        + " id.PackNum, id.PackLine, id.ShipDate, id.OrderNum, id.OrderLine,ih.DueDate "
                                + " From Erp.InvcDtl as id "
                                        + " Left Join Erp.InvcHead as ih "
                                            + " on ih.Company = id.Company "
                                            + " and ih.InvoiceNum = id.InvoiceNum "
                                            + " and ih.CustNum = ih.CustNum "
                                        + " Left Join Erp.Customer as c "
                                            + " on c.Company = id.Company "
                                            + " and c.CustNum = id.CustNum "
                                        + " Left Join Erp.ShipTo as st "
                                            + " on st.company = id.Company "
                                            + " and st.CustNum = id.CustNum "
                                            + " and st.ShipToNum = id.ShipToNum "
                                        + " Left Join Erp.Part as p "
                                            + " on p.Company = id.Company "
                                            + " and p.PartNum = id.PartNum "
                                        + " Left Join Erp.PartClass as pc "
                                            + " on pc.Company = p.Company "
                                            + " and pc.ClassID = p.ClassID) as f "
                            + " where f.Posted = 1 and f.CreditMemo = 0 and f.Company = @company and f.CustNum = @custNum and f.ShipToNum=@shipToNum "
                            + " Group by  f.CustID,f.ShipToNum,f.DomEmb, f.Cliente, f.InvoiceNum,f.InvoiceDate,f.InvoiceAmt, f.InvoiceBal,f.DueDate"
                            + " Order BY InvoiceDate  desc;";
                    SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@shipToNum",shipToNum),
                                            new SqlParameter("@company",company)
                                       };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
                else
                {
                    string consulta = "  Select 0 as secuencia, f.CustID as ClienteID, f.Cliente,f.ShipToNum as DomicilioID,f.DomEmb as DomicilioEmbarque," +
                        " f.InvoiceNum as NumeroFactura,FORMAT(f.InvoiceDate, 'yyyy-MM-dd') as InvoiceDate,f.InvoiceAmt as ImporteFactura," +
                        " f.InvoiceBal as SaldoFactura,FORMAT(f.DueDate, 'yyyy-MM-dd') as DueDate "
                            + " From( "
                                + " Select id.Company, id.CustNum, c.CustID, c.Name as Cliente, id.ShipToNum, st.Name as DomEmb, ih.OpenInvoice, ih.CreditMemo, ih.Posted, ih.InvoiceType, ih.TranDocTypeID, ih.InvoiceDate, "
                                        + " id.InvoiceNum, ih.LegalNumber, ih.InvoiceAmt, ih.DocInvoiceAmt, ih.InvoiceBal, ih.DocInvoiceBal, "
                                        + " id.InvoiceLine, id.SellingShipQty, id.PartNum, p.PartDescription, id.IUM, id.SalesUM, pc.ClassID, pc.Description as Clase, id.UnitPrice, id.DocUnitPrice, id.ExtPrice, id.DocExtPrice, "
                                        + " id.PackNum, id.PackLine, id.ShipDate, id.OrderNum, id.OrderLine,ih.DueDate "
                                + " From Erp.InvcDtl as id "
                                        + " Left Join Erp.InvcHead as ih "
                                            + " on ih.Company = id.Company "
                                            + " and ih.InvoiceNum = id.InvoiceNum "
                                            + " and ih.CustNum = ih.CustNum "
                                        + " Left Join Erp.Customer as c "
                                            + " on c.Company = id.Company "
                                            + " and c.CustNum = id.CustNum "
                                        + " Left Join Erp.ShipTo as st "
                                            + " on st.company = id.Company "
                                            + " and st.CustNum = id.CustNum "
                                            + " and st.ShipToNum = id.ShipToNum "
                                        + " Left Join Erp.Part as p "
                                            + " on p.Company = id.Company "
                                            + " and p.PartNum = id.PartNum "
                                        + " Left Join Erp.PartClass as pc "
                                            + " on pc.Company = p.Company "
                                            + " and pc.ClassID = p.ClassID) as f "
                            + " where f.Posted = 1 and f.CreditMemo = 0 and f.Company = @company and f.CustNum = @custNum   "
                            + " Group by  f.CustID,f.ShipToNum,f.DomEmb, f.Cliente, f.InvoiceNum,f.InvoiceDate,f.InvoiceAmt, f.InvoiceBal,f.DueDate"
                            + " Order BY InvoiceDate  desc;";
                    SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),                                           
                                            new SqlParameter("@company",company)
                                       };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerCorreosClienteDocimicilio(string custNum, string shipToNum, string company)
        {
            try
            {
                string consulta = "Select st.Company,st.CustNum,  c.CustID, c.Name as Cliente, st.ShipToNum, st.Name as DomEmbarque, " +
                            "ISNULL(st.EMailAddress,'') as Principal, ISNULL(cc.EMailAddress,'') as Contacto "
                            + " From Erp.ShipTo as st "
                                + " Left Join Erp.Customer as c "
                                    + " on c.Company = st.Company "
                                    + " and c.CustNum = st.CustNum "
                                + " Left Join Erp.CustCnt as cc "
                                    + " on cc.Company = st.Company "
                                    + " and cc.CustNum = st.CustNum "
                                    + " and cc.ShipToNum = st.ShipToNum "
                        + " where st.Company =@company AND st.CustNum = @custNum and st.ShipToNum in ('', @shipToNum)";
                SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@shipToNum",shipToNum),
                                            new SqlParameter("@company",company)
                                       };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerHistorialClientePorProducto(string custNum, string parNum,string codigoBarras,string shipToNum, string company)
        {
            try ///----cabos---->3 // 02082019 agregar parametro de sesion["PlantID"]
            {
                if (parNum != "")
                {
                    string consulta = "Select id.Company,CASE WHEN ih.Plant='MfgSys' THEN 'CANCUN' ELSE ih.Plant END as Plant,"
                                     + "FORMAT(ih.InvoiceDate, 'yyyy-MM-dd') as InvoiceDate, id.InvoiceNum, ih.LegalNumber, id.CustNum, c.CustID, c.Name as Cliente, st.ShipToNum, "
                                    + " st.Name as Domicilio, id.SellingShipQty, p.PartNum, p.PartDescription, p.Barras, p.ClassID, p.Clase, p.ProdCode, p.Grupo From Erp.InvcDtl as id "
                                            + " Left Join Erp.InvcHead as ih "
                                                + " on ih.Company = id.Company "
                                                + " and ih.InvoiceNum = id.InvoiceNum "
                                               + "  and ih.CustNum = id.CustNum "
                                           + "  Left Join Erp.Customer as c "
                                                + " on c.Company = id.Company "
                                               + "  and c.CustNum = id.CustNum "
                                           + "  Left Join Erp.ShipTo as st "
                                                + " on st.Company = id.Company "
                                                + " and st.ShipToNum = id.ShipToNum "
                                                + " and st.CustNum = id.CustNum "
                                           + "  Left Join(Select p.Company, p.PartNum, p.PartDescription, pg.ProdCode, pg.Description as Grupo, pc.ClassID, "
                                                + " pc.Description as Clase, ppc.ProdCode as Barras From Erp.Part as p "
                                                            + " Left Join Erp.ProdGrup as pg "
                                                                + " on p.Company = pg.Company "
                                                               + "  and p.ProdCode = pg.ProdCode "
                                                           + "  Left Join Erp.PartClass as pc "
                                                              + "   on pc.Company = p.Company "
                                                       + "  and pc.ClassID = p.ClassID "
                                                           + "  Left Join Erp.PartPC as ppc "
                                                               + "  on ppc.Company = p.Company "
                                                               + "  and ppc.PartNum = p.PartNum "
                                                               + "  and ppc.UOMCode = p.SalesUM "
                                                      + "   ) as p "
                                               + "  on p.Company = id.Company "
                                               + "  and p.PartNum = id.PartNum "
                                   + "  Where id.Company = @company and ih.Posted = 1 and id.CustNum = @custNum and id.PartNum = @parNum and id.ShipToNum in ('',@ShipToNum)  and id.InvoiceNum>10000 and SellingShipQty>0 "
                                   + " Order by ih.InvoiceDate Desc";
                    SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@parNum",parNum),
                                            new SqlParameter("@ShipToNum",shipToNum),
                                            new SqlParameter("@company",company)
                                            
                                       };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                }
                else if (codigoBarras != "")
                {

                    string consulta = "Select id.Company,CASE WHEN ih.Plant='MfgSys' THEN 'CANCUN' ELSE ih.Plant END as Plant,"
                                     + " FORMAT(ih.InvoiceDate, 'yyyy-MM-dd') as InvoiceDate, id.InvoiceNum, ih.LegalNumber, id.CustNum, c.CustID, c.Name as Cliente, st.ShipToNum, "
                                    + " st.Name as Domicilio, id.SellingShipQty, p.PartNum, p.PartDescription, p.Barras, p.ClassID, p.Clase, p.ProdCode, p.Grupo From Erp.InvcDtl as id "
                                            + " Left Join Erp.InvcHead as ih "
                                                + " on ih.Company = id.Company "
                                                + " and ih.InvoiceNum = id.InvoiceNum "
                                               + "  and ih.CustNum = id.CustNum "
                                           + "  Left Join Erp.Customer as c "
                                                + " on c.Company = id.Company "
                                               + "  and c.CustNum = id.CustNum "
                                           + "  Left Join Erp.ShipTo as st "
                                                + " on st.Company = id.Company "
                                                + " and st.ShipToNum = id.ShipToNum "
                                                + " and st.CustNum = id.CustNum "
                                           + "  Left Join(Select p.Company, p.PartNum, p.PartDescription, pg.ProdCode, pg.Description as Grupo, pc.ClassID, "
                                                + " pc.Description as Clase, ppc.ProdCode as Barras From Erp.Part as p "
                                                            + " Left Join Erp.ProdGrup as pg "
                                                                + " on p.Company = pg.Company "
                                                               + "  and p.ProdCode = pg.ProdCode "
                                                           + "  Left Join Erp.PartClass as pc "
                                                              + "   on pc.Company = p.Company "
                                                       + "  and pc.ClassID = p.ClassID "
                                                           + "  Left Join Erp.PartPC as ppc "
                                                               + "  on ppc.Company = p.Company "
                                                               + "  and ppc.PartNum = p.PartNum "
                                                               + "  and ppc.UOMCode = p.SalesUM "
                                                      + "   ) as p "
                                               + "  on p.Company = id.Company "
                                               + "  and p.PartNum = id.PartNum "
                                   + "  Where id.Company = @company and ih.Posted = 1 and id.CustNum = @custNum and Barras = @Barras and id.ShipToNum in ('',@ShipToNum)  and id.InvoiceNum>10000 and SellingShipQty>0 "
                                    + " Order by ih.InvoiceDate Desc ";
                    SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@Barras",codigoBarras),
                                            new SqlParameter("@ShipToNum",shipToNum),
                                            new SqlParameter("@company",company)
                                            
                                       };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                }
                else 
                {

                    string consulta = "Select id.Company,CASE WHEN ih.Plant='MfgSys' THEN 'CANCUN' ELSE ih.Plant END as Plant,"
                                     + " FORMAT(ih.InvoiceDate, 'yyyy-MM-dd') as InvoiceDate, id.InvoiceNum, ih.LegalNumber, id.CustNum, c.CustID, c.Name as Cliente, st.ShipToNum, "
                                    + " st.Name as Domicilio, id.SellingShipQty, p.PartNum, p.PartDescription, p.Barras, p.ClassID, p.Clase, p.ProdCode, p.Grupo From Erp.InvcDtl as id "
                                            + " Left Join Erp.InvcHead as ih "
                                                + " on ih.Company = id.Company "
                                                + " and ih.InvoiceNum = id.InvoiceNum "
                                               + "  and ih.CustNum = id.CustNum "
                                           + "  Left Join Erp.Customer as c "
                                                + " on c.Company = id.Company "
                                               + "  and c.CustNum = id.CustNum "
                                           + "  Left Join Erp.ShipTo as st "
                                                + " on st.Company = id.Company "
                                                + " and st.ShipToNum = id.ShipToNum "
                                                + " and st.CustNum = id.CustNum "
                                           + "  Left Join(Select p.Company, p.PartNum, p.PartDescription, pg.ProdCode, pg.Description as Grupo, pc.ClassID, "
                                                + " pc.Description as Clase, ppc.ProdCode as Barras From Erp.Part as p "
                                                            + " Left Join Erp.ProdGrup as pg "
                                                                + " on p.Company = pg.Company "
                                                               + "  and p.ProdCode = pg.ProdCode "
                                                           + "  Left Join Erp.PartClass as pc "
                                                              + "   on pc.Company = p.Company "
                                                       + "  and pc.ClassID = p.ClassID "
                                                           + "  Left Join Erp.PartPC as ppc "
                                                               + "  on ppc.Company = p.Company "
                                                               + "  and ppc.PartNum = p.PartNum "
                                                               + "  and ppc.UOMCode = p.SalesUM "
                                                      + "   ) as p "
                                               + "  on p.Company = id.Company "
                                               + "  and p.PartNum = id.PartNum "
                                   + "  Where id.Company = @company and ih.Posted = 1 and id.CustNum = @custNum and id.ShipToNum in ('',@ShipToNum)  and id.InvoiceNum>10000 and SellingShipQty>0 "
                                   +" Order by ih.InvoiceDate Desc ";
                    SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@ShipToNum",shipToNum),
                                            new SqlParameter("@company",company)
                                            
                                       };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                }
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerDatosClienteEmpresa(string custNum, string company)
        {
            try
            {
                string consulta = "select cu.CustNum, cu.Name, cu.ResaleID, co.Company, co.Name as Empresa,co.Address1,co.City, co.State, co.Zip, co.StateTaxID "
                        + "  from erp.Customer cu "
                        + "inner join erp.Company co on cu.Company = co.Company "
                        + " where cu.Company = @company and cu.CustNum = @custNum ";

                SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@company",company)
                                       };
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }        

        public DataTable ObtenerDatosClienteAuto(string company, string user, string custNum)
        {
            try
            {
                string consulta = "Select cu.CustNum, cu.CustID+' - '+cu.name as Name " +
                        " from Erp.ShipTo st " +
                        "  left join Erp.Customer cu   on cu.CustNum = st.CustNum and cu.Company = st.Company " +
                        "  left join erp.CustomerPriceLst cpl on cpl.CustNum = cu.CustNum  and cpl.Company = cu.Company " +
                        "  left join erp.PriceLst pl on pl.Company = cpl.Company and pl.ListCode = cpl.ListCode " +
                        "  left join Erp.CustGrup cg on cg.GroupCode = cu.GroupCode  and cg.Company = cu.Company " +
                        "  left join  Erp.SalesRep sr on sr.SalesRepCode = st.SalesRepCode and sr.Company = st.Company " +
                        "  where cg.Company = @company " +
                        "and pl.EndDate >= GETDATE() and cu.CustID like" +"'%" +@custNum +"%'"+ "and " +
                        "  st.SalesRepCode in " +
                        "  (Select SalesRepCode from erp.SaleAuth as sa " +
                         " left join erp.UserFile as uf " +
                         " on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp " +
                         " where sa.DcdUserID = @user and Company = @company and cu.CustID like" + "'%" + @custNum + "%'"+")  " +
                        " group by cu.CustNum, cu.Name,cu.CustID ";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@custNum",custNum),
                                                    new SqlParameter("@user",user)
                                               };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
    }
}