﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csVendedorD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csVendedorD()
        {
            conexion = new csConexionD();
        }
        #endregion


        public DataTable ObtenerEmailVendedor( string company,string vendedor)
        {
            try
            {
                string consulta = "Select sa.SalesRepCode,sr.SalesRepCode,isnull(sr.EMailAddress,'') as EMailAddressSR,isnull(uf.EMailAddress,'') as EMailAddress,sa.DcdUserID "
                       + " from erp.SaleAuth as sa "
                       + " left join erp.UserFile as uf  on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp "
                       + " left join erp.SalesRep as sr on sr.Company = sa.Company and sr.SalesRepCode = sa.SalesRepCode "
                       + " where sa.DcdUserID = @vendedor and sa.Company = @company;  ";

                SqlParameter[] param ={
                                            new SqlParameter("@vendedor",vendedor),
                                            new SqlParameter("@company",company)
                                       };
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
    }
}