﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;


namespace VentasMobile.cDatos
{
    public class csListaPreciosD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        //string company = WebConfigurationManager.AppSettings["company"];
        public csListaPreciosD()
        {
            conexion = new csConexionD();
        }
        #endregion
        #region Metodos
        public DataTable ObtenerListaPrecios(string company, string CustID, string Grup)
        {
            try
            {
                string consulta = " select * from erp.CustomerPriceLst cpl " +
                                  " inner join Erp.Customer c on cpl.CustNum = c.CustNum  and cpl.Company = c.Company " +
                                  " inner join erp.CustGrup cg on cg.Company = c.Company and cg.GroupCode = c.GroupCode " +
                                  " WHERE " +
                                  " Company= @company " +
                                  " AND CustID= @CustID " +
                                  " AND cg.GroupCode =  @Grup ";
                SqlParameter[] param ={                                                   
                                                    new SqlParameter("@Company",company),
                                                    new SqlParameter("@CustID",CustID),
                                                    new SqlParameter("@Grup",Grup)
                                      };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        // lista de precios por clienete 23012019
        public DataTable ObtenerListaPreciosByClienet(string company, string CustNun, string Grup)
        {
            try
            {
                string consulta = "select top 1 pl.ListCode,cpl.CustNum,c.Name from erp.CustomerPriceLst cpl " +
                                  " inner join erp.PriceLst pl on cpl.Company = pl.Company  AND cpl.ListCode = pl.ListCode " +
                                  " inner join erp.Customer as c on c.Company = cpl.Company and c.CustNum = cpl.CustNum " +
                                  " where " +
                                  " cpl.Company = @Company " +
                                  " AND cpl.CustNum = @CustID " +
                                  " AND cpl.ShipToNum = '' and EndDate >= GETDATE() " +
                                  " order by SeqNum asc ";
                                  
                SqlParameter[] param ={
                                                    new SqlParameter("@Company",company),
                                                    new SqlParameter("@CustID",CustNun)
                                      };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        // precio lista por domicilio de embarque
        public DataTable ObtenerListaPreciosByDomicilio(string company, string CustNun, string domi)
        {
            try
            {
                string consulta = "select top 1 pl.ListCode,cpl.CustNum,c.Name from erp.CustomerPriceLst cpl " +
                                  " inner join erp.PriceLst pl on cpl.Company = pl.Company  AND cpl.ListCode = pl.ListCode " +
                                  " inner join erp.Customer as c on c.Company = cpl.Company and c.CustNum = cpl.CustNum " +
                                  " where " +
                                  " cpl.Company = @Company " +
                                  " AND cpl.CustNum = @CustID " +
                                  " AND cpl.ShipToNum = @domi and EndDate >= GETDATE() " +
                                  " order by SeqNum asc ";

                SqlParameter[] param ={
                                                    new SqlParameter("@Company",company),
                                                    new SqlParameter("@CustID",CustNun),
                                                   new SqlParameter("@domi",domi)
                                      };
                conexion.ConsultaReturnTabla(consulta, param, 2);

                if (conexion.Table.Rows.Count > 0)
                {
                    return conexion.Table;
                }
                else
                {
                    string consulta2 = "select top 1 pl.ListCode,cpl.CustNum,c.Name from erp.CustomerPriceLst cpl  "
                                   + " inner join erp.PriceLst pl on cpl.Company = pl.Company  AND cpl.ListCode = pl.ListCode "
                                   + " inner join erp.Customer as c on c.Company = cpl.Company and c.CustNum = cpl.CustNum "
                                   + " where "
                                   + " cpl.Company = @Company "
                                   + " AND cpl.CustNum = @CustID "
                                   + " and EndDate >= GETDATE() "
                                   + " AND cpl.ShipToNum = '' "
                                   + " order by SeqNum asc";
                    SqlParameter[] param2 ={
                                                    new SqlParameter("@Company",company),
                                                    new SqlParameter("@CustID",CustNun),
                                                   new SqlParameter("@domi",domi)
                                      };
                    conexion.ConsultaReturnTabla(consulta2, param2, 2);

                    return conexion.Table;
                }
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
 }