﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{

    public class csSaldosD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csSaldosD()
        {
            conexion = new csConexionD();
        }
        #endregion
        #region Metodos

        /// <summary>
        /// Metodo Obetiene Choferes
        /// </summary>
        /// jjcome
        /// <returns></returns>
        public DataTable ObtenerSaldosXfactura(string custNum, string shipToNum, string company)
        {
            try
            {
                string consulta = " Select f.Cliente,f.DomEmb as 'Domicilio embarque', f.InvoiceNum as 'Numero factura'," +
                    "FORMAT(f.InvoiceDate, 'yyyy-MM-dd') as 'Fecha factura',FORMAT(f.DueDate, 'yyyy-MM-dd') as 'Fecha vencimiento',f.InvoiceAmt as Importe, f.InvoiceBal as 'Saldo factura' "
                        + " From( "
                            + " Select id.Company, id.CustNum, c.CustID, c.Name as Cliente, id.ShipToNum, st.Name as DomEmb, ih.OpenInvoice, ih.CreditMemo, ih.Posted, ih.InvoiceType, ih.TranDocTypeID, ih.InvoiceDate, "
                                    + " id.InvoiceNum, ih.LegalNumber, ih.InvoiceAmt, ih.DocInvoiceAmt, ih.InvoiceBal, ih.DocInvoiceBal, "
                                    + " id.InvoiceLine, id.SellingShipQty, id.PartNum, p.PartDescription, id.IUM, id.SalesUM, pc.ClassID, pc.Description as Clase, id.UnitPrice, id.DocUnitPrice, id.ExtPrice, id.DocExtPrice, "
                                    + " id.PackNum, id.PackLine, id.ShipDate, id.OrderNum, id.OrderLine,ih.DueDate "
                            + " From Erp.InvcDtl as id "
                                    + " Left Join Erp.InvcHead as ih "
                                        + " on ih.Company = id.Company "
                                        + " and ih.InvoiceNum = id.InvoiceNum "
                                        + " and ih.CustNum = ih.CustNum "
                                    + " Left Join Erp.Customer as c "
                                        + " on c.Company = id.Company "
                                        + " and c.CustNum = id.CustNum "
                                    + " Left Join Erp.ShipTo as st "
                                        + " on st.company = id.Company "
                                        + " and st.CustNum = id.CustNum "
                                        + " and st.ShipToNum = id.ShipToNum "
                                    + " Left Join Erp.Part as p "
                                        + " on p.Company = id.Company "
                                        + " and p.PartNum = id.PartNum "
                                    + " Left Join Erp.PartClass as pc "
                                        + " on pc.Company = p.Company "
                                        + " and pc.ClassID = p.ClassID) as f "
                        + " where f.Posted = 1 and f.CreditMemo = 0 and f.InvoiceBal > 0 and f.Company = @company and f.CustNum = @custNum and f.ShipToNum=@shipToNum "
                        + " Group by  f.CustID,f.ShipToNum,f.DomEmb, f.Cliente, f.InvoiceNum,f.InvoiceDate,f.InvoiceAmt, f.InvoiceBal,f.DueDate "
                        + " ORDER BY  f.InvoiceDate DESC ;";
                SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@shipToNum",shipToNum),
                                            new SqlParameter("@company",company)
                                       };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerSaldosDisponible(string custNum, string company)
        {
            try
            {
                string consulta = "Select c.Company, c.CustID, c.CustNum, c.Name, c.GroupCode, g.GroupDesc,c.CreditHold, c.CreditLimit as limite "
                     + " , isnull(s.SaldoFacturas + "
                     + " (select isnull(sum(SaldoOrdenes), 0) as SaldoOrdenes "
                     + " from "
                     + " ( select ExtPriceDtl + (select SUM(TaxAmt) from erp.OrderRelTax as ort "
                     + " where ort.Company = odtl.Company and ort.OrderNum = odtl.OrderNum and ort.OrderLine = odtl.OrderLine) as SaldoOrdenes "
                     + " from erp.OrderDtl  as odtl "
                     + " left join erp.OrderHed as oh on oh.Company = odtl.Company and oh.OrderNum = odtl.OrderNum "
                     + " where odtl.Company = c.Company and oh.CustNum = c.CustNum and oh.OpenOrder = 1 and odtl.OpenLine = 1 "
                     + " ) as saldo),0)as SaldoDisponible , "
                     + " isnull(c.CreditLimit - (isnull(s.SaldoFacturas, 0) + (select isnull(sum(SaldoOrdenes), 0) "
                     + " from (select ExtPriceDtl + (select SUM(TaxAmt) from erp.OrderRelTax as ort "
                     + " where ort.Company = odtl.Company and ort.OrderNum = odtl.OrderNum and ort.OrderLine = odtl.OrderLine) as SaldoOrdenes "
                     + " from erp.OrderDtl  as odtl "
                     + " left join erp.OrderHed as oh on oh.Company = odtl.Company and oh.OrderNum = odtl.OrderNum "
                     + " where odtl.Company = c.Company and oh.CustNum = c.CustNum and oh.OpenOrder = 1 and odtl.OpenLine = 1) as saldo)),0) as creditoDisponible "
                     + " From Erp.Customer as c "
                         + " Left Join( "
                                 + " Select f.Company, f.CustNum, sum (InvoiceBal) as SaldoFacturas FRom Erp.InvcHead as f "
                                 + " where Posted = 1 and CreditMemo = 0 "
                                 + " group by f.Company, f.CustNum) as s  on s.Company = c.Company and s.CustNum = c.CustNum "
                         + " Left Join Erp.CustGrup as g on g.Company = c.Company and g.GroupCode = c.GroupCode "
                         + " Where c.Company=@company and c.CustNum=@custNum;";
                SqlParameter[] param ={
                                            new SqlParameter("@custNum",custNum),
                                            new SqlParameter("@company",company)
                                       };
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}