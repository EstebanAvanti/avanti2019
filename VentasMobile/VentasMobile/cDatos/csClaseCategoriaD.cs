﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csClaseCategoriaD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csClaseCategoriaD()
        {
            conexion = new csConexionD();
        }
        #endregion

        public DataTable ObtenerCategorias(string company, string PlantID)
        {
            try
            {
                // 02082019 se agrega el parametro PlantID, para que reciba el de la sesion 
                string consulta = "  select pc.ClassID, pc.Description from Erp.PartClass as pc "
                           + " left Join  erp.Part as p "
                           + " on pc.Company = p.Company "
                           + " and pc.ClassID = p.ClassID "
                           + " where pc.Company = @company  and pc.ClassID in  "
                           + " (select ClassID from erp.PartClassPlt where Company = pc.Company  and OwnerPlant = @PlantID)  "
                           + " group by pc.ClassID, pc.Description order by pc.Description;";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@PlantID",PlantID)

                                      };

                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }


        public DataTable ObtenerCategoriaID(string company,string classID, string PlantID)
        {
            try // si es planta de cancun se excluye los de CABOS si no alreves
            {

                string consulta = "  select pc.ClassID, pc.Description from erp.Part as p " +
                        " inner Join Erp.PartClass as pc " +
                        " on pc.Company = p.Company " +
                        " and pc.ClassID = p.ClassID " +
                    " where  pc.ClassID=@classID and pc.Company = @Company and pc.ClassID in " +
                    "(select ClassID from erp.PartClassPlt where Company=pc.Company and OwnerPlant=@PlantID)" +
                        " group by pc.ClassID, pc.Description";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@classID",classID),
                                                    new SqlParameter("@PlantID",PlantID)

                                      };

                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

    }
}