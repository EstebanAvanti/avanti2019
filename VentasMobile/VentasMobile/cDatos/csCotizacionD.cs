﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
  
    public class csCotizacionD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csCotizacionD()
        {
            conexion = new csConexionD();
        }
        #endregion
        public DataTable ObtenerCotizacion(string company, int quoteNum)
        {
            try
            {
                string consulta = " select  oh.QuoteNum,oh.PONum,oh.QuoteComment, " +
                                  " oh.CustNum,oh.ShipToNum,st.Name as Domicilio,oh.ECCSalesRepID,sl.SalesRepCode," +
                                  " oh.ChangedBy as Vendedor, " +
                                  " od.PartNum,pp.ProdCode as codigoBarras,  " +
                                  "  oh.DueDate, oh.Company,od.PartNum as Clave,od.LineDesc as Descripcion, " +
                                  " od.OrderQty as Cantidad, od.ListPrice as Precio, od.ExtPriceDtl as Total " +
                                  " ,oh.DocTotalGrossValue as Subtotal,oh.DocTotalTax as Impuestos, oh.QuoteAmt TotalFinal " +
                                  " , oh.CurrencyCode as Moneda, od.SellingExpectedUM as Unidad " +
                                  " from QuoteHed oh " +
                                  " left join erp.QuoteDtl od on od.Company = oh.Company and od.QuoteNum = oh.QuoteNum " +
                                  " left join erp.ShipTo as st on st.Company = oh.Company and st.ShipToNum = oh.ShipToNum and st.CustNum = oh.CustNum "+
                                  " left join erp.SalesRep as sl on sl.Company = oh.Company and sl.SalesRepCode = oh.ECCSalesRepID "+
                                  " left join erp.PartPC as pp on pp.Company = od.Company and pp.PartNum = od.PartNum " +
                                  "  where oh.Company = @company " +
                                  " And   oh.QuoteNum= @orderNum " +
                                   " And oh.Mobile_c=1 ;"; //---------> agregado 25092019
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@orderNum",quoteNum),
                                                    
                                               };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        public bool ActualizaVentasMobile(string company, int QuoteNum)
        {
            try
            {
                string consulta = "update erp.QuoteHed_UD set Mobile_c=1 where " +
                    " ForeignSysRowID=(select SysRowID from erp.QuoteHed where " +
                    " company=@company and QuoteNum=@QuoteNum);";
                SqlParameter[] param ={
                                                     new SqlParameter("@company",company),
                                                    new SqlParameter("@QuoteNum",QuoteNum)
                                               };
                return conexion.ConsultaInsertUpdate(consulta, param, 2);

            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerCotizacionXcliente(string company, int cliente)
        {
            try
            {
                string consulta = " select    FORMAT(oh.DueDate, 'yyyy-MM-dd') as DueDate, oh.QuoteNum, "+
                                  " oh.CustNum,oh.ShipToNum,st.Name as Domicilio "+
                                  " from QuoteHed oh " +                  
                                  " join erp.ShipTo as st on st.Company = oh.Company and st.ShipToNum = oh.ShipToNum and st.CustNum = oh.CustNum "+
                                  " left join erp.SalesRep as sl on sl.Company = oh.Company and sl.SalesRepCode = oh.ECCSalesRepID " +
                                  "  where oh.Company = @company " +
                                  " And oh.CustNum= @CustNum"+
                                  " And oh.Mobile_c=1 " ; //-------> agregado 25092019
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@CustNum",cliente),

                                               };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
    }

}