﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csOrdenVentaD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csOrdenVentaD()
        {
            conexion = new csConexionD();
        }
        #endregion

        public bool ActualizaVendedorEnOrdenVenta(string company, int OrderNum, string SalesRep)
        {
            try
            {
                string consulta = "update erp.OrderHed set SalesRepList=@SalesRep,RepSplit1=100 where Company=@company and OrderNum=@OrderNum;";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@OrderNum",OrderNum),
                                                    new SqlParameter("@SalesRep",SalesRep)
                                               };
                return conexion.ConsultaInsertUpdate(consulta, param, 2);
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaConsignaOrdenVenta(string company, int OrderNum)
        {
            try
            {
                string consulta = "update erp.OrderHed_UD set Consigna_c=1 where ForeignSysRowID=(select SysRowID from erp.OrderHed where company=@company and OrderNum=@OrderNum);";
                SqlParameter[] param ={

                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@OrderNum",OrderNum)
                                               };
                return conexion.ConsultaInsertUpdate(consulta, param, 2);
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaVentasMobile(string company, int OrderNum)
        {
            try
            {
                string consulta = "update erp.OrderHed_UD set Mobile_c=1 where ForeignSysRowID=(select SysRowID from erp.OrderHed where company=@company and OrderNum=@OrderNum);";
                SqlParameter[] param ={
                                                     new SqlParameter("@company",company),
                                                    new SqlParameter("@OrderNum",OrderNum)
                                               };
                return conexion.ConsultaInsertUpdate(consulta, param, 2);

            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaReadyToCal(string company, int OrderNum)
        {
            try
            {
                string consulta = "update erp.OrderHed set ReadyToCalc=1 where Company=@company and OrderNum=@OrderNum;";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@OrderNum",OrderNum)
                                               };
                return conexion.ConsultaInsertUpdate(consulta, param, 2);
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaPlantaEnLineasOV(string company, int OrderNum,string planta,string almacen)
        {
            try
            {
                string consulta = " update erp.OrderRel set Plant=@Planta,WarehouseCode=@Almacen  where " +
                        " Company=@company and OrderNum=@OrderNum;";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@OrderNum",OrderNum),
                                                    new SqlParameter("@Planta",planta),
                                                    new SqlParameter("@Almacen",almacen)
                                               };
                return conexion.ConsultaInsertUpdate(consulta, param, 2);
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerOrdenVentaPDF(string company, int orderNum)
        {
            try
            {
                string consulta = " select  oh.OrderNum,oh.PONum,oh.OrderComment,c.Consigna_c,CASE WHEN Plant = 'MfgSys' then 'CANCUN' else ISNULL(plant, '') END as Sucursal , " +
                                 " oh.CustNum,oh.ShipToNum,st.Name as Domicilio,oh.SalesRepList,sl.SalesRepCode,sl.Name as Vendedor, " +
                                 " od.PartNum,pp.ProdCode as codigoBarras,od.SalesUM as Unidad," +
                                 " oh.EntryPerson,  oh.OrderDate,FORMAT(oh.RequestDate, 'yyyy-MM-dd') as RequestDate," +
                                 " FORMAT(oh.NeedByDate, 'yyyy-MM-dd') as NeedByDate,ice.StringTime(oh.ShipByTime,'hh:mm:ss') as ShipByTime, " +
                                 " oh.Company,od.PartNum as Clave,od.LineDesc as Descripcion, od.OrderQty as Cantidad, od.UnitPrice as Precio, od.ExtPriceDtl as Total " +
                                 " ,oh.DocTotalCharges as Subtotal,oh.DocTotalTax as Impuestos, oh.DocOrderAmt TotalFinal, oh.PSCurrCode as Moneda " +
                                 " from erp.OrderHed oh " +
                                 " left join  erp.OrderDtl od on od.Company = oh.Company and od.OrderNum = oh.OrderNum " +
                                 " left join erp.ShipTo as st on st.Company = oh.Company and st.ShipToNum = oh.ShipToNum and st.CustNum = oh.CustNum " +
                                 " left join erp.SalesRep as sl on sl.Company = oh.Company and sl.SalesRepCode = oh.SalesRepList " +
                                 " left join erp.PartPC as pp on pp.Company = od.Company and pp.PartNum = od.PartNum " +
                                 " left join erp.OrderHed_UD  as c on c.ForeignSysRowID=oh.SysRowID " +
                                 " where oh.Company = @company " +
                                 " And  oh.OrderNum = @orderNum;";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@orderNum",orderNum),

                                               };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerOrdenVentaXcliente(string company, int Cliente)
        {
            try
            {
                string consulta = "select  FORMAT( oh.OrderDate, 'yyyy-MM-dd') as  OrderDate, oh.OrderNum,oh.CustNum, cu.Name , oh.ShipToNum, " +
                                 " CASE WHEN Plant = 'MfgSys' then 'CANCUN' else ISNULL(plant, '') END as Sucursal " +
                                 " ,st.Name as Domicilio,oh.SalesRepList,sl.SalesRepCode,sl.Name as Vendedor " +
                                 "  from erp.OrderHed oh " +
                                 " left " +
                                 " join erp.ShipTo as st on st.Company = oh.Company and st.ShipToNum = oh.ShipToNum and st.CustNum = oh.CustNum " +
                                 "left join erp.SalesRep as sl on sl.Company = oh.Company and sl.SalesRepCode = oh.SalesRepList " +
                                 " left join erp.OrderHed_UD as c on c.ForeignSysRowID = oh.SysRowID " +
                                 "  left join Erp.Customer cu on cu.CustNum = oh.CustNum and cu.Company = oh.Company " +
                                 "  where c.Mobile_c = '1' AND oh.Company = @company " +
                                 " AND oh.CustNum = @cliente " +
                                 " group by oh.OrderDate, oh.OrderNum,oh.CustNum, cu.Name, oh.ShipToNum, oh.PONum,oh.OrderComment,c.Consigna_c,Plant " +
                                 "  ,st.Name ,oh.SalesRepList,sl.SalesRepCode,sl.Name , " +
                                 "  oh.EntryPerson, oh.OrderDate, oh.Company " +
                                 "  ,oh.DocTotalCharges ,oh.DocTotalTax, oh.DocOrderAmt , oh.PSCurrCode "+
                                 " Order by oh.OrderDate desc";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@cliente",Cliente),

                                               };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
    }
}