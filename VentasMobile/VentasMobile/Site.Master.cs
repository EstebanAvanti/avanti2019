﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cEntidad;

namespace VentasMobile
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string company = "";
            if (this.Session["CurComp"] != null)
                company = this.Session["CurComp"].ToString();
            if (!IsPostBack)
            {
                //Se valida que si no esta logeado
                if (String.IsNullOrEmpty((String)Session["SessionID"]))
                {
                    Session.RemoveAll();
                    Session.Abandon();
                    Response.Redirect("~/Acceso.aspx");
                }
                else
                {
                    int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                    lblCantidadCarrito.Text = cantidadCarrito.ToString();
                    uxLabelUsuario.Text = this.Session["Name"].ToString();
                    uxLabelCompañia.Text = company;

                    if (company == "ADE001")
                    {
                        accordionSidebar.Attributes["class"] = "navbar-nav sidebar bg-gradient-av accordion sidebar-master toggled-menu";
                        uxImageButtonLogoEmpresa.ImageUrl = "~/images/png/avanti.jpg";
                    }
                    else if (company == "FBD001")
                    {
                        accordionSidebar.Attributes["class"] = "navbar-nav sidebar bg-gradient-fb accordion sidebar-master toggled-menu";
                        ScriptManager.RegisterStartupScript(this, GetType(), "", "cambiaColorEmpresa('" + company + "');", true);
                        uxImageButtonLogoEmpresa.ImageUrl = "~/images/png/FB-Inicio.png";
                    }
                    else if (company == "BBY002")
                    {
                        accordionSidebar.Attributes["class"] = "navbar-nav sidebar bg-gradient-bb accordion sidebar-master toggled-menu";
                        uxImageButtonLogoEmpresa.ImageUrl = "~/images/png/BB-Inicio.png";
                    }
                }
            }
            if (company == "ADE001")
            {

            }
            else if (company == "FBD001")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "colorCompany", "cambiaColorEmpresa('" + company + "');", true);
            }
            else if (company == "BBY002")
            {

            }

            if (Session["Categorias"] == null)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "ocultarNavFiltros", "ocultarNavFiltros('navFiltros');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "mostrarNavFiltros", "mostrarNavFiltros('navFiltros');", true);
            }
        }

        protected void Inicio_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/vOperacion/Inicio");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void messagesDropdown_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/vOperacion/DetalleOrdenVenta");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void linkPedidos_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/vOperacion/OrdenVenta");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void linkClientes_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/vCatalogo/Clientes");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void linkInventarios_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/vCatalogo/Inventario");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void linkReportes_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/vCatalogo/Reportes");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        
        protected void Salir_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/vOperacion/Salir");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        

        protected void uxImageButtonLogoEmpresa_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect("~/vOperacion/Inicio");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
    }
}