﻿//// Funciones Js, Jesus Jacome
/// Fecha de creacion 06/03/2019

function mostrarMensaje(mensaje, tipo, cerrar, limpiarCampos, idioma) {
    var cuerpoMensaje, mensaje, tipo;
    switch (idioma) {
        case 'es-mx':
            switch (tipo) {
                case 'error':
                    switch (mensaje) {
                        case '1':
                            mensaje = 'No se guardaron los datos.';
                            break;
                        case '2':
                            mensaje = 'Al realizar proceso de actualización de datos.';
                            break;
                        case '3':
                            mensaje = 'Problema en el envío de correo.';
                            break;
                        default:
                            mensaje = mensaje + ' Favor contacte al administrador del sitio.';
                    }
                        cuerpoMensaje ='<div class="modal-content" >' +
                            '<div class="modal-header bg-danger ">' +
                            '<p class="text-center mb-0 text-white ml-2 font-weight-bold w-100">¡Error!</p>' +
                            '<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '</div>' +
                            '<div class="modal-body">' +
                                '<div class="cuerpoMensaje msj_error"> <p class="text-justify mb-0"><img src="../images/menuIcons/Exclamation_opt.png" class="mr-2"  />' + mensaje + '</p></div>' +
                            '</div>' +
                            '</div >';
                    break;
                case 'correcto':
                    switch (mensaje) {
                        case '1':
                            mensaje = 'Se guardó correctamente.';
                            break;
                        case '2':
                            mensaje = 'Se actualizó correctamente.';
                            break;
                        case '3':
                            mensaje = 'Se eliminó correctamente.';
                            break;
                        case '4':
                            mensaje = 'Correo enviado correctamente';
                            break;
                    }
                    cuerpoMensaje = '<div class="modal-content" >' +
                        '<div class="modal-header bg-success ">' +
                        '<p class="text-center mb-0 text-white ml-2 font-weight-bold w-100">¡Correcto!</p>' +
                        '<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        '<div class="cuerpoMensaje msj_error"> <p class="text-justify mb-0 text-center m-auto"><img src="../images/menuIcons/Accept_opt.png" class="mr-2"  />' + mensaje + '</p></div>' +
                        '</div>' +
                        '</div >';
                    break;
                case 'precaucion':
                    switch (mensaje) {
                        case '1':
                            mensaje = 'Faltan datos por llenar, favor verifique.';
                            break;
                        case '2':
                            mensaje = 'El formato no es el correcto.';
                            break;
                        case '3':
                            mensaje = 'El carrito de compras ya está vacío.';
                            break;
                        case '4':
                            mensaje = 'No se puede agregar producto con precio 0, porfavor verifique.';
                            break;
                        case '5':
                            mensaje = 'No se puede agregar producto con existencia 0, porfavor verifique.';
                            break;
                        case '6':
                            mensaje = 'No se puede agregar producto con cantidad mayor a la existencia, porfavor verifique.';
                            break;
                        case '7':
                            mensaje = 'Una cotización no puede ser consigna, favor verifque los datos.';
                            break;
                        case '8':
                            mensaje = 'El precio total es superior al limite de credito disponible. Se eliminará el producto, debe agregarlo nuevamente.';
                            break;
                        case '9':
                            mensaje = 'Seleccione un cliente que tenga lista de precios.';
                            break;
                        case '10':
                            mensaje = 'Debe seleccionar Grupo, Cliente y Domicilio antes de agregar productos.';
                            break;
                        case '11':
                            mensaje = 'Credito bloqueado. No se puede agregar producto.';
                            break;
                        case '12':
                            mensaje = 'La fecha requerida del cliente debe ser mayor a la fecha actual.';
                            break;
                        case '13':
                            mensaje = 'La fecha de embarque debe ser mayor o igual a la fecha actual.';
                            break;
                    }
                    cuerpoMensaje = '<div class="modal-content" >' +
                        '<div class="modal-header bg-warning ">' +
                        '<p class="text-center mb-0 text-white ml-2 font-weight-bold w-100">¡Precaución!</p>' +
                        '<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        '<div class="cuerpoMensaje msj_error"> <p class="text-justify mb-0"><img src="../images/menuIcons/Error_opt.png" class="mr-2"  />' + mensaje + '</p></div>' +
                        '</div>' +
                        '</div >';
                    break;
                case 'informacion':
                    switch (mensaje) {
                        case '1':
                            mensaje = 'No se encontraron registros.';
                            break;
                        case '2':
                            mensaje = 'El carrito de compras se vacío correctamente.';
                            break;
                        case '3':
                            mensaje = 'No se encontro reporte para mostrar.';
                            break;
                    }
                    cuerpoMensaje = '<div class="modal-content" >' +
                        '<div class="modal-header bg-info ">' +
                        '<p class="text-center mb-0 text-white ml-2 font-weight-bold w-100">Información</p>' +
                        '<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        '<div class="cuerpoMensaje msj_error"> <p class="text-justify mb-0 text-center m-auto"><img src="../images/menuIcons/Information_opt.png" class="mr-2" />' + mensaje + '</p></div>' +
                        '</div>' +
                        '</div >';
                    break;
                default: cuerpoMensaje = mensaje;
                    break;
            }
            break;
        case 'en':
            switch (tipo) {
            }
            break;

    }
    document.getElementById("DivMensaje").innerHTML = cuerpoMensaje.toString();
    $(".Mensajes").show("fast");    
    $('#ModalMensaje').modal('show');
}
