﻿//// Funciones Js, Jesus Jacome
/// Fecha de creacion 06/03/2019


function mostrarMenu() {

    var navMenu = document.getElementById("accordionSidebar");
    var divContenedor = document.getElementById("content-wrapper");

    if (navMenu.classList.contains('toggled-menu')) {
        navMenu.classList.remove('toggled-menu');

        divContenedor.classList.remove('div-desbloquear');
        divContenedor.classList.add('div-bloquear');
    }
    else {
        navMenu.classList.add('toggled-menu');
        divContenedor.classList.remove('div-bloquear');
        divContenedor.classList.add('div-desbloquear');
    }    
}

function ModalConfirmacionMostrar(modal) {
    $('#' + modal).modal('show');
}
function ModalConfirmacionOcultar() {
    var div = document.getElementsByClassName("modal-backdrop fade show");
    div[0].remove();    
}
function ModalConfirmacionOcultarOVC(div) {
    var div2 = document.getElementsByClassName("modal-backdrop fade show");
    div2[0].remove();    
    var div = document.getElementById(div);
    div.style.display = "none";
    
}
function goToDiv(idName) {    
    top.location.href = "#buscarProductos";    
}

function irArriba() {
    top.location.href = "#Inicio";  
}

function irBuscarProducto() {
    top.location.href = "#buscarProductos";      
}


function cambiaColorEmpresa(company) {

    if (company = "FBD001") {

        $('.bg-gradient-bb').removeClass("bg-gradient-bb").addClass("bg-naranja-fb");
        $('.card-pedidos').removeClass("card-pedidos").addClass("card-pedidosFB");
        $('.text-menu').removeClass("text-menu").addClass("text-menu-fb");
        $('.ir-arriba').removeClass("ir-arriba").addClass("ir-arriba-fb");
        $('.ir-productos').removeClass("ir-productos").addClass("ir-productos-fb");
        $('.text-bb').removeClass("text-bb").addClass("text-cat-fb");
    }
}

function ocultarNavFiltros(div) {

    var div = document.getElementById(div);
    div.style.display = "none";
}


function mostrarNavFiltros(div) {

    var div = document.getElementById(div);
    div.style.display = "block";
}