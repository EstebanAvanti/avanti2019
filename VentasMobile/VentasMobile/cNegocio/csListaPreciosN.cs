﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csListaPreciosN
    {
        csListaPreciosD listaD;
        public DataTable ObtenerlistaPrecios( string company, string CustNum, string Grup)
        {
            try
            {
                listaD = new csListaPreciosD();
                return listaD.ObtenerListaPrecios( company, CustNum, Grup);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //----ObtenerListaPreciosByClienet
        public DataTable ObtenerListaPreciosByClienet(string company, string CustID, string Grup)
        {
            try
            {
                listaD = new csListaPreciosD();
                return listaD.ObtenerListaPreciosByClienet(company, CustID, Grup);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        // ObtenerListaPreciosByDomicilio
        public DataTable ObtenerListaPreciosByDomicilio(string company, string CustID, string domi)
        {
            try
            {
                listaD = new csListaPreciosD();
                return listaD.ObtenerListaPreciosByDomicilio(company, CustID, domi);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}