﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;
namespace VentasMobile.cNegocio
{
    public class csPlantasN
    {
        csPlantasD plantasD;
        public DataTable ObtenerPlantByuserCompany(string cliente, string company)
        {
            try
            {
                plantasD = new csPlantasD();
                return plantasD.ObtenerPlantbyUserCompany(cliente, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerPlantasbyId(string id, string company)
        {
            try
            {
                plantasD = new csPlantasD();
                return plantasD.ObtenerPlantasById(id, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerDepositosByPlanta(string company, string plant)
        {
            try
            {
                plantasD = new csPlantasD();
                return plantasD.ObtenerDepositosByPlanta(company,plant);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}