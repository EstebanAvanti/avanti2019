﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography;
using System.Web.Configuration;
using VentasMobile.cNegocio;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Web.Services;

/// <summary>
/// Summary description for csGenerales
/// </summary>
public class csGenerales
{
    public csGenerales()
    { }

    #region "Funciones"
    /// <summary>
    /// Carga Los datos de una tabla a un DropDownList
    /// </summary>
    /// <param name="control">nombre del control asociado</param>
    /// <param name="DataTextField">nombre del campo que se desplegara</param>
    /// <param name="DataValueField">id o valor del campo para la propiedad de valor</param>
    /// <param name="dtSource">tabla de datos</param>
    public void CargarDropDownList(DropDownList control, String DataTextField, String DataValueField, DataTable dtSource)
    {
        control.DataTextField = DataTextField;
        control.DataValueField = DataValueField;
        control.DataSource = dtSource;
        control.DataBind();
    }
    /// <summary>
    /// Carga los datos de una tabla a un CheckBoxList
    /// </summary>
    /// <param name="control">nombre del control asociado</param>
    /// <param name="DataTextField">Nombre del campo que se desplegara</param>
    /// <param name="DataValueFiel">id o valor del campo para la propiedad valor</param>
    /// <param name="dtSource">Tabla de datos</param>
    public void CargarCheckBoxList(CheckBoxList control, String DataTextField, String DataValueFiel, DataTable dtSource)
    {
        control.DataTextField = DataTextField;
        control.DataValueField = DataValueFiel;
        control.DataSource = dtSource;
        control.DataBind();
    }
    public void CargarRadioButtonList(RadioButtonList control2, String DataTextField, String DataValueFiel, DataTable dtSource)
    {
        control2.DataTextField = DataTextField;
        control2.DataValueField = DataValueFiel;
        control2.DataSource = dtSource;
        control2.DataBind();
    }
    /// <summary>
    /// Limpia todos los campos de un formulario
    /// </summary>
    /// <param name="ctrl">Control del que se limpiarán los campos. Regularmente [this]</param>
    public void limpiaForm(Control ctrl)
    {
        foreach (Control control in ctrl.Controls)
        {
            if (control.GetType() == typeof(TextBox))
            {
                ((TextBox)control).Text = String.Empty;
            }
            if (control.GetType() == typeof(HiddenField))
            {
                ((HiddenField)control).Value = null;
            }
            if ((control.GetType() == typeof(DropDownList)))
            {
                ((DropDownList)control).SelectedValue = "0";
            }
            if (control.HasControls())
                limpiaForm(control);
        }
        Mensajes _msj = new Mensajes(ctrl, ctrl.GetType());
        _msj.limpiar();
    }
    #endregion

    public DateTime ObtenerHoraHusoHorarioZona(string husoHorario)
    {
        DateTime localDateTime = DateTime.Now;
        try
        {            
        localDateTime = localDateTime.ToLocalTime();

        DateTime utcDateTime = localDateTime.ToUniversalTime();
        string lblUtcDateTime = utcDateTime.ToString();

        TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(husoHorario);
        DateTime worldDateTime = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(lblUtcDateTime), timeZoneInfo);
        return worldDateTime;

        }
        catch (Exception)
        {
            return localDateTime;
        }
    }
    public void EnviarCorreoUsuario(string correo, string usuario, string contraseña)
    {
        try
        {
            //Configuración del Mensaje
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("mail.example.com");
            //Especificamos el correo desde el que se enviará el Email y el nombre de la persona que lo envía
            mail.From = new MailAddress("desarrollo@example.com", "Departamento TI", Encoding.UTF8);
            //Aquí ponemos el asunto del correo
            mail.Subject = "Alta Usuario en el sistema Proyecto_.";
            //Aquí ponemos el mensaje que incluirá el correo
            mail.IsBodyHtml = true;
            mail.Body = "Buen día."+
                "<br/>" +        
                "Se creo el usuario solicitado para el sistema de Proyecto_, favor de ingresar con: <br /> "+
                "<br/>"+
                "Usuario: <strong>" + usuario + "</strong> Contraseña: <strong> " + contraseña + " </strong>"+
                "<br/>"+
                "<br/>"+
                "Al momento de ingresar con esa contraseña, el sistema pedirá que cambie la contraseña por una personalizada."+
                "<br/>"+
                "<br/>"+
                "Saludos Cordiales.";
            //Especificamos a quien enviaremos el Email, no es necesario que sea Gmail, puede ser cualquier otro proveedor
            mail.To.Add(correo);
            //Si queremos enviar archivos adjuntos tenemos que especificar la ruta en donde se encuentran
            //mail.Attachments.Add(new Attachment(@"C:\Documentos\carta.docx"));

            //Configuracion del SMTP
            SmtpServer.Port = 25; //Puerto que utiliza Gmail para sus servicios
            //Especificamos las credenciales con las que enviaremos el mail
            SmtpServer.Credentials = new System.Net.NetworkCredential("desarrollo@example.com", "desarrollo.2016");
            SmtpServer.EnableSsl = false;
            SmtpServer.Send(mail);
        }
        catch (Exception)
        {
        }
    }
    public string GetSHA1(string str)
    {
        SHA1 sha1 = SHA1Managed.Create();
        ASCIIEncoding encoding = new ASCIIEncoding();
        byte[] stream = null;
        StringBuilder sb = new StringBuilder();
        stream = sha1.ComputeHash(encoding.GetBytes(str));
        for (int i = 0; i<stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
        return sb.ToString();
    }
    public string cargarCombosIdioma(string lenguaje,int tipo)
    {
        
            string seleccionar = "";

            if (lenguaje != null)
            {
                switch (lenguaje.ToString())
                {
                    case "es-mx":
                        switch (tipo)
                        {
                            case 1: seleccionar = "Seleccione registro";
                                break;
                            case 2: seleccionar = "Todos los registros";
                                break;
                        }                        
                        break;
                    case "en":
                        switch (tipo)
                        {
                            case 1: seleccionar = seleccionar = "Select record";
                                break;
                            case 2: seleccionar = "All record";
                                break;
                        }                             
                        break;
                    default:
                        seleccionar = "Select record";
                        break;

                }

            }
            return seleccionar;
        

    }
    public string EnviarCorreoOC(string folio, string rutaPDF, string enviarCorreos, string comentarioCorreo, string company, string cliente,
        string domicilio, string vendedor, string userVendedor)
    {
        string resultado = "";
        DataTable emailUser = new DataTable();
        csVendedorN cVendedor = new csVendedorN();
        int puerto = Convert.ToInt32(WebConfigurationManager.AppSettings["puerto"]);
        bool ssl = Convert.ToBoolean(WebConfigurationManager.AppSettings["ssl"]);
        string Path = @"" + rutaPDF;
        string LogoCompany = "", host = "", correoEmisor = "", correoEmisorContrasenia = "",correoVendedor="";

        if (company == "FBD001")
        {
            correoEmisor = WebConfigurationManager.AppSettings["correoEmisorFBD"];
            correoEmisorContrasenia = WebConfigurationManager.AppSettings["ContraseniaCorreoEmisorFBD"];
            LogoCompany = WebConfigurationManager.AppSettings["rutaReporteOC-FBD"];
            host = WebConfigurationManager.AppSettings["host"];

        }
        else if (company == "BBY002")
        {
            correoEmisor = WebConfigurationManager.AppSettings["correoEmisorBBY"];
            correoEmisorContrasenia = WebConfigurationManager.AppSettings["ContraseniaCorreoEmisorBBY"];
            LogoCompany = WebConfigurationManager.AppSettings["rutaReporteOC-BBY"];
            host = WebConfigurationManager.AppSettings["host"];

        }
        else if (company == "ADE001")
        {
            //correoEmisorContrasenia = WebConfigurationManager.AppSettings["ContraseniaCorreoEmisorFBD"];
            //LogoCompany = WebConfigurationManager.AppSettings["rutaReporteOC-ADE"];
            //host = WebConfigurationManager.AppSettings["hostADE001"];
        }

        //emailUser = cVendedor.ObtenerDatosCliente(company, userVendedor);//----consulta coreos del vendedor
        //if (emailUser.Rows.Count > 0)
        //    correoVendedor = ","+emailUser.Rows[0]["EMailAddressSR"].ToString();
        //else
        //    resultado = "No se encontro correo emisor.";
        //------>coreo nuevo----------
        //correoVendedor = "esteba.alcocer@grupoavanti.com";
        /*-------------------------MENSAJE DE CORREO----------------------*/
        System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();
        mmsg.From = new MailAddress(correoEmisor, "Ventas Mobile-Vendedor: "+vendedor, Encoding.UTF8);
        mmsg.Subject = "Número folio: " + folio;
        mmsg.SubjectEncoding = System.Text.Encoding.UTF8;  
        mmsg.To.Add(enviarCorreos+correoVendedor);
        //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario
        //Direccion de correo electronico que queremos que reciba una copia del mensaje
        //mmsg.Bcc.Add("ejemplo@grupoavanti.com"); //Opcional        
        //Cuerpo del Mensaje
        mmsg.IsBodyHtml = true;
        mmsg.Body = generarHtml(LogoCompany, folio, comentarioCorreo, cliente, domicilio, vendedor) ;
        mmsg.Attachments.Add(new Attachment(Path));

        System.Net.Mail.SmtpClient clienteM = new System.Net.Mail.SmtpClient();
        clienteM.Credentials = new System.Net.NetworkCredential(correoEmisor,correoEmisorContrasenia);
        clienteM.Port = puerto;
        clienteM.EnableSsl = ssl;
        clienteM.Host = host; 
        try
        {
            clienteM.Send(mmsg);
            resultado= "true";
            return resultado;
        }
        catch (System.Net.Mail.SmtpException ex)
        {
            //Aquí gestionamos los errores al intentar enviar el correo
            return "Error: " + ex.Message.ToString() ;
        }
    }
    public string generarHtml(string Logocompany,string folio,string comentario,string cliente,string domicilio,string vendedor)
    {
        string resultado = "<!doctype html><html><head><meta charset = 'UTF-8' http-equiv = 'X-UA-Compatible' content = 'IE=edge' />   "+
               " <meta name = 'viewport' content = 'width=device-width, initial-scale=1' />           " +
                  "<title></title >  " +
                  "<style type = 'text/css' > " +
                      "  p { margin: 10px 0; padding: 0;        } " +
                       "  table { border - collapse:collapse;        } " +
                        " h1 { display: block; margin: 0; padding: 0;        } " +
                        " img,a img { border: 0; height: auto; outline: none; text - decoration:none;        } " +
                        " body,#bodyTable,#bodyCell {height:100%;margin:0;padding:0;width:100%;} " +
                            " img { -ms - interpolation - mode:bicubic;        } " +
                        " table { mso - table - lspace:0pt; mso - table - rspace:0pt;        } " +
                        " p,a,li,td,blockquote { mso - line - height - rule:exactly;        } " +
                        " p,a,li,td,body,table,blockquote { -ms - text - size - adjust:100 %; -webkit - text - size - adjust:100 %;        } " +
                        " a[x - apple - data - detectors] { color: inherit !important; text - decoration:none !important; font - size:inherit !important; font - family:inherit !important; font - weight:inherit !important; line - height:inherit !important;        } " +
                            " # bodyCell {padding:10px;} " +
                            " .templateContainer { max - width:600px !important;        } " +
                            " .mcnImage { vertical - align:bottom;        } " +
                            " .mcnDividerBlock { table - layout:fixed !important;        } " +
                        " body,#bodyTable {background-color:#FAFAFA;} " +
                        " # bodyCell {border-top:0;} " +
                            " .templateContainer { border: 0;        } " +
                        " h1 { " +
                        " color:#202020;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:normal;text-align:left;} " +

                            " #templatePreheader {background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:2px none;padding-top:6px;padding-bottom:6px;} " +

                            " #templatePreheader .mcnTextContent,#templatePreheader p {color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:left;} " +

                            " #templatePreheader a,#templatePreheader p a {color:#656565;font-weight:normal;text-decoration:underline;} " +

                            " #templateHeader {background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0;} " +

                            " #templateHeader .mcnTextContent,#templateHeader p {color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left;} " +

                            " #templateHeader a,#templateHeader p a {color:#2BAADF;font-weight:normal;text-decoration:underline;} " +

                            " #templateBody {background-color:#FFFFFF;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;	border-top:0;border-bottom:2px solid #EAEAEA;padding-top:0;padding-bottom:9px;} " +

                            " #templateBody .mcnTextContent,#templateBody p {color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left;} " +

                            " #templateBody a,#templateBody p a {color:#2BAADF;font-weight:normal;text-decoration:underline;} " +

                            " #templateFooter {background-color:#FAFAFA;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px;} " +

                            " #templateFooter .mcnTextContent,#templateFooter p {color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center;} " +

                            " #templateFooter a,#templateFooter p a {color:#656565;font-weight:normal;text-decoration:underline;} " +

                            " @media only screen and(min-width:768px){ " +
                                " .templateContainer { width: 600px !important; } } " +
                            " @media only screen and(max-width: 480px){ " +
                              "   body,table,td,p,a,li,blockquote { -webkit - text - size - adjust:none !important;                } " +
                               "  body { width: 100 % !important; min - width:100 % !important;                } " +
                                " # bodyCell {padding-top:10px !important;} " +
                                " .mcnImage { width: 100 % !important;                } " +
                                " .mcnTextContentContainer { max - width:100 % !important; width: 100 % !important;                } " +
                                " .mcnTextContent { padding - right:18px !important; padding - left:18px !important;                } " +
                                " h1 { font - size:22px !important; line - height:125 % !important;                } " +
                                " .mcnTextContent, p { font - size:14px !important; line - height:150 % !important;                } " +
                                " # templatePreheader {display:block !important;} " +
                                " # templatePreheader .mcnTextContent,#templatePreheader p {font-size:14px !important;line-height:150% !important;} " +
                                " # templateHeader .mcnTextContent,#templateHeader p {font-size:16px !important;line-height:150% !important;} " +
                                " # templateBody .mcnTextContent,#templateBody p {font-size:16px !important;line-height:150% !important;} " +
                                " # templateFooter .mcnTextContent,#templateFooter p {font-size:14px !important;line-height:150% !important;} " +
                            " } " +
        " </style >  </head >  <body > " +
        "<table align = 'center' border = '0' cellpadding = '0' cellspacing = '0' height = '100%' width = '100%' id = 'bodyTable' >              " +
                         "<tr >              " +
                             "<td align = 'center' valign = 'top' id = 'bodyCell' >                   " +
                               "       <table border = '0' cellpadding = '0' cellspacing = '0' width = '100%' class='templateContainer'><tr> " +
                            "<td valign = 'top' id='templatePreheader'> " +
                              "  <table border = '0' cellpadding='0' cellspacing='0' width='100%' class='mcnImageBlock' style='min-width:100%;'> " +
                                   " <tbody class='mcnImageBlockOuter'><tr> " +
                                              "  <td valign = 'top' style='padding:0px' class='mcnImageBlockInner'> " +
                                                   " <table align = 'left' width='100%' border='0' cellpadding='0' cellspacing='0' class='mcnImageContentContainer' style='min-width:100%;'> " +
                                                       " <tbody>	<tr> " +
                                                              //"  <td class='mcnImageContent' valign='top' style='padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;'> " +
                                                              //"      <img align = 'center' alt='' src='"+Logocompany+"' width='189' style='max-width:189px; padding-bottom: 0; display: inline !important; vertical-align: bottom;' class='mcnImage'/> " +
                                                              //"   </td> " +
                                                           "  </tr><tr> " +
                                                               " <td valign = 'top' id='templateBody'></td> " +
                                                          "   </tr></tbody></table></td></tr></tbody></table></td> " +
                        " </tr> " +
                       " <tr> " +
                           " <td valign = 'top' id='templateHeader'> " +
                               " <table border = '0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width:100%;'> " +
                                    "<tbody class='mcnTextBlockOuter'> " +
                                      "  <tr> " +
                                           " <td valign = 'top' class='mcnTextBlockInner' style='padding-top:9px;'> " +
                                              "  <table align = 'left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%; min-width:100%;' width='100%' class='mcnTextContentContainer'> " +
                                                    "<tbody><tr> " +
                                                           " <td valign = 'top' class='mcnTextContent' style='padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;'> " +
                                                              "  <h1 style = 'text-align: center;' ></h1 > " +
                                                               " <p ><strong > Buen día.</strong>&nbsp;<br/>Seguimiento para solicitud " +
                                                                        " <br/><br/> "+
                                                                " 	<strong>Su numero de folio es:</strong>&nbsp;"+folio+"<br/> " +
                                                                 "   <strong>Cliente:</strong>&nbsp;" + cliente + "<br/> " +
                                                                 "   <strong>Domicilio:</strong>&nbsp;" +domicilio+"<br/> " +
                                                                  "  <strong>Vendedor:</strong>&nbsp;"+vendedor+"<br/> " +
                                                                  "  <br/> " +
                                                                  "  <strong>Comentario:</strong>&nbsp;"+comentario+"<br/> " +
                                                                  "  <br/> " +
                                                                  "  <strong>¡Saludos cordiales!</strong><br/><br/><br/> " +
                                                               "  </p></td></tr></tbody></table></td></tr></tbody></table></td> " +
                        " </tr><tr> " +
                          "  <td valign = 'top' id='templateBody'></td> " +
                       "  </tr><tr> " +
                           " <td valign = 'top' id='templateFooter'> " +
                               " <table border = '0' cellpadding='0' cellspacing='0' width='100%' class='mcnTextBlock' style='min-width:100%;'> " +
                                   " <tbody class='mcnTextBlockOuter'> " +
                                      "  <tr> " +
                                           " <td valign = 'top' class='mcnTextBlockInner' style='padding-top:9px;'> " +
                                                "<table align = 'left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%; min-width:100%;' width='100%' class='mcnTextContentContainer'> " +
                                                   " <tbody>	<tr> " +
                                                            "<td valign = 'top' class='mcnTextContent' style='padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;'> " +

                                                               "  Cualquier detalle que encuentren favor de contactar al departamento de Sistemas Grupo Avanti.<br/> " +
                                                               " <br/> " +
                        " </td></tr></tbody></table></td></tr></tbody></table></td> " +
                      "   </tr></table></td></tr></table></body></html>";

        return resultado;
    }


    //Generar PDF VENTA/COTIZACION
    public bool To_pdfSinEnvio(int numeroOrden, string company)
    {
        try
        {
            string cliente = "";
            string FechaOrden = "";
            string vendedor = "";
            string nombreCliente = "";
            string rfcCliente = "";
            string nombreEmpresa = "";
            string rfcEmpresa = "";
            string DirEmpresa = "";
            string CiudadEmpresa = "";
            string sucursal = "";
            string comentario = "";
            string Subtotal = "";
            string Impuestos = "";
            string Total = "";
            string Domicilio = "";
            string Moneda = "";
            iTextSharp.text.Font f = FontFactory.GetFont("Calibri", 8, iTextSharp.text.Font.BOLD);
            string rutaPDF = WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Orden de venta " + company + " " + numeroOrden + ".pdf";

            // se va a  optener el encabezado y detalle de la order
            DataTable dtOrd = new DataTable();
            csOrdenVentaN ord = new csOrdenVentaN();
            DataTable tabletemp = new DataTable();
            tabletemp.Clear();
            tabletemp.Columns.Add("Clave", typeof(string));
            tabletemp.Columns.Add("CodigoBarras", typeof(string));
            tabletemp.Columns.Add("Unidad", typeof(string));
            tabletemp.Columns.Add("Descripcion", typeof(string));
            tabletemp.Columns.Add("Cantidad", typeof(int));
            tabletemp.Columns.Add("Precio", typeof(double));
            tabletemp.Columns.Add("Total", typeof(double));

            dtOrd = ord.ObtenerOrdenVentaPDF(company, numeroOrden);
            if (dtOrd.Rows.Count > 0)
            {

                company = dtOrd.Rows[0]["Company"].ToString();
                cliente = dtOrd.Rows[0]["CustNum"].ToString();
                FechaOrden = dtOrd.Rows[0]["OrderDate"].ToString();
                vendedor = dtOrd.Rows[0]["vendedor"].ToString();
                sucursal = dtOrd.Rows[0]["Sucursal"].ToString();
                Domicilio = dtOrd.Rows[0]["Domicilio"].ToString();
                comentario = dtOrd.Rows[0]["OrderComment"].ToString();
                Subtotal = dtOrd.Rows[0]["Subtotal"].ToString();
                Impuestos = dtOrd.Rows[0]["Impuestos"].ToString();
                Total = dtOrd.Rows[0]["TotalFinal"].ToString();
                Moneda = dtOrd.Rows[0]["Moneda"].ToString();
                foreach (DataRow dr in dtOrd.Rows)
                {
                    DataRow resultRow = tabletemp.NewRow();
                    resultRow["Clave"] = dr["Clave"].ToString();
                    resultRow["CodigoBarras"] = dr["CodigoBarras"].ToString();
                    resultRow["Unidad"] = dr["Unidad"].ToString();
                    resultRow["Descripcion"] = dr["Descripcion"].ToString();
                    resultRow["Cantidad"] = Convert.ToInt32(dr["Cantidad"]);
                    resultRow["Precio"] = Convert.ToDouble(dr["Precio"]);
                    resultRow["Total"] = Convert.ToDouble(dr["Total"]);
                    tabletemp.Rows.Add(resultRow);
                }

            }

            if (File.Exists(rutaPDF) == false)
            {
                System.IO.FileStream fs = new FileStream(rutaPDF, FileMode.Create);
                Document doc = new Document(PageSize.LETTER, 15, 15, 15, 15);
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                doc.AddAuthor("TI");
                doc.AddCreator("Sample application using iTextSharp");
                doc.AddKeywords("PDF creacion");
                doc.AddSubject("Document subject - Describing the steps creating a PDF document");
                doc.AddTitle("The document title - PDF creation using iTextSharp");
                doc.Open();
                // consultar datos de la empresa y del clientes en base a la sesion cliente y sesion empresa
                csClientesN clie = new csClientesN();
                DataTable tbl = new DataTable();
                tbl = clie.ObtenerDatosClienteEmpresa(cliente, company);
                if (tbl.Rows.Count > 0)
                {
                    nombreCliente = tbl.Rows[0]["Name"].ToString();
                    rfcCliente = tbl.Rows[0]["ResaleID"].ToString();
                    nombreEmpresa = tbl.Rows[0]["Empresa"].ToString();
                    rfcEmpresa = tbl.Rows[0]["StateTaxID"].ToString();
                    DirEmpresa = tbl.Rows[0]["Address1"].ToString();
                    CiudadEmpresa = tbl.Rows[0]["City"].ToString();
                }

                // Creamos la imagen de la compañia y le ajustamos el tamaño
                if (company == "BBY002")
                {

                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-BBY"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 150 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 740);
                    doc.Add(imagen);
                }
                else if (company == "FBD001")
                {
                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-FBD"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 70 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 720);
                    doc.Add(imagen);
                }
                else if (company == "ADE001")
                {
                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-ADE"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 150 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 740);
                    doc.Add(imagen);
                }
                Paragraph parraf = new Paragraph("No Orden: " + numeroOrden + "  ", f);
                parraf.SpacingBefore = 0;
                parraf.SpacingAfter = 0;
                parraf.Alignment = 2; //0-Left, 1 middle,2 Right
                doc.Add(parraf);
                string remito = "Empresa: " + nombreEmpresa;
                string envio = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                Paragraph parrafo = new Paragraph("             Orden de Venta ", FontFactory.GetFont("CALIBRI", 15, iTextSharp.text.Font.BOLD));
                parrafo.SpacingBefore = 0;
                parrafo.SpacingAfter = 0;
                parrafo.Alignment = 1; //0-Left, 1 middle,2 Right
                doc.Add(parrafo);
                doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb = writer.DirectContent;
                cb.BeginText();
                BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.SetFontAndSize(f_cn, 11);
                cb.SetTextMatrix(20, 700); //(xPos, yPos) 
                cb.ShowText("Empresa: " + nombreEmpresa);
                cb.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cbc = writer.DirectContent;
                cbc.BeginText();
                BaseFont f_cnc = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc.SetFontAndSize(f_cnc, 11);
                cbc.SetTextMatrix(300, 700); //(xPos, yPos) 
                cbc.ShowText("Cliente: " + nombreCliente);
                cbc.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb1 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb1.SetFontAndSize(f_cn1, 11);
                cb1.SetTextMatrix(20, 680); //(xPos, yPos) 
                cb1.ShowText("RFC: " + rfcEmpresa);
                cb1.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cbc1 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc1.SetFontAndSize(f_cn, 11);
                cbc1.SetTextMatrix(300, 680); //(xPos, yPos) 
                cbc1.ShowText("RFC: " + rfcCliente);
                cbc1.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb2 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb2.SetFontAndSize(f_cn2, 11);
                cb2.SetTextMatrix(20, 660); //(xPos, yPos) 
                cb2.ShowText("Domicilio: " + DirEmpresa);
                cb2.EndText();
                PdfContentByte cbc2 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc2.SetFontAndSize(f_cnc2, 11);
                cbc2.SetTextMatrix(300, 660); //(xPos, yPos) 
                cbc2.ShowText("Domicilio: " + Domicilio);
                cbc2.EndText();
                PdfContentByte cb3 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb3.SetFontAndSize(f_cn3, 11);
                cb3.SetTextMatrix(20, 640); //(xPos, yPos) 
                cb3.ShowText("Moneda: " + Moneda);
                cb3.EndText();
                PdfContentByte cbc3 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc3.SetFontAndSize(f_cnc3, 11);
                cbc3.SetTextMatrix(150, 640); //(xPos, yPos) 
                cbc3.ShowText("Vendedor: " + vendedor);
                cbc3.EndText();
                PdfContentByte cbc4 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc4 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc4.SetFontAndSize(f_cnc4, 11);
                cbc4.SetTextMatrix(20, 620); //(xPos, yPos) 
                cbc4.ShowText("Sucursal: " + sucursal);
                cbc4.EndText();
                PdfContentByte cbc5 = writer.DirectContent;
                cbc1.BeginText();
                if (FechaOrden != "")
                {
                    BaseFont f_cnc5 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc5.SetFontAndSize(f_cn, 11);
                    cbc5.SetTextMatrix(150, 620); //(xPos, yPos) 
                    cbc5.ShowText("Fecha: " + Convert.ToDateTime(FechaOrden).ToString("dd/MM/yyyy"));
                    cbc5.EndText();
                }
                doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                doc.Add(new Paragraph("                     "));
                GeneraDocumento(doc, tabletemp, Subtotal, Impuestos, Total);
                doc.AddCreationDate();

                doc.Add(new Paragraph("                                          "));
                doc.Add(new Paragraph("                                          "));
                doc.Add(new Paragraph("Comentario:" + comentario));
                doc.Close();

                return true;
            }
            else
            {
                return true;
            }

        }
        catch (Exception ex)
        {
            return false;
        }
    }
    public bool To_pdf(int numeroOrden, string company)
    {
        try
        {
            string cliente = "";
            string FechaOrden = "";
            string vendedor = "";
            string nombreCliente = "";
            string rfcCliente = "";
            string nombreEmpresa = "";
            string rfcEmpresa = "";
            string DirEmpresa = "";
            string CiudadEmpresa = "";
            string sucursal = "";
            string comentario = "";
            string Subtotal = "";
            string Impuestos = "";
            string Total = "";
            string Domicilio = "";
            string Moneda = "";
            iTextSharp.text.Font f = FontFactory.GetFont("Calibri", 8, iTextSharp.text.Font.BOLD);
            string rutaPDF = WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Orden de venta " + company + " " + numeroOrden + ".pdf";

            // se va a  optener el encabezado y detalle de la order
            DataTable dtOrd = new DataTable();
            csOrdenVentaN ord = new csOrdenVentaN();
            DataTable tabletemp = new DataTable();
            tabletemp.Clear();
            tabletemp.Columns.Add("Clave", typeof(string));
            tabletemp.Columns.Add("CodigoBarras", typeof(string));
            tabletemp.Columns.Add("Unidad", typeof(string));
            tabletemp.Columns.Add("Descripcion", typeof(string));
            tabletemp.Columns.Add("Cantidad", typeof(int));
            tabletemp.Columns.Add("Precio", typeof(double));
            tabletemp.Columns.Add("Total", typeof(double));

            dtOrd = ord.ObtenerOrdenVentaPDF(company, numeroOrden);
            if (dtOrd.Rows.Count > 0)
            {

                company = dtOrd.Rows[0]["Company"].ToString();
                cliente = dtOrd.Rows[0]["CustNum"].ToString();
                FechaOrden = dtOrd.Rows[0]["OrderDate"].ToString();
                vendedor = dtOrd.Rows[0]["vendedor"].ToString();
                sucursal = dtOrd.Rows[0]["Sucursal"].ToString();
                Domicilio = dtOrd.Rows[0]["Domicilio"].ToString();
                comentario = dtOrd.Rows[0]["OrderComment"].ToString();
                Subtotal = dtOrd.Rows[0]["Subtotal"].ToString();
                Impuestos = dtOrd.Rows[0]["Impuestos"].ToString();
                Total = dtOrd.Rows[0]["TotalFinal"].ToString();
                Moneda = dtOrd.Rows[0]["Moneda"].ToString();
                foreach (DataRow dr in dtOrd.Rows)
                {
                    DataRow resultRow = tabletemp.NewRow();
                    resultRow["Clave"] = dr["Clave"].ToString();
                    resultRow["CodigoBarras"] = dr["CodigoBarras"].ToString();
                    resultRow["Unidad"] = dr["Unidad"].ToString();
                    resultRow["Descripcion"] = dr["Descripcion"].ToString();
                    resultRow["Cantidad"] = Convert.ToInt32(dr["Cantidad"]);
                    resultRow["Precio"] = Convert.ToDouble(dr["Precio"]);
                    resultRow["Total"] = Convert.ToDouble(dr["Total"]);
                    tabletemp.Rows.Add(resultRow);
                }

            }
            if (File.Exists(rutaPDF) == false)
            {
                System.IO.FileStream fs = new FileStream(rutaPDF, FileMode.Create);
                Document doc = new Document(PageSize.LETTER, 15, 15, 15, 15);
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                doc.AddAuthor("TI");
                doc.AddCreator("Sample application using iTextSharp");
                doc.AddKeywords("PDF creacion");
                doc.AddSubject("Document subject - Describing the steps creating a PDF document");
                doc.AddTitle("The document title - PDF creation using iTextSharp");
                doc.Open();
                // consultar datos de la empresa y del clientes en base a la sesion cliente y sesion empresa
                csClientesN clie = new csClientesN();
                DataTable tbl = new DataTable();
                tbl = clie.ObtenerDatosClienteEmpresa(cliente, company);
                if (tbl.Rows.Count > 0)
                {
                    nombreCliente = tbl.Rows[0]["Name"].ToString();
                    rfcCliente = tbl.Rows[0]["ResaleID"].ToString();
                    nombreEmpresa = tbl.Rows[0]["Empresa"].ToString();
                    rfcEmpresa = tbl.Rows[0]["StateTaxID"].ToString();
                    DirEmpresa = tbl.Rows[0]["Address1"].ToString();
                    CiudadEmpresa = tbl.Rows[0]["City"].ToString();
                }

                // Creamos la imagen de la compañia y le ajustamos el tamaño
                if (company == "BBY002")
                {

                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-BBY"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 150 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 740);
                    doc.Add(imagen);
                }
                else if (company == "FBD001")
                {
                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-FBD"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 70 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 720);
                    doc.Add(imagen);
                }
                else if (company == "ADE001")
                {
                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-ADE"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 150 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 740);
                    doc.Add(imagen);
                }
                Paragraph parraf = new Paragraph("No Orden: " + numeroOrden + "  ", f);
                parraf.SpacingBefore = 0;
                parraf.SpacingAfter = 0;
                parraf.Alignment = 2; //0-Left, 1 middle,2 Right
                doc.Add(parraf);
                string remito = "Empresa: " + nombreEmpresa;
                string envio = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                Paragraph parrafo = new Paragraph("             Orden de Venta ", FontFactory.GetFont("CALIBRI", 15, iTextSharp.text.Font.BOLD));
                parrafo.SpacingBefore = 0;
                parrafo.SpacingAfter = 0;
                parrafo.Alignment = 1; //0-Left, 1 middle,2 Right
                doc.Add(parrafo);
                doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb = writer.DirectContent;
                cb.BeginText();
                BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.SetFontAndSize(f_cn, 11);
                cb.SetTextMatrix(20, 700); //(xPos, yPos) 
                cb.ShowText("Empresa: " + nombreEmpresa);
                cb.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cbc = writer.DirectContent;
                cbc.BeginText();
                BaseFont f_cnc = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc.SetFontAndSize(f_cnc, 11);
                cbc.SetTextMatrix(300, 700); //(xPos, yPos) 
                cbc.ShowText("Cliente: " + nombreCliente);
                cbc.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb1 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb1.SetFontAndSize(f_cn1, 11);
                cb1.SetTextMatrix(20, 680); //(xPos, yPos) 
                cb1.ShowText("RFC: " + rfcEmpresa);
                cb1.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cbc1 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc1.SetFontAndSize(f_cn, 11);
                cbc1.SetTextMatrix(300, 680); //(xPos, yPos) 
                cbc1.ShowText("RFC: " + rfcCliente);
                cbc1.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb2 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb2.SetFontAndSize(f_cn2, 11);
                cb2.SetTextMatrix(20, 660); //(xPos, yPos) 
                cb2.ShowText("Domicilio: " + DirEmpresa);
                cb2.EndText();
                PdfContentByte cbc2 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc2.SetFontAndSize(f_cnc2, 11);
                cbc2.SetTextMatrix(300, 660); //(xPos, yPos) 
                cbc2.ShowText("Domicilio: " + Domicilio);
                cbc2.EndText();
                PdfContentByte cb3 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb3.SetFontAndSize(f_cn3, 11);
                cb3.SetTextMatrix(20, 640); //(xPos, yPos) 
                cb3.ShowText("Moneda: " + Moneda);
                cb3.EndText();
                PdfContentByte cbc3 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc3.SetFontAndSize(f_cnc3, 11);
                cbc3.SetTextMatrix(150, 640); //(xPos, yPos) 
                cbc3.ShowText("Vendedor: " + vendedor);
                cbc3.EndText();
                PdfContentByte cbc4 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc4 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc4.SetFontAndSize(f_cnc4, 11);
                cbc4.SetTextMatrix(20, 620); //(xPos, yPos) 
                cbc4.ShowText("Sucursal: " + sucursal);
                cbc4.EndText();
                PdfContentByte cbc5 = writer.DirectContent;
                cbc1.BeginText();
                if (FechaOrden != "")
                {
                    BaseFont f_cnc5 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc5.SetFontAndSize(f_cn, 11);
                    cbc5.SetTextMatrix(150, 620); //(xPos, yPos) 
                    cbc5.ShowText("Fecha: " + Convert.ToDateTime(FechaOrden).ToString("dd/MM/yyyy"));
                    cbc5.EndText();
                }
                doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                doc.Add(new Paragraph("                     "));
                GeneraDocumento(doc, tabletemp, Subtotal, Impuestos, Total);
                doc.AddCreationDate();

                doc.Add(new Paragraph("                                          "));
                doc.Add(new Paragraph("                                          "));
                doc.Add(new Paragraph("Comentario:" + comentario));
                doc.Close();
                return true;
            }
            else
            {
                return true;
            }

        }
        catch (Exception ex)
        {
            return false;
        }
    }
    public bool To_pdfCot(int numeroCotizacion, string company,string sucursal)
    {
        try
        {
            string cliente = "";
            string FechaOrden = "";
            string vendedor = "";
            string nombreCliente = "";
            string rfcCliente = "";
            string nombreEmpresa = "";
            string rfcEmpresa = "";
            string DirEmpresa = "";
            string CiudadEmpresa = "";
            if(sucursal=="MfgSys")
                sucursal = "CANCUN";
            string comentario = "";
            string Subtotal = "";
            string Impuestos = "";
            string Total = "";
            string Domicilio = "";
            string Moneda = "";
            iTextSharp.text.Font f = FontFactory.GetFont("CALIBRI", 8, iTextSharp.text.Font.BOLD);
            string rutaPDF = WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Cotización " + company + " " + numeroCotizacion + ".pdf";

            // se va a  optener el encabezado y detalle de la order
            csCotizacionN ord = new csCotizacionN();
            DataTable dtOrd = new DataTable();
            DataTable tabletemp = new DataTable();
            tabletemp.Clear();
            tabletemp.Columns.Add("Clave", typeof(string));
            tabletemp.Columns.Add("CodigoBarras", typeof(string));
            tabletemp.Columns.Add("Unidad", typeof(string));
            tabletemp.Columns.Add("Descripcion", typeof(string));
            tabletemp.Columns.Add("Cantidad", typeof(int));
            tabletemp.Columns.Add("Precio", typeof(double));
            tabletemp.Columns.Add("Total", typeof(double));

            dtOrd = ord.ObtenerCotizacion(company, numeroCotizacion);
            if (dtOrd.Rows.Count > 0)
            {

                company = dtOrd.Rows[0]["Company"].ToString();
                cliente = dtOrd.Rows[0]["CustNum"].ToString();
                FechaOrden = dtOrd.Rows[0]["DueDate"].ToString();
                vendedor = dtOrd.Rows[0]["vendedor"].ToString();
                Domicilio = dtOrd.Rows[0]["Domicilio"].ToString();
                comentario = dtOrd.Rows[0]["QuoteComment"].ToString();
                Subtotal = dtOrd.Rows[0]["Subtotal"].ToString();
                Impuestos = dtOrd.Rows[0]["Impuestos"].ToString();
                Moneda = dtOrd.Rows[0]["Moneda"].ToString();

                Total =(Convert.ToDouble(Subtotal)+Convert.ToDouble(Impuestos)).ToString();

                foreach (DataRow dr in dtOrd.Rows)
                {
                    DataRow resultRow = tabletemp.NewRow();
                    resultRow["Clave"] = dr["Clave"].ToString();
                    resultRow["CodigoBarras"] = dr["CodigoBarras"].ToString();
                    resultRow["Unidad"] = dr["Unidad"].ToString();
                    resultRow["Descripcion"] = dr["Descripcion"].ToString();
                    resultRow["Cantidad"] = Convert.ToInt32(dr["Cantidad"]);
                    resultRow["Precio"] = Convert.ToDouble(dr["Precio"]);
                    resultRow["Total"] = Convert.ToDouble(dr["Total"]);
                    tabletemp.Rows.Add(resultRow);
                }

            }
            if (File.Exists(rutaPDF) == false)
            {
                System.IO.FileStream fs = new FileStream(rutaPDF, FileMode.Create);
                Document doc = new Document(PageSize.LETTER, 15, 15, 15, 15);
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                doc.AddAuthor("TI");
                doc.AddCreator("Sample application using iTextSharp");
                doc.AddKeywords("PDF creacion");
                doc.AddSubject("Document subject - Describing the steps creating a PDF document");
                doc.AddTitle("The document title - PDF creation using iTextSharp");
                doc.Open();
                // consultar datos de la empresa y del clientes en base a la sesion cliente y sesion empresa
                csClientesN clie = new csClientesN();
                DataTable tbl = new DataTable();
                tbl = clie.ObtenerDatosClienteEmpresa(cliente, company);
                if (tbl.Rows.Count > 0)
                {
                    nombreCliente = tbl.Rows[0]["Name"].ToString();
                    rfcCliente = tbl.Rows[0]["ResaleID"].ToString();
                    nombreEmpresa = tbl.Rows[0]["Empresa"].ToString();
                    rfcEmpresa = tbl.Rows[0]["StateTaxID"].ToString();
                    DirEmpresa = tbl.Rows[0]["Address1"].ToString();
                    CiudadEmpresa = tbl.Rows[0]["City"].ToString();
                }

                // Creamos la imagen de la compañia y le ajustamos el tamaño
                if (company == "BBY002")
                {

                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-BBY"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 150 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 740);
                    doc.Add(imagen);
                }
                else if (company == "FBD001")
                {
                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-FBD"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 70 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 720);
                    doc.Add(imagen);
                }
                else if (company == "ADE001")
                {
                    string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-ADE"];
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_LEFT;
                    float percentage = 0.0f;
                    percentage = 150 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);
                    imagen.SetAbsolutePosition(20, 740);
                    doc.Add(imagen);
                }

                Paragraph parraf = new Paragraph("No Cotización: " + numeroCotizacion + "   ", f);
                parraf.SpacingBefore = 0;
                parraf.SpacingAfter = 0;
                parraf.Alignment = 2; //0-Left, 1 middle,2 Right
                doc.Add(parraf);
                // Insertamos la imagen en el documento            
                string remito = "Empresa: " + nombreEmpresa;
                string envio = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                Paragraph parrafo = new Paragraph("             Cotización ", FontFactory.GetFont("CALIBRI", 15, iTextSharp.text.Font.BOLD));
                parrafo.SpacingBefore = 0;
                parrafo.SpacingAfter = 0;
                parrafo.Alignment = 1; //0-Left, 1 middle,2 Right
                doc.Add(parrafo);
                doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb = writer.DirectContent;
                cb.BeginText();
                BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.SetFontAndSize(f_cn, 11);
                cb.SetTextMatrix(20, 700); //(xPos, yPos) 
                cb.ShowText("Empresa: " + nombreEmpresa);
                cb.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cbc = writer.DirectContent;
                cbc.BeginText();
                BaseFont f_cnc = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc.SetFontAndSize(f_cnc, 11);
                cbc.SetTextMatrix(300, 700); //(xPos, yPos) 
                cbc.ShowText("Cliente: " + nombreCliente);
                cbc.EndText();
                doc.Add(new Paragraph("                         "));
                PdfContentByte cb1 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb1.SetFontAndSize(f_cn1, 11);
                cb1.SetTextMatrix(20, 680); //(xPos, yPos) 
                cb1.ShowText("RFC: " + rfcEmpresa);
                cb1.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cbc1 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc1.SetFontAndSize(f_cnc1, 11);
                cbc1.SetTextMatrix(300, 680); //(xPos, yPos) 
                cbc1.ShowText("RFC: " + rfcCliente);
                cbc1.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cb2 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb2.SetFontAndSize(f_cn2, 11);
                cb2.SetTextMatrix(20, 660); //(xPos, yPos) 
                cb2.ShowText("Domicilio: " + DirEmpresa);
                cb2.EndText();
                doc.Add(new Paragraph("                     "));
                PdfContentByte cbc2 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc2.SetFontAndSize(f_cnc2, 11);
                cbc2.SetTextMatrix(300, 660); //(xPos, yPos) 
                cbc2.ShowText("Domicilio: " + Domicilio);
                cbc2.EndText();
                PdfContentByte cb3 = writer.DirectContent;
                cb1.BeginText();
                BaseFont f_cn3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb3.SetFontAndSize(f_cn3, 11);
                cb3.SetTextMatrix(20, 640); //(xPos, yPos) 
                cb3.ShowText("Moneda: " + Moneda);
                cb3.EndText();
                //doc.Add(new Paragraph("                     "));
                PdfContentByte cbc3 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc3.SetFontAndSize(f_cnc3, 11);
                cbc3.SetTextMatrix(150, 640); //(xPos, yPos) 
                cbc3.ShowText("Vendedor: " + vendedor);
                cbc3.EndText();
                PdfContentByte cbc4 = writer.DirectContent;
                cbc1.BeginText();
                BaseFont f_cnc4 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cbc4.SetFontAndSize(f_cnc4, 11);
                cbc4.SetTextMatrix(20, 620); //(xPos, yPos) 
                cbc4.ShowText("Sucursal: " + sucursal);
                cbc4.EndText();
                PdfContentByte cbc5 = writer.DirectContent;
                cbc1.BeginText();
                if (FechaOrden != "")
                {
                    BaseFont f_cnc5 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc5.SetFontAndSize(f_cnc5, 11);
                    cbc5.SetTextMatrix(150, 620); //(xPos, yPos)                 
                    cbc5.ShowText("Fecha: " + Convert.ToDateTime(FechaOrden).ToString("dd/MM/yyyy"));
                    cbc5.EndText();
                }

                doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                doc.Add(new Paragraph("                     "));
                GeneraDocumento(doc, tabletemp, Subtotal, Impuestos, Total);
                doc.AddCreationDate();

                doc.Add(new Paragraph("                                          "));
                doc.Add(new Paragraph("                                          "));
                doc.Add(new Paragraph("Comentario: " + comentario));
                doc.Close();
                return true;
            }
            else
            {
                return true;
            }

        }
        catch (Exception ex)
        {
            return false;
        }
    }
    public bool GeneraDocumento(Document document, DataTable dataTable, string Subtotal, string Impuestos, string Total)
    {
        try
        {

            iTextSharp.text.Font f = FontFactory.GetFont("CALIBRI", 8, iTextSharp.text.Font.NORMAL);
            decimal suma = 0;
            //Document document = new Document();
            PdfPTable table = new PdfPTable(dataTable.Columns.Count);
            table.WidthPercentage = 100;
            float[] headerwidths = { 10, 20, 10, 40, 10, 10, 15 };
            table.SetWidths(headerwidths);

            //Set columns names in the pdf file
            for (int k = 0; k < dataTable.Columns.Count; k++)
            {
                PdfPCell cell = new PdfPCell(new Phrase(dataTable.Columns[k].ColumnName, f));
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                cell.BackgroundColor = new iTextSharp.text.BaseColor(169, 169, 169);
                table.AddCell(cell);
            }

            //Add values of DataTable in pdf file
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    if (dataTable.Columns[j].ColumnName == "Precio" | dataTable.Columns[j].ColumnName == "Total")
                    {
                        PdfPCell cell = new PdfPCell(new Phrase("$ " + dataTable.Rows[i][j].ToString(), f));
                        //Align the cell in the center
                        cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell.VerticalAlignment = PdfPCell.ALIGN_RIGHT;

                        table.AddCell(cell);
                    }
                    else if (dataTable.Columns[j].ColumnName == "Descripcion")
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(dataTable.Rows[i][j].ToString(), f));
                        //Align the cell in the center
                        cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        cell.VerticalAlignment = PdfPCell.ALIGN_LEFT;
                        table.AddCell(cell);
                    }
                    else
                    {
                        PdfPCell cell = new PdfPCell(new Phrase(dataTable.Rows[i][j].ToString(), f));
                        //Align the cell in the center
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell);
                    }

                }
            }
            foreach (DataRow dr in dataTable.Rows)
            {
                suma = suma + Convert.ToDecimal(dr["Total"]);
            }
            // agrega subtotal
            for (int j = 0; j < dataTable.Columns.Count; j++)
            {
                if (dataTable.Columns[j].ColumnName == "Total")
                {
                    PdfPCell cell2 = new PdfPCell(new Phrase("$ " + Subtotal, f));
                    //Align the cell in the center
                    cell2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell2.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                    table.AddCell(cell2);
                }
                else if (dataTable.Columns[j].ColumnName == "Precio")
                {
                    PdfPCell cell4 = new PdfPCell(new Phrase("Subtotal: ", f));
                    //Align the cell in the center
                    cell4.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cell4.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell4);
                }
                else
                {
                    PdfPCell cell3 = new PdfPCell(new Phrase(" "));
                    //Align the cell in the center
                    cell3.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell3.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell3.Border = 0;
                    table.AddCell(cell3);
                }
            }
            // agrega impuestos
            for (int j = 0; j < dataTable.Columns.Count; j++)
            {
                if (dataTable.Columns[j].ColumnName == "Total")
                {
                    PdfPCell cell2 = new PdfPCell(new Phrase("$ " + Impuestos, f));
                    //Align the cell in the center
                    cell2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell2.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                    table.AddCell(cell2);
                }
                else if (dataTable.Columns[j].ColumnName == "Precio")
                {
                    PdfPCell cell4 = new PdfPCell(new Phrase("Impuestos: ", f));
                    //Align the cell in the center
                    cell4.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cell4.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell4);
                }
                else
                {
                    PdfPCell cell3 = new PdfPCell(new Phrase(" "));
                    //Align the cell in the center
                    cell3.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cell3.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    cell3.Border = 0;
                    table.AddCell(cell3);
                }
            }
            // agrega total
            for (int j = 0; j < dataTable.Columns.Count; j++)
            {
                if (dataTable.Columns[j].ColumnName == "Total")
                {
                    PdfPCell cell2 = new PdfPCell(new Phrase("$ " + Total, f));
                    //Align the cell in the center
                    cell2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    cell2.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                    table.AddCell(cell2);
                }
                else if (dataTable.Columns[j].ColumnName == "Precio")
                {
                    PdfPCell cell4 = new PdfPCell(new Phrase("Total: ", f));
                    //Align the cell in the center
                    cell4.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cell4.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell4);
                }
                else
                {
                    PdfPCell cell3 = new PdfPCell(new Phrase(" "));
                    //Align the cell in the center
                    cell3.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cell3.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    cell3.Border = 0;
                    table.AddCell(cell3);
                }

            }
            document.Add(table);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

}