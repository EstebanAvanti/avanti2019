﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;


namespace VentasMobile.cNegocio
{
    public class csGrupoClientesN
    {
        csGrupoClientesD grupoD;
        public DataTable ObtenerGrupoClientesXUser( string user, string company)
        {
            try
            {
                grupoD = new csGrupoClientesD();
                return grupoD.ObtenerGrupClienteXuser( user, company );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerClientesxGrupoXUser(string userId, string grupoId, string company )
        {
            try
            {
                grupoD = new csGrupoClientesD();
                return grupoD.ObtenerClienteXgrupXuser(userId, grupoId, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerDomicilioCliente(string userId, string custId, string company)
        {
            try
            {
                grupoD = new csGrupoClientesD();
                return grupoD.ObtenerDomicilioCliente(userId, custId, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        // ObtenerClienteXgrupXCust
        public DataTable ObtenerClienteXgrupXCust(string userId, string CustID, string company)
        {
            try
            {
                grupoD = new csGrupoClientesD();
                return grupoD.ObtenerClienteXgrupXCust(userId, CustID, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}