﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csClientesN
    {
        csClientesD cClientes;

        public DataTable ObtenerDatosCliente(string company,string grup, string custNum,string user,string shipToNum)
        {
            try
            {
                cClientes = new csClientesD();
                return cClientes.ObtenerDatosCliente(company,grup, custNum,user, shipToNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerHistorialCompraPorClienteDomicilio(string custNum, string shipToNum, string company)
        {
            try
            {
                cClientes = new csClientesD();
                return cClientes.ObtenerHistorialCompraPorClienteDomicilio(custNum, shipToNum, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerCorreosClienteDocimicilio(string custNum, string shipToNum, string company)
        {
            try
            {
                cClientes = new csClientesD();
                return cClientes.ObtenerCorreosClienteDocimicilio(custNum, shipToNum, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerHistorialClientePorProducto(string custNum, string parNum,string codigoBarras,string shiptoNum, string company)
        // 02082019 agregar parametro de sesion["PlantID"]
        {
            try
            {
                cClientes = new csClientesD();
                return cClientes.ObtenerHistorialClientePorProducto(custNum, parNum,codigoBarras,shiptoNum, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable ObtenerDatosClienteEmpresa(string custNum, string company)
        {
            try
            {
                cClientes = new csClientesD();
                return cClientes.ObtenerDatosClienteEmpresa(custNum, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerDatosClienteAuto(string company, string user,string CustNum)
        {
            try
            {
                cClientes = new csClientesD();
                return cClientes.ObtenerDatosClienteAuto(company, user,  CustNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}