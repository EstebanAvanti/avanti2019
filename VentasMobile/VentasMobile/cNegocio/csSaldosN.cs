﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csSaldosN
    {
        csSaldosD saldoD;
        public DataTable ObtenerSaldosXfactura(string cliente,string shipToNum, string company)
        {
            try
            {
                saldoD  = new csSaldosD();
                return saldoD .ObtenerSaldosXfactura(cliente, shipToNum, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerSaldosDisponible(string cliente, string company)
        {
            try
            {
                saldoD = new csSaldosD();
                return saldoD.ObtenerSaldosDisponible(cliente, company);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}