﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VentasMobile.cDatos;
using System.Data;

namespace VentasMobile.cNegocio
{
    public class csCotizacionN
    {
        csCotizacionD OD;

        public DataTable ObtenerCotizacion(string company, int quoteNum)
        {
            try
            {
                OD = new csCotizacionD();
                return OD.ObtenerCotizacion(company, quoteNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaVentasMobile(string company, int QuoteNum)
        {
            try
            {
                OD = new csCotizacionD();
                return OD.ActualizaVentasMobile(company, QuoteNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        // ObtenerCotizacionXcliente
        public DataTable ObtenerCotizacionXcliente(string company, int cliente)
        {
            try
            {
                OD = new csCotizacionD();
                return OD.ObtenerCotizacionXcliente(company, cliente);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}