﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VentasMobile.cEntidad
{
    public class csClientesE
    {
        private string _custID;       
        private string _custNum;
        private string name;
        public string CustID { get => _custID; set => _custID = value; }
        public string CustNum { get => _custNum; set => _custNum = value; }
        public string Name { get => name; set => name = value; }
    }
}