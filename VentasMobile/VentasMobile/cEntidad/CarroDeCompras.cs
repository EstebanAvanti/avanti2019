﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VentasMobile.cEntidad
{
    public class CarroDeCompras
    {
        public List<ProductosAlcarro> ListaProductos { get; private set; }
        public static CarroDeCompras CapturarProducto()
        {
            CarroDeCompras _carrito = (CarroDeCompras)HttpContext.Current.Session["ASPCarroDeCompras"];
            if (_carrito == null)
            {
                HttpContext.Current.Session["ASPCarroDeCompras"] = _carrito = new CarroDeCompras();
            }
            return _carrito;
        }
        protected  CarroDeCompras()
        {
            ListaProductos = new List<cEntidad.ProductosAlcarro>();
        }
        public void Agregar(string pId, double precio, string descripcion, int Cant)
        {
            cEntidad.ProductosAlcarro NuevoProducto = new cEntidad.ProductosAlcarro(pId, precio, descripcion);
            if (ListaProductos.Contains(NuevoProducto))
            {
                foreach (cEntidad.ProductosAlcarro item in ListaProductos)
                {
                    if (item.Equals(NuevoProducto))
                    {
                        item.Cantidad = item.Cantidad + Cant;
                        return;
                    }
                }
            }
            else
            {
                NuevoProducto.Cantidad = Cant;
                ListaProductos.Add(NuevoProducto);
            }
        }
        public void EliminarProductos(string pIdProducto, double precio, string descripcion)
        {
            cEntidad.ProductosAlcarro eliminaritems = new cEntidad.ProductosAlcarro(pIdProducto, precio, descripcion);
            ListaProductos.Remove(eliminaritems);
        }
        public void CantidadDeProductos(string pIdProducto, int pCantidad, double precio, string descripcion)
        {
            if (pCantidad == 0)
            {
                EliminarProductos(pIdProducto, precio, descripcion);
                return;
            }
            cEntidad.ProductosAlcarro updateProductos = new cEntidad.ProductosAlcarro(pIdProducto, precio, descripcion);
            foreach (cEntidad.ProductosAlcarro item in ListaProductos)
            {
                if (item.Equals(updateProductos))
                {
                    item.Cantidad = pCantidad;
                    return;
                }
            }
        }

        public double SubTotal()
        {
            double subtotal = 0;
            foreach (cEntidad.ProductosAlcarro  item in ListaProductos)
            {
                subtotal += item.Total;
            }
            return subtotal;
        }
    }  
}