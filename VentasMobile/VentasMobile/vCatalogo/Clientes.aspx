﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="VentasMobile.vCatalogo.Clientes" %>

<asp:Content ID="Clientes" ContentPlaceHolderID="MainContent" runat="server">    
    <div  id="ModalMensaje" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
        <div id="DivMensaje" class="modal-content">      
        </div>
        </div>
    </div>
<asp:UpdatePanel ID="uxUpdatePanelFormulario" runat="server">
    <ContentTemplate>
        <div id="Inicio" class="container mb-5">
            <div id="BuscadorCliente" class="row">
                <h4 class="mb-5 col-12"><i class="fas fa-address-card fa-lg mr-2"></i>Información de Clientes</h4>
                <div class="col-dropbox">
                    <asp:Label ID="uxLabelGrupos" runat="server" Text="Grupo de Clientes: " Font-Bold="True"></asp:Label>
                    <asp:DropDownList ID="uxDropDownListGrupo" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListGrupo_SelectedIndexChanged" ></asp:DropDownList> 
                </div>
                <div class="col-dropbox">
                    <asp:Label ID="UxLabelClientes" runat="server" Text="Clientes: " Font-Bold="True"></asp:Label>
                    <asp:DropDownList ID="uxDropDownListClientes" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListClientes_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-dropbox">
                    <asp:Label ID="uxLabelDomicilios" runat="server" Text="Domicilios: " Font-Bold="True"></asp:Label>  
                    <asp:DropDownList ID="uxDropDownListDomicilio" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListDomicilio_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-buscarCliente">
                <asp:Label ID="uxLabelCliente" runat="server" Text="No.Cliente: " Font-Bold="True"></asp:Label>  
                <asp:TextBox ID="txtSearch" runat="server"  CssClass="form-control" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>
                </div>
                <div class="col-buscarClienteBoton">                     
                <asp:Button ID="BtnSearch" runat="server" CausesValidation="false" Text="Buscar" class="btn btn-secondary btn-lg mt-3" OnClick="BtnSearch_Click" />
                <asp:Button ID="uxButtonCancelar" runat="server" CausesValidation="false" Text="Limpiar" class="btn btn-secondary btn-lg mt-3" OnClick="uxButtonCancelar_Click" />
                </div>
                <div class="col-12 text-center mb-4">
                <asp:HiddenField ID="uxHiddenFieldInventario" runat="server" />
                <asp:Button ID="btnRealizarVenta" runat="server"  CausesValidation="false" Text="Realizar pedido" class="btn btn-info bg-gradient-bb btn-lg  mt-4" OnClick="btnRealizarVenta_Click" />
            </div>
            </div>
            <div id="InfoCliente" class="row mt-4">
                <div class="col-sm-12 col-md-6 col-lg-6 border-top infoCliente">
                    <asp:Label ID="uxLabelNombre" runat="server" Text="Nombre:" Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="uxTextBoxNombre" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 border-top-infoCliente">
                    <asp:Label ID="uxLabelRfc" runat="server" Text="RFC: " Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="uxTextBoxRcf" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <asp:Label ID="uxLabelDireccion" runat="server" Text="Dirección:" Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="uxTextboxDirecion" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <asp:Label ID="uxLabelLimiteCred" runat="server" Font-Bold="True" Text="Limite de Credito: "></asp:Label>
                    <asp:TextBox ID="uxTextBoxLimiteCred" runat="server" CssClass="form-control"  Enabled="False"></asp:TextBox>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Saldo: "></asp:Label>
                    <asp:TextBox ID="uxTextBoxSaldo" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <asp:Label ID="lblCreditoDisponible" runat="server" Font-Bold="True" Text="Credito disponible: "></asp:Label>
                    <asp:TextBox ID="uxTexBoxCreditoDisponible" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                </div>
            </div>            
        </div>
        <div class="card">
            <ul class="nav nav-tabs md-tabs bg-gradient-bb" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link  active" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Ventas por producto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Historial facturas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Facturas con saldo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="order-tab" data-toggle="tab" href="#order" role="tab" aria-controls="order" aria-selected="false">Ordenes creadas VM</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link" id="quote-tab" data-toggle="tab" href="#quote" role="tab" aria-controls="quote" aria-selected="false">Cotizaciones creadas VM</a>
                </li>
            </ul>
            <div class="card-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <h4 class="mb-3">Historial de facturas:</h4>
                        <div class="table table-responsive d-flex flex-wrap text-wrap">
                            <asp:GridView ID="uxGridViewHistorial"  runat="server" AutoGenerateColumns="False" 
                            DataKeyNames="NumeroFactura" class="table table-responsive thead-dark f-12 text-wrap"  OnPageIndexChanging="uxGridViewHistorial_PageIndexChanging"   PageSize="20"   AllowPaging="True">
                                    <Columns>
                                        <asp:BoundField DataField="NumeroFactura" HeaderText="Número factura">
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="secuencia" HeaderText="No" />
                                        <asp:BoundField DataField="ClienteID" HeaderText="Número cliente">
                                        </asp:BoundField>--%>
                                        <asp:BoundField DataField="Cliente" HeaderText="Cliente">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DomicilioEmbarque" HeaderText="Domicilio embarque">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="InvoiceDate" HeaderText="Fecha factura">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DueDate" HeaderText="Fecha vencimiento">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ImporteFactura" HeaderText="Importe factura">
                                        </asp:BoundField>
                                    </Columns>
                            </asp:GridView>
                        </div>
                        
                    </div>
                    <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h4 class="mb-3">Facturas del cliente(Saldo pediente):</h4>
                        <div class="table table-responsive d-flex flex-wrap text-wrap">
                            <asp:GridView ID="uxGridViewSaldos" runat="server"  class="table table-responsive thead-dark f-12 text-wrap"></asp:GridView>
                        </div>                        
                    </div>
                    <div class="tab-pane fade  show active" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <h4 class="mb-3">Historial de ventas por producto:</h4>   
                        <div class="row">
                            <div class="col-6">
                                Codigo Producto: <asp:TextBox ID="uxTextBoxCodigoProducto" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-6">
                                Codigo Barras: <asp:TextBox ID="uxTextBoxCodigoBarras" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-12 m-auto text-center">
                                <asp:Button ID="uxButtonBuscarHistorialProducto" runat="server" Text="Buscar" OnClick="uxButtonBuscarHistorialProducto_Click" class="btn btn-primary btn-lg mb-3 bg-gradient-bb mt-3"/>
                            </div>
                        </div>   
                        <div class="table table-responsive d-flex flex-wrap text-wrap">                            
                            <asp:GridView ID="uxGridViewHistorialProductos"  runat="server" AutoGenerateColumns="False" 
                            DataKeyNames="PartNum" class="table table-responsive thead-dark f-12 text-wrap"  OnPageIndexChanging="uxGridViewHistorial_PageIndexChanging"   PageSize="20"   AllowPaging="True">
                                <Columns>
                                        <asp:BoundField DataField="Plant" HeaderText="Planta">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartNum" HeaderText="Producto">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartDescription" HeaderText="Descripcion">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Barras" HeaderText="CB">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SellingShipQty" HeaderText="Cantidad">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="InvoiceNum" HeaderText="Numero Factura">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LegalNumber" HeaderText="Factura Legal">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="InvoiceDate" HeaderText="Fecha Factura">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Domicilio" HeaderText="Domicilio Embarque">
                                        </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="order" role="tabpanel" aria-labelledby="order-tab">                       
                        <br />
                        Historial de Ordenes de venta por Cliente:<br />
                        <br />          
                        <br />
                        <div class="table table-responsive d-flex flex-wrap text-wrap"> 
                            <asp:GridView ID="uxGridViewOrdenes"  runat="server" AutoGenerateColumns="False" 
                            DataKeyNames="OrderNum" Width="100%" OnRowCommand="uxGridViewOrdenes_RowCommand"    >
                                  <Columns>
                                        <asp:BoundField DataField="OrderDate" HeaderText="Fecha">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="OrderNum" HeaderText="NumeroOrden">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Name" HeaderText="Cliente">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Domicilio" HeaderText="Domicilio">
                                        </asp:BoundField>
                                        <asp:ButtonField HeaderText="PDF" ImageUrl="~/images/icons/pdf.png" CommandName="Seleccionar" ButtonType="Image"/>
                                        <%--<asp:ButtonField HeaderText="Reenviar"  CommandName="Reenviar" ButtonType="Button"/>--%>
                                  </Columns>
                            </asp:GridView>
                        </div>
                    </div>                    
                     <div class="tab-pane fade" id="quote" role="tabpanel" aria-labelledby="Quote-tab">                       
                        <br />
                        Historial de Cotizaciones por Cliente:<br />
                        <br />          
                        <br />
                        <div class="table table-responsive d-flex flex-wrap text-wrap"> 
                            <asp:GridView ID="GridViewCotizaciones" runat="server" AutoGenerateColumns="False" 
                                DataKeyNames="QuoteNum" OnRowCommand="GridViewCotizaciones_RowCommand">                         
                                  <Columns>
                                        <asp:BoundField DataField="DueDate" HeaderText="Fecha">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="QuoteNum" HeaderText="NumeroCotizacion">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CustNum" HeaderText="Cliente">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Domicilio" HeaderText="Domicilio">
                                        </asp:BoundField>
                                        <asp:ButtonField HeaderText="PDF" ImageUrl="~/images/icons/pdf.png" CommandName="Seleccionar" ButtonType="Image"/>
                                     <asp:ButtonField HeaderText="Reenviar"  CommandName="Reenviar" ButtonType="Button"/>
                                  </Columns>
                            </asp:GridView>
                        </div>
                    </div> 
                </div>
            </div>
        </div>        
         </ContentTemplate>
     <Triggers>
        <asp:PostBackTrigger ControlID="uxGridViewOrdenes" />
        <asp:PostBackTrigger ControlID="GridViewCotizaciones" />                    
    </Triggers>
</asp:UpdatePanel>
 <br />        
<asp:UpdatePanel ID="uxUpdatePanelTabla" runat="server">
<ContentTemplate>        
            <div id="ModalConfirmacion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="exampleModalCenterTitle">Mensaje</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="Label2" 
                        CssClass="jqtransform sin_formato size_table" Text="El carrito de compras se eliminará ya que está a punto de cambiar la información del cliente." Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                       <asp:Button ID="uxButtonConfirmacionCambioClienteCancelar" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white" runat="server" Text="Cancelar" 
                            OnClick="uxButtonConfirmacionCambioClienteCancelar_Click" />
                        <asp:Button ID="uxButtonConfirmacionCambioCliente" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" 
                            OnClick="uxButtonConfirmacionCambioCliente_Click" />  
                  </div>
                </div>
              </div>
            </div>    
            <asp:LinkButton ID="uxLinkButtonBotonArriba" runat="server" OnClick="uxLinkButtonBotonArriba_Click" OnClientClick="irArriba()" class="ir-arriba fa-stack">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
            </asp:LinkButton>
</ContentTemplate>

</asp:UpdatePanel>
</asp:Content>