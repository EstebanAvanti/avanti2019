﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reportes.aspx.cs" Inherits="VentasMobile.vCatalogo.Reportes" %>


<asp:Content ID="Inventario" ContentPlaceHolderID="MainContent" runat="server">   
    <div class="jumbotron" style="background:#F8F7F2 !important;"> 
        <div  id="ModalMensaje" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered " role="document">
            <div id="DivMensaje" class="modal-content">      
            </div>
          </div>
    </div>
        <asp:UpdatePanel ID="uxUpdatePanelFormulario" runat="server">
            <ContentTemplate>
                <h4 class="mb-4 col-12"><i class="fa fa-fw fa-chart-line"></i> Reportes</h4>
                <br/>                  
                <iframe id="iframeReportes" name="iframeReportes" width="100%" height="1200" runat="server" allowfullscreen></iframe>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />     
    </div>
</asp:Content>

