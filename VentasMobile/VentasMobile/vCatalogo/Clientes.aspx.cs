﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cNegocio;
using System.Data;
using VentasMobile.cEntidad;
using System.IO;
using System.Web.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Web.Services;



namespace VentasMobile.vCatalogo
{
    public partial class Clientes : System.Web.UI.Page
    {
        private enumAccion _accion = enumAccion.Ninguno;
        private enum enumAccion { Ninguno = 0, CambioGrupo = 1, CambioCliente = 2, CambioDomicilio = 3, LimpiarFormulario = 4 };
        private DataTable uxTabla = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                string company = "";
                if (Session["CurComp"] != null)
                    company = this.Session["CurComp"].ToString();
                if (String.IsNullOrEmpty((String)Session["SessionID"]))
                {
                    Session.RemoveAll();
                    Session.Abandon();
                    Response.Redirect("~/Acceso.aspx");
                }
                else
                {
                    company = this.Session["CurComp"].ToString();
                    colorEmpresa(company);
                }

                string PlantID = this.Session["PlantID"].ToString();
                string categoria = "";
                if (Session["Categorias"] != null)
                {
                    categoria = Convert.ToString(Session["Categorias"]);

                    csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                    DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                    if (catego.Rows.Count > 0)
                    {
                        System.Web.UI.WebControls.Label uxLabelCategoriaVM = (System.Web.UI.WebControls.Label)this.Master.FindControl("uxLabelCategoriaV");
                        uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                        System.Web.UI.WebControls.Label uxLabelCategoriaL = (System.Web.UI.WebControls.Label)this.Master.FindControl("uxLabelCategoriaL");
                        uxLabelCategoriaL.Visible = true;
                    }
                    else
                    {
                        System.Web.UI.WebControls.Label uxLabelCategoriaVM = (System.Web.UI.WebControls.Label)this.Master.FindControl("uxLabelCategoriaV");
                        uxLabelCategoriaVM.Text = "No se encontro categoría.";
                        System.Web.UI.WebControls.Label uxLabelCategoriaL = (System.Web.UI.WebControls.Label)this.Master.FindControl("uxLabelCategoriaL");
                        uxLabelCategoriaL.Visible = true;
                    }
                }

                CargarComboGrupos();
                if (Session["Grupo"] != null)
                {
                    uxDropDownListGrupo.SelectedValue = Convert.ToString(Session["Grupo"]);
                    CargarComboClientes();
                }
                if (Session["Cliente"] != null)
                {
                    uxDropDownListClientes.SelectedValue = Convert.ToString(Session["Cliente"]);

                    CargarDatosCliente();
                    CargarSaldosDisponible();
                    CargarHistorialCliente();
                    CargarComboDomicilios();
                    CargarOrdenes();
                    CargarCotizaciones();
                }
                if (Session["Domicilio"] != null)
                {
                    uxDropDownListDomicilio.SelectedValue = Convert.ToString(Session["Domicilio"]);

                    CargarDatosCliente();
                    CargarSaldosCliente();
                    CargarSaldosDisponible();
                    CargarHistorialCliente();
                    CargarOrdenes();
                    CargarCotizaciones();
                }

            }


        }
        private void CargarComboGrupos()
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                string user = this.Session["DcdUserID"].ToString();
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerGrupoClientesXUser(user, company);
                DataRow row = uxTabla.NewRow();
                row["GroupDesc"] = "Seleccione Grupo";
                row["GroupCode"] = 0;
                uxTabla.Rows.InsertAt(row, 0);
                uxDropDownListGrupo.DataSource = uxTabla;
                uxDropDownListGrupo.DataTextField = "GroupDesc";
                uxDropDownListGrupo.DataValueField = "GroupCode";
                uxDropDownListGrupo.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarComboClientes()
        {
            try
            {

                // aqui se cambia CustumerID por CustNum
                string company = this.Session["CurComp"].ToString();
                string grupo = uxDropDownListGrupo.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerClientesxGrupoXUser(user, grupo, company);
                DataRow row = uxTabla.NewRow();
                row["Name"] = "Seleccione cliente";
                row["CustNum"] = 0;
                uxTabla.Rows.InsertAt(row, 0);
                uxDropDownListClientes.DataSource = uxTabla;
                uxDropDownListClientes.DataTextField = "Name";
                uxDropDownListClientes.DataValueField = "CustNum";
                uxDropDownListClientes.DataBind();

                uxDropDownListDomicilio.Items.Clear();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarComboDomicilios()
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                string cliente = uxDropDownListClientes.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerDomicilioCliente(user, cliente, company);
                DataRow row = uxTabla.NewRow();
                row["Name"] = "Seleccione domicilio";
                row["ShipToNum"] = "NA";
                uxTabla.Rows.InsertAt(row, 0);
                uxDropDownListDomicilio.DataSource = uxTabla;
                uxDropDownListDomicilio.DataTextField = "Name";
                uxDropDownListDomicilio.DataValueField = "ShipToNum";
                uxDropDownListDomicilio.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarDatosCliente()
        {
            try
            {

                string grup = uxDropDownListGrupo.SelectedValue;
                string cliente = uxDropDownListClientes.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                string shipToNum = "";
                if (Session["Domicilio"] != null)
                {
                    shipToNum = Session["Domicilio"].ToString();
                }
                csClientesN cClientes = new csClientesN();
                uxTabla = cClientes.ObtenerDatosCliente(company, grup, cliente, user, shipToNum);
                if (uxTabla.Rows.Count != 0)
                {
                    uxTextBoxNombre.Text = uxTabla.Rows[0]["Name"].ToString();
                    uxTextBoxRcf.Text = uxTabla.Rows[0]["ResaleID"].ToString();
                    uxTextboxDirecion.Text = uxTabla.Rows[0]["Address1"].ToString();
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarSaldosCliente()
        {
            try
            {
                string cliente = uxDropDownListClientes.SelectedValue;
                string shipTpNum = uxDropDownListDomicilio.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csSaldosN sSaldo = new csSaldosN();
                uxTabla = sSaldo.ObtenerSaldosXfactura(cliente, shipTpNum, company);

                if (uxTabla.Rows.Count > 0)
                {
                    uxGridViewSaldos.AutoGenerateColumns = true;
                    uxGridViewSaldos.DataSource = uxTabla;
                    uxGridViewSaldos.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('', 'error', true,false,'es-mx');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarSaldosDisponible()
        {
            try
            {
                string cliente = uxDropDownListClientes.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csSaldosN sSaldo = new csSaldosN();
                uxTabla = sSaldo.ObtenerSaldosDisponible(cliente, company);
                if (uxTabla.Rows.Count != 0)
                {
                    double saldo = Convert.ToDouble(uxTabla.Rows[0]["SaldoDisponible"].ToString());
                    double limite = Convert.ToDouble(uxTabla.Rows[0]["limite"].ToString());
                    double creditoDisponible = Convert.ToDouble(uxTabla.Rows[0]["creditoDisponible"].ToString());


                    string CreditHold = uxTabla.Rows[0]["CreditHold"].ToString();
                    if (CreditHold == "False")
                    {
                        if (creditoDisponible > 0)
                        {
                            uxTexBoxCreditoDisponible.Text = creditoDisponible.ToString("C");

                        }
                        else
                        {
                            uxTexBoxCreditoDisponible.Text = "0";
                        }
                        uxTexBoxCreditoDisponible.ForeColor = Color.Black;
                    }
                    else
                    {
                        uxTexBoxCreditoDisponible.Text = "Credito bloqueado";
                        uxTexBoxCreditoDisponible.ForeColor = Color.Red;

                    }
                    uxTextBoxLimiteCred.Text = limite.ToString("C");
                    uxTextBoxSaldo.Text = saldo.ToString("C");
                }
                else
                {
                    uxTextBoxSaldo.Text = "0";
                    uxTextBoxLimiteCred.Text = "0";
                    uxTexBoxCreditoDisponible.Text = "0";
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarHistorialCliente()
        {
            try
            {
                string shipTpNum = "NA";
                string cliente = uxDropDownListClientes.SelectedValue;
                if (uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                    shipTpNum = uxDropDownListDomicilio.SelectedValue;
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csClientesN cClientes = new csClientesN();
                uxTabla = cClientes.ObtenerHistorialCompraPorClienteDomicilio(cliente, shipTpNum, company);
                int c = 0;
                foreach (DataRow rows in uxTabla.Rows)
                {
                    c = c + 1;
                    rows["secuencia"] = c;
                }
                uxGridViewHistorial.DataSource = uxTabla;
                uxGridViewHistorial.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void ValidarCambio()
        {
            try
            {

                if (_accion == enumAccion.CambioGrupo)
                {
                    Session["Grupo"] = uxDropDownListGrupo.SelectedValue;
                    Session["ASPCarroDeCompras"] = null;
                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;
                    uxDropDownListClientes.Items.Clear();
                    uxDropDownListDomicilio.Items.Clear();



                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();
                    uxGridViewSaldos.DataSource = DTvacio;
                    uxGridViewSaldos.DataBind();
                    uxTextBoxNombre.Text = "";
                    uxTextBoxRcf.Text = "";
                    uxTextboxDirecion.Text = "";
                    uxTextBoxLimiteCred.Text = "";
                    uxTextBoxSaldo.Text = "";
                    uxTexBoxCreditoDisponible.Text = "";

                }
                else if (_accion == enumAccion.CambioCliente)
                {
                    Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                    Session["ASPCarroDeCompras"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;
                    uxDropDownListDomicilio.Items.Clear();


                    string listaPrecio = "", nombreCliente = "";
                    string company = this.Session["CurComp"].ToString();
                    DataTable dt = new DataTable();
                    csListaPreciosN listp = new csListaPreciosN();
                    string custnum = uxDropDownListClientes.SelectedValue;
                    string grupo = uxDropDownListGrupo.SelectedValue;
                    dt = listp.ObtenerListaPreciosByClienet(company, custnum, grupo);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow rows in dt.Rows)
                        {
                            listaPrecio = rows["ListCode"].ToString();
                            nombreCliente = rows["Name"].ToString();
                            break;
                        }
                        Session["ListaPrecios"] = listaPrecio;
                        Session["NombreCliente"] = nombreCliente;
                    }
                    CargarComboDomicilios();
                    CargarDatosCliente();
                    CargarHistorialCliente();
                    CargarSaldosDisponible();
                }
                else if (_accion == enumAccion.CambioDomicilio)
                {
                    Session["Domicilio"] = uxDropDownListDomicilio.SelectedValue.ToString();
                    Session["DomicilioE"] = uxDropDownListDomicilio.SelectedItem.ToString();

                    Session["ASPCarroDeCompras"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;

                    string listaPrecio = "", nombreCliente = "";
                    string company = this.Session["CurComp"].ToString();
                    DataTable dt = new DataTable();

                    csListaPreciosN listp = new csListaPreciosN();
                    string custnum = uxDropDownListClientes.SelectedValue;
                    string domicilio = uxDropDownListDomicilio.SelectedValue;
                    dt = listp.ObtenerListaPreciosByDomicilio(company, custnum, domicilio);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow rows in dt.Rows)
                        {
                            listaPrecio = rows["ListCode"].ToString();
                            nombreCliente = rows["Name"].ToString();
                            break;
                        }
                        Session["ListaPrecios"] = listaPrecio;
                        Session["NombreCliente"] = nombreCliente;
                    }

                    CargarDatosCliente();
                    CargarSaldosCliente();
                    CargarSaldosDisponible();
                    CargarHistorialCliente();

                }
                else if (_accion == enumAccion.LimpiarFormulario)
                {

                    Session["Grupo"] = null;
                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ASPCarroDeCompras"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;

                    CargarComboGrupos();
                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();
                    uxGridViewSaldos.DataSource = DTvacio;
                    uxGridViewSaldos.DataBind();
                    uxGridViewHistorial.DataSource = DTvacio;
                    uxGridViewHistorial.DataBind();
                    uxTextBoxNombre.Text = "";
                    uxTextBoxRcf.Text = "";
                    uxTextboxDirecion.Text = "";
                    uxTextBoxLimiteCred.Text = "";
                    uxTextBoxSaldo.Text = "";
                    uxTexBoxCreditoDisponible.Text = "";

                }

                _accion = enumAccion.Ninguno;
                Response.Redirect("clientes");

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
        public void colorEmpresa(string company)
        {
            try
            {
                if (company == "ADE001")
                {

                }
                else if (company == "FBD001")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "colorCompany", "cambiaColorEmpresa('" + company + "');", true);
                }
                else if (company == "BBY002")
                {

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarOrdenes()
        {
            try
            {
                string cliente = uxDropDownListClientes.SelectedValue;
                string company = this.Session["CurComp"].ToString();
                csOrdenVentaN cClientes = new csOrdenVentaN();
                DataTable tOrden = new DataTable();
                tOrden = cClientes.ObtenerOrdenVentaXcliente(company, Convert.ToInt32(cliente));

                //int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                //if (cantidadCarrito > 0)
                //{
                //    Session["Cambio"] = "2";
                //    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                //}
                //else
                //{
                //    var clienteID = uxDropDownListClientes.SelectedItem.Text.ToString().Split('-');
                //    Session["custID"] = clienteID[0].ToString().Trim();
                //    Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                //    string listaPrecio = "", nombreCliente = "";
                //    DataTable dt = new DataTable();
                //    csListaPreciosN listp = new csListaPreciosN();
                //    string custnum = uxDropDownListClientes.SelectedValue;
                //    string grupo = uxDropDownListGrupo.SelectedValue;
                //    dt = listp.ObtenerListaPreciosByClienet(company, custnum, grupo);
                //    if (dt.Rows.Count > 0)
                //    {
                //        foreach (DataRow rows in dt.Rows)
                //        {
                //            listaPrecio = rows["ListCode"].ToString();
                //            nombreCliente = rows["Name"].ToString();
                //            break;
                //        }
                //        Session["ListaPrecios"] = listaPrecio;
                //        Session["NombreCliente"] = nombreCliente;
                //    }
                //    CargarComboDomicilios();
                //    CargarDatosCliente();
                //    CargarHistorialCliente();
                //    CargarSaldosDisponible();


                //}
                colorEmpresa(company);
                uxGridViewOrdenes.DataSource = tOrden;
                uxGridViewOrdenes.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void To_pdf(int numeroOrden, string company)
        {
            try
            {
                string cliente = "";
                string FechaOrden = "";
                string vendedor = "";
                string nombreCliente = "";
                string rfcCliente = "";
                string nombreEmpresa = "";
                string rfcEmpresa = "";
                string DirEmpresa = "";
                string CiudadEmpresa = "";
                string sucursal = "";
                string comentario = "";
                string Subtotal = "";
                string Impuestos = "";
                string Total = "";
                string Domicilio = "";
                string Moneda = "";
                iTextSharp.text.Font f = FontFactory.GetFont("Calibri", 8, iTextSharp.text.Font.BOLD);
                string rutaPDF = WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Orden de venta " + company + " " + numeroOrden + ".pdf";

                // se va a  optener el encabezado y detalle de la order
                DataTable dtOrd = new DataTable();
                csOrdenVentaN ord = new csOrdenVentaN();
                DataTable tabletemp = new DataTable();
                tabletemp.Clear();
                tabletemp.Columns.Add("Clave", typeof(string));
                tabletemp.Columns.Add("CodigoBarras", typeof(string));
                tabletemp.Columns.Add("Unidad", typeof(string));
                tabletemp.Columns.Add("Descripcion", typeof(string));
                tabletemp.Columns.Add("Cantidad", typeof(int));
                tabletemp.Columns.Add("Precio", typeof(double));
                tabletemp.Columns.Add("Total", typeof(double));

                dtOrd = ord.ObtenerOrdenVentaPDF(this.Session["CurComp"].ToString(), numeroOrden);
                if (dtOrd.Rows.Count > 0)
                {

                    company = dtOrd.Rows[0]["Company"].ToString();
                    cliente = dtOrd.Rows[0]["CustNum"].ToString();
                    FechaOrden = dtOrd.Rows[0]["OrderDate"].ToString();
                    vendedor = dtOrd.Rows[0]["vendedor"].ToString();
                    sucursal = dtOrd.Rows[0]["Sucursal"].ToString();
                    Domicilio = dtOrd.Rows[0]["Domicilio"].ToString();
                    comentario = dtOrd.Rows[0]["OrderComment"].ToString();
                    Subtotal = dtOrd.Rows[0]["Subtotal"].ToString();
                    Impuestos = dtOrd.Rows[0]["Impuestos"].ToString();
                    Total = dtOrd.Rows[0]["TotalFinal"].ToString();
                    Moneda = dtOrd.Rows[0]["Moneda"].ToString();
                    foreach (DataRow dr in dtOrd.Rows)
                    {
                        DataRow resultRow = tabletemp.NewRow();
                        resultRow["Clave"] = dr["Clave"].ToString();
                        resultRow["CodigoBarras"] = dr["CodigoBarras"].ToString();
                        resultRow["Unidad"] = dr["Unidad"].ToString();
                        resultRow["Descripcion"] = dr["Descripcion"].ToString();
                        resultRow["Cantidad"] = Convert.ToInt32(dr["Cantidad"]);
                        resultRow["Precio"] = Convert.ToDouble(dr["Precio"]);
                        resultRow["Total"] = Convert.ToDouble(dr["Total"]);
                        tabletemp.Rows.Add(resultRow);
                    }

                }
                if (File.Exists(rutaPDF) == false)
                {
                    System.IO.FileStream fs = new FileStream(rutaPDF, FileMode.Create);
                    Document doc = new Document(PageSize.LETTER, 15, 15, 15, 15);
                    PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                    doc.AddAuthor("TI");
                    doc.AddCreator("Sample application using iTextSharp");
                    doc.AddKeywords("PDF creacion");
                    doc.AddSubject("Document subject - Describing the steps creating a PDF document");
                    doc.AddTitle("The document title - PDF creation using iTextSharp");
                    doc.Open();
                    // consultar datos de la empresa y del clientes en base a la sesion cliente y sesion empresa
                    csClientesN clie = new csClientesN();
                    DataTable tbl = new DataTable();
                    tbl = clie.ObtenerDatosClienteEmpresa(cliente, company);
                    if (tbl.Rows.Count > 0)
                    {
                        nombreCliente = tbl.Rows[0]["Name"].ToString();
                        rfcCliente = tbl.Rows[0]["ResaleID"].ToString();
                        nombreEmpresa = tbl.Rows[0]["Empresa"].ToString();
                        rfcEmpresa = tbl.Rows[0]["StateTaxID"].ToString();
                        DirEmpresa = tbl.Rows[0]["Address1"].ToString();
                        CiudadEmpresa = tbl.Rows[0]["City"].ToString();
                    }

                    // Creamos la imagen de la compañia y le ajustamos el tamaño
                    if (company == "BBY002")
                    {

                        string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-BBY"];
                        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_LEFT;
                        float percentage = 0.0f;
                        percentage = 150 / imagen.Width;
                        imagen.ScalePercent(percentage * 100);
                        imagen.SetAbsolutePosition(20, 740);
                        doc.Add(imagen);
                    }
                    else if (company == "FBD001")
                    {
                        string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-FBD"];
                        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_LEFT;
                        float percentage = 0.0f;
                        percentage = 150 / imagen.Width;
                        imagen.ScalePercent(percentage * 100);
                        imagen.SetAbsolutePosition(20, 740);
                        doc.Add(imagen);
                    }
                    else if (company == "ADE001")
                    {
                        string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-ADE"];
                        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_LEFT;
                        float percentage = 0.0f;
                        percentage = 150 / imagen.Width;
                        imagen.ScalePercent(percentage * 100);
                        imagen.SetAbsolutePosition(20, 740);
                        doc.Add(imagen);
                    }
                    Paragraph parraf = new Paragraph("No Orden: " + numeroOrden + "  ", f);
                    parraf.SpacingBefore = 0;
                    parraf.SpacingAfter = 0;
                    parraf.Alignment = 2; //0-Left, 1 middle,2 Right
                    doc.Add(parraf);
                    string remito = "Empresa: " + nombreEmpresa;
                    string envio = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                    Paragraph parrafo = new Paragraph("             Orden de Venta ", FontFactory.GetFont("CALIBRI", 15, iTextSharp.text.Font.BOLD));
                    parrafo.SpacingBefore = 0;
                    parrafo.SpacingAfter = 0;
                    parrafo.Alignment = 1; //0-Left, 1 middle,2 Right
                    doc.Add(parrafo);
                    doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cb = writer.DirectContent;
                    cb.BeginText();
                    BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb.SetFontAndSize(f_cn, 11);
                    cb.SetTextMatrix(20, 700); //(xPos, yPos) 
                    cb.ShowText("Empresa: " + nombreEmpresa);
                    cb.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cbc = writer.DirectContent;
                    cbc.BeginText();
                    BaseFont f_cnc = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc.SetFontAndSize(f_cnc, 11);
                    cbc.SetTextMatrix(300, 700); //(xPos, yPos) 
                    cbc.ShowText("Cliente: " + nombreCliente);
                    cbc.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cb1 = writer.DirectContent;
                    cb1.BeginText();
                    BaseFont f_cn1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb1.SetFontAndSize(f_cn1, 11);
                    cb1.SetTextMatrix(20, 680); //(xPos, yPos) 
                    cb1.ShowText("RFC: " + rfcEmpresa);
                    cb1.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cbc1 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc1.SetFontAndSize(f_cn, 11);
                    cbc1.SetTextMatrix(300, 680); //(xPos, yPos) 
                    cbc1.ShowText("RFC: " + rfcCliente);
                    cbc1.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cb2 = writer.DirectContent;
                    cb1.BeginText();
                    BaseFont f_cn2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb2.SetFontAndSize(f_cn2, 11);
                    cb2.SetTextMatrix(20, 660); //(xPos, yPos) 
                    cb2.ShowText("Domicilio: " + DirEmpresa);
                    cb2.EndText();
                    PdfContentByte cbc2 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc2.SetFontAndSize(f_cnc2, 11);
                    cbc2.SetTextMatrix(300, 660); //(xPos, yPos) 
                    cbc2.ShowText("Domicilio: " + Domicilio);
                    cbc2.EndText();
                    PdfContentByte cb3 = writer.DirectContent;
                    cb1.BeginText();
                    BaseFont f_cn3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb3.SetFontAndSize(f_cn3, 11);
                    cb3.SetTextMatrix(20, 640); //(xPos, yPos) 
                    cb3.ShowText("Moneda: " + Moneda);
                    cb3.EndText();
                    PdfContentByte cbc3 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc3.SetFontAndSize(f_cnc3, 11);
                    cbc3.SetTextMatrix(150, 640); //(xPos, yPos) 
                    cbc3.ShowText("vendedor: " + vendedor);
                    cbc3.EndText();
                    PdfContentByte cbc4 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc4 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc4.SetFontAndSize(f_cnc4, 11);
                    cbc4.SetTextMatrix(20, 620); //(xPos, yPos) 
                    cbc4.ShowText("Sucursal: " + sucursal);
                    cbc4.EndText();
                    PdfContentByte cbc5 = writer.DirectContent;
                    cbc1.BeginText();
                    if (FechaOrden != "")
                    {
                        BaseFont f_cnc5 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbc5.SetFontAndSize(f_cn, 11);
                        cbc5.SetTextMatrix(150, 620); //(xPos, yPos) 
                        cbc5.ShowText("Fecha: " + Convert.ToDateTime(FechaOrden).ToString("dd/MM/yyyy"));
                        cbc5.EndText();
                    }
                    doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                    doc.Add(new Paragraph("                     "));
                    GeneraDocumento(doc, tabletemp, Subtotal, Impuestos, Total);
                    doc.AddCreationDate();

                    doc.Add(new Paragraph("                                          "));
                    doc.Add(new Paragraph("                                          "));
                    doc.Add(new Paragraph("Comentario:" + comentario));
                    doc.Close();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void GeneraDocumento(Document document, DataTable dataTable, string Subtotal, string Impuestos, string Total)
        {
            try
            {

                iTextSharp.text.Font f = FontFactory.GetFont("CALIBRI", 8, iTextSharp.text.Font.NORMAL);
                decimal suma = 0;
                //Document document = new Document();
                PdfPTable table = new PdfPTable(dataTable.Columns.Count);
                table.WidthPercentage = 100;
                float[] headerwidths = { 10, 20, 10, 40, 10, 10, 15 };
                table.SetWidths(headerwidths);

                //Set columns names in the pdf file
                for (int k = 0; k < dataTable.Columns.Count; k++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(dataTable.Columns[k].ColumnName, f));
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    cell.BackgroundColor = new iTextSharp.text.BaseColor(169, 169, 169);
                    table.AddCell(cell);
                }

                //Add values of DataTable in pdf file
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        if (dataTable.Columns[j].ColumnName == "Precio" | dataTable.Columns[j].ColumnName == "Total")
                        {
                            PdfPCell cell = new PdfPCell(new Phrase("$ " + dataTable.Rows[i][j].ToString(), f));
                            //Align the cell in the center
                            cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                            cell.VerticalAlignment = PdfPCell.ALIGN_RIGHT;

                            table.AddCell(cell);
                        }
                        else if (dataTable.Columns[j].ColumnName == "Descripcion")
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(dataTable.Rows[i][j].ToString(), f));
                            //Align the cell in the center
                            cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            cell.VerticalAlignment = PdfPCell.ALIGN_LEFT;
                            table.AddCell(cell);
                        }
                        else
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(dataTable.Rows[i][j].ToString(), f));
                            //Align the cell in the center
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                            table.AddCell(cell);
                        }

                    }
                }
                foreach (DataRow dr in dataTable.Rows)
                {
                    suma = suma + Convert.ToDecimal(dr["Total"]);
                }
                // agrega subtotal
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    if (dataTable.Columns[j].ColumnName == "Total")
                    {
                        PdfPCell cell2 = new PdfPCell(new Phrase("$ " + Subtotal, f));
                        //Align the cell in the center
                        cell2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                        table.AddCell(cell2);
                    }
                    else if (dataTable.Columns[j].ColumnName == "Precio")
                    {
                        PdfPCell cell4 = new PdfPCell(new Phrase("Subtotal: ", f));
                        //Align the cell in the center
                        cell4.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell4.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell4);
                    }
                    else
                    {
                        PdfPCell cell3 = new PdfPCell(new Phrase(" "));
                        //Align the cell in the center
                        cell3.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell3.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell3.Border = 0;
                        table.AddCell(cell3);
                    }
                }
                // agrega impuestos
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    if (dataTable.Columns[j].ColumnName == "Total")
                    {
                        PdfPCell cell2 = new PdfPCell(new Phrase("$ " + Impuestos, f));
                        //Align the cell in the center
                        cell2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                        table.AddCell(cell2);
                    }
                    else if (dataTable.Columns[j].ColumnName == "Precio")
                    {
                        PdfPCell cell4 = new PdfPCell(new Phrase("Impuestos: ", f));
                        //Align the cell in the center
                        cell4.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell4.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell4);
                    }
                    else
                    {
                        PdfPCell cell3 = new PdfPCell(new Phrase(" "));
                        //Align the cell in the center
                        cell3.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell3.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        cell3.Border = 0;
                        table.AddCell(cell3);
                    }
                }
                // agrega total
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    if (dataTable.Columns[j].ColumnName == "Total")
                    {
                        PdfPCell cell2 = new PdfPCell(new Phrase("$ " + Total, f));
                        //Align the cell in the center
                        cell2.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        cell2.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                        table.AddCell(cell2);
                    }
                    else if (dataTable.Columns[j].ColumnName == "Precio")
                    {
                        PdfPCell cell4 = new PdfPCell(new Phrase("Total: ", f));
                        //Align the cell in the center
                        cell4.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell4.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        table.AddCell(cell4);
                    }
                    else
                    {
                        PdfPCell cell3 = new PdfPCell(new Phrase(" "));
                        //Align the cell in the center
                        cell3.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell3.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        cell3.Border = 0;
                        table.AddCell(cell3);
                    }

                }
                document.Add(table);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxDropDownListGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "1";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                }
                else
                {
                    uxTextBoxLimiteCred.Text = "";
                    uxTextBoxSaldo.Text = "";
                    uxTexBoxCreditoDisponible.Text = "";
                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();
                    uxGridViewSaldos.DataSource = DTvacio;
                    uxGridViewSaldos.DataBind();
                    uxGridViewHistorial.DataSource = DTvacio;
                    uxGridViewHistorial.DataBind();
                    uxGridViewOrdenes.DataSource = DTvacio;
                    uxGridViewOrdenes.DataBind();
                    uxTextBoxNombre.Text = "";
                    uxTextBoxRcf.Text = "";
                    uxTextboxDirecion.Text = "";
                    uxTextBoxLimiteCred.Text = "";
                    uxTextBoxSaldo.Text = "";
                    uxTexBoxCreditoDisponible.Text = "";

                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;

                    Session["Grupo"] = uxDropDownListGrupo.SelectedValue;

                    CargarComboClientes();
                }
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListClientes_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (Session["Grupo"] != null)
                {
                    string company = this.Session["CurComp"].ToString();
                    int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                    if (cantidadCarrito > 0)
                    {
                        Session["Cambio"] = "2";
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                    }
                    else
                    {
                        var clienteID = uxDropDownListClientes.SelectedItem.Text.ToString().Split('-');
                        Session["custID"] = clienteID[0].ToString().Trim();
                        Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                        string UserId = this.Session["DcdUserID"].ToString();
                        string custId = uxDropDownListClientes.SelectedValue;
                        Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                        csGrupoClientesN sRuta = new csGrupoClientesN();
                        uxTabla = sRuta.ObtenerClienteXgrupXCust(UserId, custId, company);
                        if (uxTabla.Rows.Count > 0)
                        {
                            uxDropDownListGrupo.SelectedValue = uxTabla.Rows[0]["GroupCode"].ToString();
                            Session["Grupo"] = uxDropDownListGrupo.SelectedValue;
                        }
                        CargarComboDomicilios();
                        CargarDatosCliente();
                        CargarHistorialCliente();
                        CargarSaldosDisponible();
                        CargarOrdenes();
                        CargarCotizaciones();
                    }
                    colorEmpresa(company);
                }
                else
                {
                    CargarComboGrupoByCust();
                    CargarComboDomicilios();
                    CargarDatosCliente();
                    CargarHistorialCliente();
                    CargarSaldosDisponible();
                    CargarOrdenes();
                    CargarCotizaciones();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListDomicilio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                string company = this.Session["CurComp"].ToString();

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "3";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                }
                else
                {
                    Session["Domicilio"] = uxDropDownListDomicilio.SelectedValue.ToString();
                    Session["DomicilioE"] = uxDropDownListDomicilio.SelectedItem.ToString();
                    string listaPrecio = "", nombreCliente = "";
                    DataTable dt = new DataTable();

                    csListaPreciosN listp = new csListaPreciosN();
                    string custnum = uxDropDownListClientes.SelectedValue;
                    string domicilio = uxDropDownListDomicilio.SelectedValue;
                    dt = listp.ObtenerListaPreciosByDomicilio(company, custnum, domicilio);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow rows in dt.Rows)
                        {
                            listaPrecio = rows["ListCode"].ToString();
                            nombreCliente = rows["Name"].ToString();
                            break;
                        }
                        Session["ListaPrecios"] = listaPrecio;
                        Session["NombreCliente"] = nombreCliente;
                    }

                    CargarDatosCliente();
                    CargarSaldosCliente();
                    CargarSaldosDisponible();
                    CargarHistorialCliente();
                }
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxGridViewHistorial_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                uxGridViewHistorial.PageIndex = e.NewPageIndex;
                CargarHistorialCliente();
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonConfirmacionCambioCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["Cambio"] != null)
                {
                    if (Session["Cambio"].ToString() == "1")
                        _accion = enumAccion.CambioGrupo;
                    else
                    if (Session["Cambio"].ToString() == "2")
                        _accion = enumAccion.CambioCliente;
                    else
                    if (Session["Cambio"].ToString() == "3")
                        _accion = enumAccion.CambioDomicilio;
                    else
                    if (Session["Cambio"].ToString() == "4")
                        _accion = enumAccion.LimpiarFormulario;
                }

                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
                ValidarCambio();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonCancelar_Click(object sender, EventArgs e)
        {
            try
            {

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "4";
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "ModalConfirmacion();", true);
                }
                else
                {
                    Session["Grupo"] = null;
                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ASPCarroDeCompras"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;

                    CargarComboGrupos();
                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();
                    uxGridViewSaldos.DataSource = DTvacio;
                    uxGridViewSaldos.DataBind();
                    uxGridViewHistorial.DataSource = DTvacio;
                    uxGridViewHistorial.DataBind();
                    uxGridViewOrdenes.DataSource = DTvacio;
                    uxGridViewOrdenes.DataBind();
                    uxTextBoxNombre.Text = "";
                    uxTextBoxRcf.Text = "";
                    uxTextboxDirecion.Text = "";
                    uxTextBoxLimiteCred.Text = "";
                    uxTextBoxSaldo.Text = "";
                    uxTexBoxCreditoDisponible.Text = "";
                }


                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void btnRealizarVenta_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("../vOperacion/OrdenVenta");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonConfirmacionCambioClienteCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["Grupo"] != null)
                {
                    uxDropDownListGrupo.SelectedValue = Convert.ToString(Session["Grupo"]);
                }
                if (Session["Cliente"] != null)
                {
                    uxDropDownListClientes.SelectedValue = Convert.ToString(Session["Cliente"]);
                }
                if (Session["Domicilio"] != null)
                {
                    uxDropDownListDomicilio.SelectedValue = Convert.ToString(Session["Domicilio"]);
                }

                Response.Redirect("Clientes.aspx");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonBuscarHistorialProducto_Click(object sender, EventArgs e)
        {
            try
            {
                string company = this.Session["CurComp"].ToString();
                if (uxDropDownListGrupo.SelectedIndex != -1 && uxDropDownListGrupo.SelectedIndex != 0
                    && uxDropDownListClientes.SelectedIndex != -1 && uxDropDownListClientes.SelectedIndex != 0)
                {
                    string domicilio = "";
                    if (uxDropDownListDomicilio.SelectedIndex != -1 && uxDropDownListDomicilio.SelectedIndex != 0)
                    {
                        domicilio = uxDropDownListDomicilio.SelectedValue.ToString();
                    }

                    csClientesN cClientes = new csClientesN();
                    DataTable datos = new DataTable();
                    // 02082019 agregar parametro de sesion["PlantID"]
                    //string PlantId = this.Session["PlantID"].ToString(); al parecer no debe ir porque el MfgSys de datos es un case para poner de que almancen biene elhistorial
                    datos = cClientes.ObtenerHistorialClientePorProducto(uxDropDownListClientes.SelectedValue, uxTextBoxCodigoProducto.Text,
                        uxTextBoxCodigoBarras.Text, domicilio, company);

                    if (datos.Rows.Count > 0)
                    {
                        uxGridViewHistorialProductos.DataSource = datos;
                        uxGridViewHistorialProductos.DataBind();
                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'precaucion', true,false,'es-mx');", true);

                }


                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxLinkButtonBotonArriba_Click(object sender, EventArgs e)
        {
            try
            {

                CargarComboGrupos();
                if (Session["Grupo"] != null)
                {
                    uxDropDownListGrupo.SelectedValue = Convert.ToString(Session["Grupo"]);
                    CargarComboClientes();
                }
                if (Session["Cliente"] != null)
                {
                    uxDropDownListClientes.SelectedValue = Convert.ToString(Session["Cliente"]);

                    CargarDatosCliente();
                    CargarSaldosDisponible();
                    CargarHistorialCliente();
                    CargarComboDomicilios();

                }
                if (Session["Domicilio"] != null)
                {
                    uxDropDownListDomicilio.SelectedValue = Convert.ToString(Session["Domicilio"]);

                    CargarDatosCliente();
                    CargarSaldosCliente();
                    CargarSaldosDisponible();
                    CargarHistorialCliente();


                }


                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxGridViewOrdenes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string NumOrden = "";
            try
            {

                if (e.CommandName == "Seleccionar")
                {
                    csGenerales generales = new csGenerales();
                    string company = this.Session["CurComp"].ToString();
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow row = uxGridViewOrdenes.Rows[index];
                    NumOrden = row.Cells[1].Text;
                    string rutaPDF =
                        WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Orden de venta " + company + " " + NumOrden + ".pdf";
                    if (File.Exists(rutaPDF))
                    {

                        //OpenFile(rutaPDF);                       


                        System.IO.FileInfo toDownload = new System.IO.FileInfo(rutaPDF);

                        if (File.Exists(rutaPDF))
                        {

                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content - Disposition", "attachment; filename =" + toDownload.Name);
                            Response.AddHeader("Content - Length", toDownload.Length.ToString());
                            Response.WriteFile(rutaPDF);
                            Response.End();
                            Response.Close();

                        }
                    }
                    else
                    {
                        //To_pdf(Convert.ToInt32(NumOrden), company);
                        generales.To_pdf(Convert.ToInt32(NumOrden), company);
                        System.IO.FileInfo toDownload = new System.IO.FileInfo(rutaPDF);

                        if (File.Exists(rutaPDF))
                        {

                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content - Disposition", "attachment; filename =" + toDownload.Name);
                            Response.AddHeader("Content - Length", toDownload.Length.ToString());
                            Response.WriteFile(rutaPDF);
                            Response.End();
                            Response.Close();

                        }
                    }

                }
                else if (e.CommandName == "Reenviar")
                {
                    string company = this.Session["CurComp"].ToString();
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow row = uxGridViewOrdenes.Rows[index];
                    NumOrden = row.Cells[1].Text;
                    Session["NumeroFolio"] = Convert.ToInt32(NumOrden);
                    Session["EsOrden"] = true;
                    Response.Redirect("/vOperacion/EnvioCorreo.aspx");
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarCotizaciones()
        {
            try
            {
                string cliente = uxDropDownListClientes.SelectedValue;
                string company = this.Session["CurComp"].ToString();
                csCotizacionN ccot = new csCotizacionN();
                DataTable tcot = new DataTable();
                tcot = ccot.ObtenerCotizacionXcliente(company, Convert.ToInt32(cliente));


                colorEmpresa(company);

                GridViewCotizaciones.DataSource = tcot;
                GridViewCotizaciones.DataBind();



            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void GridViewCotizaciones_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string NumCot = "";
            try
            {
                if (e.CommandName == "Seleccionar")
                {
                    csGenerales generales = new csGenerales();
                    string company = this.Session["CurComp"].ToString();
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow row = GridViewCotizaciones.Rows[index];
                    NumCot = row.Cells[1].Text;
                    string rutaPDF =
                        WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Cotización " + company + " " + NumCot + ".pdf";
                    if (File.Exists(rutaPDF))
                    {             //OpenFile(rutaPDF);                  

                        System.IO.FileInfo toDownload = new System.IO.FileInfo(rutaPDF);
                        if (File.Exists(rutaPDF))
                        {
                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content - Disposition", "attachment; filename =" + toDownload.Name);
                            Response.AddHeader("Content - Length", toDownload.Length.ToString());
                            Response.WriteFile(rutaPDF);
                            Response.End();
                            Response.Close();

                        }
                    }
                    else
                    {
                        generales.To_pdfCot(Convert.ToInt32(NumCot), company, Session["PlantID"].ToString());
                        //To_pdfCot(Convert.ToInt32(NumCot), company);
                        System.IO.FileInfo toDownload = new System.IO.FileInfo(rutaPDF);
                        if (File.Exists(rutaPDF))
                        {
                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content - Disposition", "attachment; filename =" + toDownload.Name);
                            Response.AddHeader("Content - Length", toDownload.Length.ToString());
                            Response.WriteFile(rutaPDF);
                            Response.End();
                            Response.Close();
                        }
                    }

                }
                else if (e.CommandName == "Reenviar")
                {
                    string company = this.Session["CurComp"].ToString();
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow row = GridViewCotizaciones.Rows[index];
                    NumCot = row.Cells[1].Text;
                    Session["NumeroFolio"] = Convert.ToInt32(NumCot);
                    Session["EsCotizacion"] = true;
                    Response.Redirect("/vOperacion/EnvioCorreo.aspx", false);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void To_pdfCot(int numeroCotizacion, string company)
        {
            try
            {
                string cliente = "";
                string FechaOrden = "";
                string vendedor = "";
                string nombreCliente = "";
                string rfcCliente = "";
                string nombreEmpresa = "";
                string rfcEmpresa = "";
                string DirEmpresa = "";
                string CiudadEmpresa = "";
                string sucursal = "";
                string comentario = "";
                string Subtotal = "";
                string Impuestos = "";
                string Total = "";
                string Domicilio = "";
                string Moneda = "";
                iTextSharp.text.Font f = FontFactory.GetFont("CALIBRI", 8, iTextSharp.text.Font.BOLD);
                string rutaPDF = WebConfigurationManager.AppSettings["rutaFormatosPDF"] + "Cotización " + company + " " + numeroCotizacion + ".pdf";

                // se va a  optener el encabezado y detalle de la order
                csCotizacionN ord = new csCotizacionN();
                DataTable dtOrd = new DataTable();
                DataTable tabletemp = new DataTable();
                tabletemp.Clear();
                tabletemp.Columns.Add("Clave", typeof(string));
                tabletemp.Columns.Add("CodigoBarras", typeof(string));
                tabletemp.Columns.Add("Unidad", typeof(string));
                tabletemp.Columns.Add("Descripcion", typeof(string));
                tabletemp.Columns.Add("Cantidad", typeof(int));
                tabletemp.Columns.Add("Precio", typeof(double));
                tabletemp.Columns.Add("Total", typeof(double));

                dtOrd = ord.ObtenerCotizacion(this.Session["CurComp"].ToString(), numeroCotizacion);
                if (dtOrd.Rows.Count > 0)
                {

                    company = dtOrd.Rows[0]["Company"].ToString();
                    cliente = dtOrd.Rows[0]["CustNum"].ToString();
                    FechaOrden = dtOrd.Rows[0]["DueDate"].ToString();
                    vendedor = dtOrd.Rows[0]["vendedor"].ToString();
                    Domicilio = dtOrd.Rows[0]["Domicilio"].ToString();
                    comentario = dtOrd.Rows[0]["QuoteComment"].ToString();
                    Subtotal = dtOrd.Rows[0]["Subtotal"].ToString();
                    Impuestos = dtOrd.Rows[0]["Impuestos"].ToString();
                    Total = dtOrd.Rows[0]["TotalFinal"].ToString();
                    Moneda = dtOrd.Rows[0]["Moneda"].ToString();
                    foreach (DataRow dr in dtOrd.Rows)
                    {
                        DataRow resultRow = tabletemp.NewRow();
                        resultRow["Clave"] = dr["Clave"].ToString();
                        resultRow["CodigoBarras"] = dr["CodigoBarras"].ToString();
                        resultRow["Unidad"] = dr["Unidad"].ToString();
                        resultRow["Descripcion"] = dr["Descripcion"].ToString();
                        resultRow["Cantidad"] = Convert.ToInt32(dr["Cantidad"]);
                        resultRow["Precio"] = Convert.ToDouble(dr["Precio"]);
                        resultRow["Total"] = Convert.ToDouble(dr["Total"]);
                        tabletemp.Rows.Add(resultRow);
                    }

                }
                if (File.Exists(rutaPDF) == false)
                {
                    System.IO.FileStream fs = new FileStream(rutaPDF, FileMode.Create);
                    Document doc = new Document(PageSize.LETTER, 15, 15, 15, 15);
                    PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                    doc.AddAuthor("TI");
                    doc.AddCreator("Sample application using iTextSharp");
                    doc.AddKeywords("PDF creacion");
                    doc.AddSubject("Document subject - Describing the steps creating a PDF document");
                    doc.AddTitle("The document title - PDF creation using iTextSharp");
                    doc.Open();
                    // consultar datos de la empresa y del clientes en base a la sesion cliente y sesion empresa
                    csClientesN clie = new csClientesN();
                    DataTable tbl = new DataTable();
                    tbl = clie.ObtenerDatosClienteEmpresa(cliente, company);
                    if (tbl.Rows.Count > 0)
                    {
                        nombreCliente = tbl.Rows[0]["Name"].ToString();
                        rfcCliente = tbl.Rows[0]["ResaleID"].ToString();
                        nombreEmpresa = tbl.Rows[0]["Empresa"].ToString();
                        rfcEmpresa = tbl.Rows[0]["StateTaxID"].ToString();
                        DirEmpresa = tbl.Rows[0]["Address1"].ToString();
                        CiudadEmpresa = tbl.Rows[0]["City"].ToString();
                    }

                    // Creamos la imagen de la compañia y le ajustamos el tamaño
                    if (company == "BBY002")
                    {

                        string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-BBY"];
                        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_LEFT;
                        float percentage = 0.0f;
                        percentage = 150 / imagen.Width;
                        imagen.ScalePercent(percentage * 100);
                        imagen.SetAbsolutePosition(20, 740);
                        doc.Add(imagen);
                    }
                    else if (company == "FBD001")
                    {
                        string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-FBD"];
                        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_LEFT;
                        float percentage = 0.0f;
                        percentage = 150 / imagen.Width;
                        imagen.ScalePercent(percentage * 100);
                        imagen.SetAbsolutePosition(20, 740);
                        doc.Add(imagen);
                    }
                    else if (company == "ADE001")
                    {
                        string rutaLogo = WebConfigurationManager.AppSettings["rutaReporteOC-ADE"];
                        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(rutaLogo);
                        imagen.BorderWidth = 0;
                        imagen.Alignment = Element.ALIGN_LEFT;
                        float percentage = 0.0f;
                        percentage = 150 / imagen.Width;
                        imagen.ScalePercent(percentage * 100);
                        imagen.SetAbsolutePosition(20, 740);
                        doc.Add(imagen);
                    }

                    Paragraph parraf = new Paragraph("No Cotización: " + numeroCotizacion + "   ", f);
                    parraf.SpacingBefore = 0;
                    parraf.SpacingAfter = 0;
                    parraf.Alignment = 2; //0-Left, 1 middle,2 Right
                    doc.Add(parraf);
                    // Insertamos la imagen en el documento            
                    string remito = "Empresa: " + nombreEmpresa;
                    string envio = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                    Paragraph parrafo = new Paragraph("             Cotización ", FontFactory.GetFont("CALIBRI", 15, iTextSharp.text.Font.BOLD));
                    parrafo.SpacingBefore = 0;
                    parrafo.SpacingAfter = 0;
                    parrafo.Alignment = 1; //0-Left, 1 middle,2 Right
                    doc.Add(parrafo);
                    doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cb = writer.DirectContent;
                    cb.BeginText();
                    BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb.SetFontAndSize(f_cn, 11);
                    cb.SetTextMatrix(20, 700); //(xPos, yPos) 
                    cb.ShowText("Empresa: " + nombreEmpresa);
                    cb.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cbc = writer.DirectContent;
                    cbc.BeginText();
                    BaseFont f_cnc = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc.SetFontAndSize(f_cnc, 11);
                    cbc.SetTextMatrix(300, 700); //(xPos, yPos) 
                    cbc.ShowText("Cliente: " + nombreCliente);
                    cbc.EndText();
                    doc.Add(new Paragraph("                         "));
                    PdfContentByte cb1 = writer.DirectContent;
                    cb1.BeginText();
                    BaseFont f_cn1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb1.SetFontAndSize(f_cn1, 11);
                    cb1.SetTextMatrix(20, 680); //(xPos, yPos) 
                    cb1.ShowText("RFC: " + rfcEmpresa);
                    cb1.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cbc1 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc1 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc1.SetFontAndSize(f_cnc1, 11);
                    cbc1.SetTextMatrix(300, 680); //(xPos, yPos) 
                    cbc1.ShowText("RFC: " + rfcCliente);
                    cbc1.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cb2 = writer.DirectContent;
                    cb1.BeginText();
                    BaseFont f_cn2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb2.SetFontAndSize(f_cn2, 11);
                    cb2.SetTextMatrix(20, 660); //(xPos, yPos) 
                    cb2.ShowText("Domicilio: " + DirEmpresa);
                    cb2.EndText();
                    doc.Add(new Paragraph("                     "));
                    PdfContentByte cbc2 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc2 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc2.SetFontAndSize(f_cnc2, 11);
                    cbc2.SetTextMatrix(300, 660); //(xPos, yPos) 
                    cbc2.ShowText("Domicilio: " + Domicilio);
                    cbc2.EndText();
                    PdfContentByte cb3 = writer.DirectContent;
                    cb1.BeginText();
                    BaseFont f_cn3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb3.SetFontAndSize(f_cn3, 11);
                    cb3.SetTextMatrix(20, 640); //(xPos, yPos) 
                    cb3.ShowText("Moneda: " + Moneda);
                    cb3.EndText();
                    //doc.Add(new Paragraph("                     "));
                    PdfContentByte cbc3 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc3 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc3.SetFontAndSize(f_cnc3, 11);
                    cbc3.SetTextMatrix(150, 640); //(xPos, yPos) 
                    cbc3.ShowText("vendedor: " + vendedor);
                    cbc3.EndText();
                    PdfContentByte cbc4 = writer.DirectContent;
                    cbc1.BeginText();
                    BaseFont f_cnc4 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cbc4.SetFontAndSize(f_cnc4, 11);
                    cbc4.SetTextMatrix(20, 620); //(xPos, yPos) 
                    cbc4.ShowText("Sucursal: " + sucursal);
                    cbc4.EndText();
                    PdfContentByte cbc5 = writer.DirectContent;
                    cbc1.BeginText();
                    if (FechaOrden != "")
                    {
                        BaseFont f_cnc5 = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        cbc5.SetFontAndSize(f_cnc5, 11);
                        cbc5.SetTextMatrix(150, 620); //(xPos, yPos)                 
                        cbc5.ShowText("Fecha: " + Convert.ToDateTime(FechaOrden).ToString("dd/MM/yyyy"));
                        cbc5.EndText();
                    }

                    doc.Add(new Paragraph("_____________________________________________________________________", FontFactory.GetFont("ARIAL", 15, iTextSharp.text.Font.BOLD, BaseColor.BLUE)));
                    doc.Add(new Paragraph("                     "));
                    GeneraDocumento(doc, tabletemp, Subtotal, Impuestos, Total);
                    doc.AddCreationDate();

                    doc.Add(new Paragraph("                                          "));
                    doc.Add(new Paragraph("                                          "));
                    doc.Add(new Paragraph("Comentario: " + comentario));
                    doc.Close();
                }


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                // return ex.Message.ToString().Replace("'", " ");
            }
        }

        [WebMethod]
        public List<string> GetEmployeeName(string empName)
        {
            try
            {
                List<string> empResult = new List<string>();
                List<object> lis = new List<object>();
                DataTable ds = new DataTable();
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csClientesN cClientes = new csClientesN();
                ds = cClientes.ObtenerDatosClienteAuto(company, user, empName);
                if (ds.Rows.Count > 0)
                {
                    DataRow row = ds.NewRow();
                    row["Name"] = "Seleccione cliente";
                    row["CustNum"] = 0;
                    ds.Rows.InsertAt(row, 0);
                    uxDropDownListClientes.DataSource = ds;
                    uxDropDownListClientes.DataTextField = "Name";
                    uxDropDownListClientes.DataValueField = "Custnum";
                    uxDropDownListClientes.DataBind();

                    foreach (DataRow row2 in ds.Rows)
                    {
                        foreach (DataColumn col in ds.Columns)
                        {

                            empResult.Add(Convert.ToString(row2[col]));
                        }
                    }

                    return empResult;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('No existe Registros', 'error', true,false,'es-mx');", true);
                    List<string> empResult3 = new List<string>();
                    return empResult3;

                }

            }
            catch (Exception ex)
            {
                List<string> empResult2 = new List<string>();
                return empResult2;
            }
        }
        private void CargarComboGrupoByCust()
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                string UserId = this.Session["DcdUserID"].ToString();
                string custId = uxDropDownListClientes.SelectedValue;
                Session["Cliente"] = uxDropDownListClientes.SelectedValue;
                csGrupoClientesN sRuta = new csGrupoClientesN();
                uxTabla = sRuta.ObtenerClienteXgrupXCust(UserId, custId, company);
                if (uxTabla.Rows.Count > 0)
                {
                    uxDropDownListGrupo.SelectedValue = uxTabla.Rows[0]["GroupCode"].ToString();
                    Session["Grupo"] = uxDropDownListGrupo.SelectedValue;

                }
                else
                {
                    CargarComboGrupos();

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {

            try
            {

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "2";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                }
                else
                {
                    Session["Grupo"] = null;
                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ASPCarroDeCompras"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;

                    CargarComboGrupos();
                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();
                    uxGridViewSaldos.DataSource = DTvacio;
                    uxGridViewSaldos.DataBind();
                    uxGridViewHistorial.DataSource = DTvacio;
                    uxGridViewHistorial.DataBind();
                    uxGridViewOrdenes.DataSource = DTvacio;
                    uxGridViewOrdenes.DataBind();
                    uxTextBoxNombre.Text = "";
                    uxTextBoxRcf.Text = "";
                    uxTextboxDirecion.Text = "";
                    uxTextBoxLimiteCred.Text = "";
                    uxTextBoxSaldo.Text = "";
                    uxTexBoxCreditoDisponible.Text = "";


                    GetEmployeeName(txtSearch.Text);

                    CargarComboGrupos();

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void BtnSearch_Click(object sender, EventArgs e)
        {

            try
            {

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                if (cantidadCarrito > 0)
                {
                    Session["Cambio"] = "2";
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                }
                else
                {
                    Session["Grupo"] = null;
                    Session["Cliente"] = null;
                    Session["Domicilio"] = null;
                    Session["DomicilioE"] = null;
                    Session["ASPCarroDeCompras"] = null;
                    Session["ListaPrecios"] = null;
                    Session["NombreCliente"] = null;
                    Session["Categorias"] = null;

                    CargarComboGrupos();
                    DataTable DTvacio = new DataTable();
                    uxDropDownListClientes.DataSource = DTvacio;
                    uxDropDownListClientes.DataBind();
                    uxDropDownListDomicilio.DataSource = DTvacio;
                    uxDropDownListDomicilio.DataBind();
                    uxGridViewSaldos.DataSource = DTvacio;
                    uxGridViewSaldos.DataBind();
                    uxGridViewHistorial.DataSource = DTvacio;
                    uxGridViewHistorial.DataBind();
                    uxGridViewOrdenes.DataSource = DTvacio;
                    uxGridViewOrdenes.DataBind();
                    uxTextBoxNombre.Text = "";
                    uxTextBoxRcf.Text = "";
                    uxTextboxDirecion.Text = "";
                    uxTextBoxLimiteCred.Text = "";
                    uxTextBoxSaldo.Text = "";
                    uxTexBoxCreditoDisponible.Text = "";


                    GetEmployeeName(txtSearch.Text);

                    CargarComboGrupos();

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
    }
}