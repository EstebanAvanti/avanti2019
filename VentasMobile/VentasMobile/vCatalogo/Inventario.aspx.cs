﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cNegocio;
using System.Data;

namespace VentasMobile.vCatalogo
{
    public partial class Inventario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string company = "";
                if (Session["CurComp"] != null)
                    company = this.Session["CurComp"].ToString();
                string categoria = "NA";

                llenarPlantas();
                llenarcategorias();
                llenarDepartamentos();
                if (Session["Categorias"] != null)
                {
                    categoria = Convert.ToString(Session["Categorias"]);
                    string PlantID = this.Session["PlantID"].ToString();
                    uxDropDownListCategoria.SelectedValue=Convert.ToString(Session["Categorias"]);
                    csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                    DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                    if (catego.Rows.Count > 0)
                    {
                        Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                        uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                        Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                        uxLabelCategoriaL.Visible = true;
                    }
                    else
                    {
                        Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                        uxLabelCategoriaVM.Text = "No se encontro categoría.";
                        Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                        uxLabelCategoriaL.Visible = true;
                    }
                   
                }

                string codigoBarras = "";
                string codigoProducto = "";
                string descripcion = "";
                string depto = "";
                string almacen = "";
                CargarProductos(uxDropDownListPlantas.SelectedValue, categoria, codigoBarras, codigoProducto, descripcion,depto, almacen);

            }
            if (String.IsNullOrEmpty((String)Session["SessionID"]))
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("~/Acceso.aspx");
            }
        }
        private void CargarProductos(string planta, string categoria, string codigoBarras, string codigoProducto, string descripcion, string depto,string almacen)
        {
            try
            {
                DataTable uxTable = new DataTable();
                string usr = Session["DcdUserID"].ToString();
                string company = Session["CurComp"].ToString();
                
                cNegocio.csProductosN Productos = new cNegocio.csProductosN();

                uxTable = Productos.ObtenerExistenciaProductos(company, planta, categoria, codigoBarras, codigoProducto, descripcion, depto,almacen);
                if (uxTable.Rows.Count > 0)
                {
                    int c = 0;
                    foreach (DataRow rows in uxTable.Rows)
                    {
                        c = c + 1;
                        rows["secuencia"] = c;
                    }
                    uxGridViewProductos.DataSource = uxTable;
                    uxGridViewProductos.DataBind();
                }
                else
                {

                    uxGridViewProductos.DataSource = uxTable;
                    uxGridViewProductos.DataBind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);

                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
        public void llenarDepositos()
        {
            try
            {

                string valorinicio = uxDropDownListPlantas.SelectedValue;
                if (valorinicio != "0")
                {
                    string company = Session["CurComp"].ToString();
                    List<KeyValuePair<int, string>> datosDepositos = new List<KeyValuePair<int, string>>()

                {
                    new KeyValuePair<int, string> (0, "Ninguno"),
                    //new KeyValuePair<int, string> (2, "CB")                 
                };
                    //Indicamos cúales van a ser los datos a asociar
                    uxDropDownListDeposito.DataSource = datosDepositos;
                    //Definimos el campo que contendrá los valores para el control
                    uxDropDownListDeposito.DataValueField = "Key";
                    //Definimos el campo que contendrá los textos que se verán en el control
                    uxDropDownListDeposito.DataTextField = "Value";
                    //Enlazamos los valores de los datos con el contenido del Control
                    uxDropDownListDeposito.DataBind();
                    csPlantasN depo = new csPlantasN();
                    DataTable dt = new DataTable();
                    dt = depo.ObtenerDepositosByPlanta(company, valorinicio);
                    if (dt.Rows.Count > 0)
                    {
                        string valor = dt.Rows[0]["BinNum"].ToString();
                        switch (valor)
                        {
                            case "DG":
                                uxDropDownListDeposito.Items.Add(new ListItem("Cancun general", dt.Rows[0]["BinNum"].ToString()));
                                break;
                            case "CB":
                                uxDropDownListDeposito.Items.Add(new ListItem("Cabos", dt.Rows[0]["BinNum"].ToString()));
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void llenarPlantas()
        {
            try
            {

                List<KeyValuePair<string, string>> datosPlantas = new List<KeyValuePair<string, string>>();
                string valorPlantas = "";
                string usr = Session["DcdUserID"].ToString();
                string company = Session["CurComp"].ToString();
                DataTable uxtablaPlantas = new DataTable();
                DataTable Planta = new DataTable();
                csPlantasN plnt = new csPlantasN();
                uxtablaPlantas = plnt.ObtenerPlantByuserCompany(usr, company);
                if (uxtablaPlantas.Rows.Count > 0)
                {
                    // hay que checar si tiene acceso a las tres empresas o a dos o a una
                    valorPlantas = uxtablaPlantas.Rows[0]["PlantList"].ToString();
                    string[] pt = valorPlantas.Split('~');
                    foreach (string pts in pt)
                    {
                        // recorer y consultar y agregar a lista
                        Planta = plnt.ObtenerPlantasbyId(pts, company);
                        if (Planta.Rows.Count > 0)
                        {
                            //Indicamos cúales van a ser los datos a asociar

                            uxDropDownListPlantas.Items.Add(new ListItem(Planta.Rows[0]["Name"].ToString(), Planta.Rows[0]["Plant"].ToString()));

                        }
                    }
                       // uxDropDownListPlantas.DataSource = datosPlantas;
                        //uxDropDownListPlantas.DataValueField = "Key";
                        //uxDropDownListPlantas.DataTextField = "Value";
                        //uxDropDownListPlantas.DataBind();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }


        }
        public void llenarcategorias()
        {
            try
            {
                string usr = Session["DcdUserID"].ToString();
                string company = Session["CurComp"].ToString();
                string PlantID = Session["PlantID"].ToString();// 20180802
                DataTable uxtablacategorias = new DataTable();
                csClaseCategoriaN cClaseCategoria = new csClaseCategoriaN();
                uxtablacategorias = cClaseCategoria.ObtenerCategorias(company, PlantID);
                if (uxtablacategorias.Rows.Count > 0)
                {// hay que checar si tiene acceso a las tres empresas o a dos o a una 
                    DataRow row = uxtablacategorias.NewRow();
                    row["Description"] = "SIN CLASE";
                    row["ClassID"] = 0;
                    uxtablacategorias.Rows.InsertAt(row, 0);
                    DataRow row2 = uxtablacategorias.NewRow();
                    row2["Description"] = "Seleccione categoría";
                    row2["ClassID"] = "NA";
                    uxtablacategorias.Rows.InsertAt(row2, 0);
                    uxDropDownListCategoria.DataSource = uxtablacategorias;
                    uxDropDownListCategoria.DataTextField = "Description";
                    uxDropDownListCategoria.DataValueField = "ClassID";
                    uxDropDownListCategoria.DataBind();

                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void colorEmpresa(string company)
        {
            try
            {
                if (company == "ADE001")
                {

                }
                else if (company == "FBD001")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "colorCompany", "cambiaColorEmpresa('" + company + "');", true);
                }
                else if (company == "BBY002")
                {

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void llenarDepartamentos()
        {
            try
            {
                string company = Session["CurComp"].ToString();
                string categoria = "";
                if (uxDropDownListCategoria.SelectedIndex != 0)
                {
                    categoria = uxDropDownListCategoria.SelectedValue;
                }

                DataTable uxtablaDep = new DataTable();
                csDepartamentoN Depto = new csDepartamentoN();
                uxtablaDep = Depto.ObtenerDepartamentos(company, categoria);
                if (uxtablaDep.Rows.Count > 0)
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "Todos";
                    row["Departamento_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    //Indicamos cúales van a ser los datos a asociar
                    uxDropDownListDepartamento.DataSource = uxtablaDep;
                    uxDropDownListDepartamento.DataValueField = "Departamento_c";
                    uxDropDownListDepartamento.DataTextField = "Character01";
                    uxDropDownListDepartamento.DataBind();
                }
                else
                {
                    DataRow row = uxtablaDep.NewRow();
                    row["Character01"] = "SIN DEPTO";
                    row["Departamento_c"] = 0;
                    uxtablaDep.Rows.InsertAt(row, 0);
                    uxDropDownListDepartamento.DataSource = uxtablaDep;
                    uxDropDownListDepartamento.DataValueField = "Departamento_c";
                    uxDropDownListDepartamento.DataTextField = "Character01";
                    uxDropDownListDepartamento.DataBind();
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxDropDownListPlantas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargarProductos(uxDropDownListPlantas.SelectedValue, "NA", "", "", "", "","");
                llenarDepositos();
                llenarcategorias();
                uxDropDownListDepartamento.ClearSelection();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
        protected void uxDropDownListDeposito_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                llenarcategorias();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session["Categorias"] = uxDropDownListCategoria.SelectedValue;
                string company = this.Session["CurComp"].ToString();
                string PlantID = this.Session["PlantID"].ToString();// 02082019
                csClaseCategoriaN cCategoria = new csClaseCategoriaN();

                string categoria = Convert.ToString(uxDropDownListCategoria.SelectedValue);
                DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria,PlantID);
                if (catego.Rows.Count > 0)
                {
                    uxDropDownListCategoria.SelectedValue = catego.Rows[0]["ClassID"].ToString();
                    Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                    uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                    Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                    uxLabelCategoriaL.Visible = true;
                }
                else
                {
                    Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                    uxLabelCategoriaVM.Text = "";
                    Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                    uxLabelCategoriaL.Visible = false;
                }
                uxTextBoxCodigoBarras.Text = "";
                uxTextBoxCodigoProducto.Text = ""; 
                uxTextBoxDescripcion.Text = "";
                if (uxDropDownListCategoria.SelectedIndex == 0)
                {
                    CargarProductos(uxDropDownListPlantas.SelectedValue, "NA", "", "", "", "","");
                    Session["Categorias"] = null;
                    llenarDepartamentos();
                }
                else
                {
                    CargarProductos(uxDropDownListPlantas.SelectedValue, uxDropDownListCategoria.SelectedValue, "", "", "", "","");
                    llenarDepartamentos();
                }


                Label uxLabelDepaV = (Label)this.Master.FindControl("uxLabelDepartamentoV");
                uxLabelDepaV.Text = "" ;
                Label uxLabelDepaL = (Label)this.Master.FindControl("uxLabelDepartamentoL");
                uxLabelDepaL.Visible = false;

                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxDropDownListDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                uxTextBoxCodigoBarras.Text = "";
                uxTextBoxCodigoProducto.Text = "";
                uxTextBoxDescripcion.Text = "";
                if (uxDropDownListDepartamento.SelectedIndex == 0)
                {
                    CargarProductos(uxDropDownListPlantas.SelectedValue, uxDropDownListCategoria.SelectedValue, "", "", "", "",""); 
                }
                else
                {
                    CargarProductos(uxDropDownListPlantas.SelectedValue, uxDropDownListCategoria.SelectedValue, "", "", "", uxDropDownListDepartamento.SelectedValue,"");
                }
                    

                Label uxLabelDepaV = (Label)this.Master.FindControl("uxLabelDepartamentoV");
                uxLabelDepaV.Text = uxDropDownListDepartamento.SelectedItem.Text;
                Label uxLabelDepaL = (Label)this.Master.FindControl("uxLabelDepartamentoL");
                uxLabelDepaL.Visible = true;
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxLinkButtonBotonArriba_Click(object sender, EventArgs e)
        {
            try
            {

                string company = this.Session["CurComp"].ToString();
                llenarPlantas();
                llenarcategorias();
                string categoria = "";
                string codigoBarras = "";
                string codigoProducto = "";
                string descripcion = "";
                string depto = "";
                CargarProductos(uxDropDownListPlantas.SelectedValue, categoria, codigoBarras, codigoProducto, descripcion, depto,"");

                
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void BuscarProducto_Click(object sender, EventArgs e)
        {
            try
            {
                string company = this.Session["CurComp"].ToString();
                if (uxDropDownListPlantas.SelectedIndex != -1)
                {
                    string categoria = "NA";
                    if (uxDropDownListCategoria.SelectedIndex != -1 )
                    {
                        categoria = uxDropDownListCategoria.SelectedValue.ToString();
                    }
                    else
                    {
                        categoria = "";
                    }
                    CargarProductos(uxDropDownListPlantas.SelectedValue, categoria, uxTextBoxCodigoBarras.Text, uxTextBoxCodigoProducto.Text, uxTextBoxDescripcion.Text.ToUpper(), "","");


                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'precaucion', true,false,'es-mx');", true);
                    uxDropDownListCategoria.Focus();
                }
                
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        
        protected void uxGridViewProductos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                uxGridViewProductos.PageIndex = e.NewPageIndex;
                string categoria = "NA";
                if (uxDropDownListCategoria.SelectedIndex != 0)
                    categoria = uxDropDownListCategoria.SelectedValue;

                CargarProductos(uxDropDownListPlantas.SelectedValue, categoria, "", "","","","");
                
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
    }
}