﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Acceso.aspx.cs" Inherits="VentasMobile.Acceso" %>
 
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="h-100">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title>Ventas Mobile</title>
    <script src="Scripts/jquery-3.3.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script> 
    <script  type="text/javascript" src="<%= Page.ResolveClientUrl("~/funciones/mensajes.js") %>" ></script>
    <script  type="text/javascript" src="<%= Page.ResolveClientUrl("~/funciones/funciones.js") %>" ></script>
    <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,700" rel="stylesheet" type="text/css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/master.css" rel="stylesheet" />
    <link href="Styles/media-queries.css" rel="stylesheet" />
</head> 
<body onkeydown = "return (event.keyCode!=13)" class="d-flex flex-column h-100 bg-login">
    <form id="formLogin" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
        <div class="full">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="float-right logo-esr">
                            <img src="images/png/logo_ESR.png" class="img-fluid mt-3"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-center mt-login">
                    <div class="card  card-login"><%--w-login--%>
                        <div class="card-header card-header-login">
                            <div class="card-title">
                                <h5 class="text-center text-bb title-card">Iniciar sesión</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate> 
                                <div class="form-group mt-3 p-form needs-validation">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text input-login"><i class="fa fa-user fa-lg text-bb"></i></div>
                                        </div>
                                            <asp:TextBox ID="uxTextBoxUserName" runat="server" class="form-control" placeholder="Usuario "></asp:TextBox>
                                        </div>
                                    <div class="invalid-feedback">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                        ErrorMessage="*Campo requerido" ControlToValidate="uxTextBoxUserName" CssClass="error invalid-feedback required-label" 
                                        SetFocusOnError="True" ></asp:RequiredFieldValidator>
                                    </div>
                                </div> 
                                <div class="form-group mt-2 p-form"> 
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text input-login"><i class="fa fa-briefcase fa-x2 text-bb"></i></div>
                                        </div>
                                        <asp:TextBox ID="uxTextBoxPassword" TextMode="Password" runat="server" AutoPostBack="false" class="form-control" placeholder="Contraseña"></asp:TextBox>
                                        </div> 
                                    <div class="invalid-feedback">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ErrorMessage="*Campo requerido" ControlToValidate="uxTextBoxPassword" CssClass="error invalid-feedback required-label" 
                                        SetFocusOnError="True" ></asp:RequiredFieldValidator>
                                    </div>
                                </div> 
                                <asp:ImageButton ID="uxImageButtonBananabay" runat="server" class="img-logoBB btn btn-light btn-bb"  ImageUrl="~/images/png/BB-login.png"
                                    OnClick="uxImageButtonBananabay_Click" />   
                                <asp:ImageButton ID="uxImageButtonFrancobolli" runat="server" class="img-logoFB btn btn-light btn-fb "  ImageUrl="~/images/png/FB-login.png"
                                    OnClick="uxImageButtonFrancobolli_Click" />
                                <div id="mesajeError" runat="server" class="alert alert-info alert-dismissible error mt-3" role="alert">
                                    <asp:Label ID="uxLabelErrorLogin" runat="server" ></asp:Label>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>    
                                </div>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>    
                <div class="row d-flex justify-content-center text-center">
                    <div class="mt-11 marcas-all">
                        <asp:Image ID="uxImageLogos" ImageUrl="~/images/png/bloque_marcas_all.png" CssClass="img-fluid" runat="server" />
                    </div>
                    <div class="mt-11 bloque-marcas">
                        <asp:Image ID="uxImageLogosBB" ImageUrl="~/images/png/bloque_marcas_BB.png" CssClass="img-fluid" runat="server" />
                    </div>
                    <div class="mt-11 bloque-marcas">
                        <asp:Image ID="uxImageLogosFB" ImageUrl="~/images/png/Bloque_marcas_FB.png" CssClass="img-fluid " runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <asp:UpdateProgress ID="uxUpdateProgressLoad" runat="server">
            <ProgressTemplate>
                <div class="overlay"></div>
                <div id="load_msj">
                    <div id="circle">
                        <div class="loader b-colorFB">
                            <div class="loader b-colorBB">
                                <div class="loader b-colorFB">
                                    <div class="loader b-colorBB">
                                        <div class="loader b-colorFB">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form> 
     <footer class="foo-login bg-gradient-bb">
        <div class="container">
            <h5 class="text-center text-white text-small">Grupo Avanti | 2019 Desarrollo TI</h5>
        </div>
    </footer> 
</body>
</html>
