﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VentasMobile.cEntidad
{
    public class ProductosAlcarro : IEquatable<ProductosAlcarro>
    {  
        public int Cantidad { get; set; }
        public double _precio { get; set; }
        public string  _descripcion { get; set; }
        public string _ListaPrecio { get; set; }
        private string _IdProducto;
        private Producto _producto = null;
        public string ListaPrecio
        {
            get { return _ListaPrecio; }
        }
        public string IdProducto
        {
            get { return _IdProducto; }
            set
            {
                _producto = null;
                _IdProducto = value;
            }
        }
        public Producto Producto
        {
            get
            {
                if (_producto == null)
                {
                    _producto = new Producto(IdProducto, _precio, _descripcion, _ListaPrecio);
                }
                return _producto;
            }
        }
        public string Descripcion
        {
            get { return Producto.Descripcion; }
        }
        public double PrecioUnitario
        {
            get { return Producto.Precio; }
        }
        public double Total
        {
            get { return PrecioUnitario * Cantidad; }
        }

        public ProductosAlcarro(string pId, double precio, string descripcion, string listaprecio)
        {
            this.IdProducto = pId;
            this._precio= precio;
            this._descripcion = descripcion;
            this._ListaPrecio = listaprecio;
        }
        public bool Equals(ProductosAlcarro pItem)
        {
            return pItem.IdProducto == IdProducto;
        }
     
      
    }
}
