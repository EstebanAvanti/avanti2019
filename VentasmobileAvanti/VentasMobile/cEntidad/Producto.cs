﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VentasMobile.cNegocio;
using System.Data;

namespace VentasMobile.cEntidad
{
    public class Producto
    {
        public string Id { get; set; }
        public double Precio { get; set; }
        public string Descripcion { get; set; }
        public string ListaPrecio { get; set; }
        public Producto(string   pId, double precio, string descripcion, string listaprecio)
        {
            Id = pId;
            Precio = precio;
            Descripcion = descripcion;
            ListaPrecio = listaprecio;
            
            //esta ser la lista de productos y precios           
            //DataTable dt = new DataTable();
            //csProductosN prod = new csProductosN();
            //dt = prod.ObtenerProductoById(Convert.ToString(Id),  clienteId,  company);
            //if (dt.Rows.Count > 0)
            //{                             
            //    Precio = Convert.ToDecimal(dt.Rows[0]["UnitPrice"].ToString());
            //    Descripcion = dt.Rows[0]["Partdescription"].ToString();
            //}


        }
    }
}