﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cEntidad;
using VentasMobile.cNegocio;
using Ice.Core;
using Erp.Common;
using Erp.BO;
using Erp.Adapters;
using Ice.Lib.Framework;
using System.Web.Configuration;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;

namespace VentasMobile.vOperacion
{
    public partial class DetalleOrdenVenta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    if (Convert.ToInt32(Session["EsCotizacion"]) == 1)
                    {
                        uxCheckBoxCotizacion.Checked = true;
                        btnImgFinalizarOrden.Enabled = false;
                    }
                    string company = "";
                    if (Session["CurComp"] != null)
                        company = this.Session["CurComp"].ToString();
                    string categoria = "";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Convert.ToString(Session["Categorias"]);
                        string PlantID = this.Session["PlantID"].ToString();// 02082019
                        csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                        DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                        if (catego.Rows.Count > 0)
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                        else
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = "No se encontro categoría.";
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                    }

                    BindData();

                    if (Session["ListaPrecios"] != null)
                    {

                        if (Session["ListaPrecios"] != null)
                            lblListaPrecio.Text = this.Session["ListaPrecios"].ToString();
                        if (Session["DomicilioE"] != null)
                            lblDomicilioEmbarque.Text = this.Session["DomicilioE"].ToString();
                        if (Session["Cliente"] != null && Session["custID"] != null)
                        {
                            lblNumeroCliente.Text = this.Session["custID"].ToString();
                            CargarSaldosDisponible();
                        }
                        if (Session["NombreCliente"] != null)
                            lblNombreCliente.Text = this.Session["NombreCliente"].ToString();

                    }


                    //CargarSaldosDisponible();



                    int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                    if (cantidadCarrito > 0)
                    {
                        CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
                        double totalCarrito = 0;
                        for (int i = 0; i < cantidadCarrito; i++)
                        {
                            totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                        }
                        lblTotalPedido.Text = totalCarrito.ToString("C");
                    }


                    ////Obtener fecha actual y fecha del dia siguiente
                    //DateTime fechaActual = DateTime.Now;
                    //DateTime fechaDiaSiguiente = DateTime.Now.AddDays(1);
                    //txtNeedBy.Text = fechaDiaSiguiente.ToString("YYYY-MM-DD");
                    //txtshipBy.Text=fechaActual.ToString("YYYY-MM-DD");

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                }
            }

            if (String.IsNullOrEmpty((String)Session["SessionID"]))
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("~/Acceso.aspx");
            }
        }
        protected void BindData()
        {
            try
            {

                gvCaritoCompras.DataSource = CarroDeCompras.CapturarProducto().ListaProductos;
                gvCaritoCompras.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        private void CargarSaldosDisponible()
        {
            try
            {
                string cliente = Session["Cliente"].ToString();
                string user = this.Session["DcdUserID"].ToString();
                string company = this.Session["CurComp"].ToString();
                csSaldosN sSaldo = new csSaldosN();
                DataTable uxTabla = sSaldo.ObtenerSaldosDisponible(cliente, company);
                if (uxTabla.Rows.Count != 0)
                {
                    double saldo = Convert.ToDouble(uxTabla.Rows[0]["SaldoDisponible"].ToString());
                    double limite = Convert.ToDouble(uxTabla.Rows[0]["limite"].ToString());
                    double creditoDisponible = Convert.ToDouble(uxTabla.Rows[0]["creditoDisponible"].ToString());

                    string CreditHold = uxTabla.Rows[0]["CreditHold"].ToString();
                    if (CreditHold == "False")
                    {
                        if (creditoDisponible > 0)
                        {
                            lblCreditoDisponible.Text = creditoDisponible.ToString("C");

                        }
                        else
                        {
                            lblCreditoDisponible.Text = "0";
                        }
                        lblCreditoDisponible.ForeColor = Color.Black;
                    }
                    else
                    {
                        lblCreditoDisponible.Text = "Credito bloqueado";
                        lblCreditoDisponible.ForeColor = Color.Red;

                    }
                    lblLimiteCredito.Text = limite.ToString("C");
                }
                else
                {
                    lblLimiteCredito.Text = "0";
                    lblCreditoDisponible.Text = "0";
                }
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                if (cantidadCarrito > 0)
                {
                    CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
                    double totalCarrito = 0;
                    for (int i = 0; i < cantidadCarrito; i++)
                    {
                        totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                    }
                    lblTotalPedido.Text = totalCarrito.ToString("C");
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        public void actualizaCarrito()
        {

            try
            {

                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                Label lblCarrito = (Label)this.Master.FindControl("lblCantidadCarrito");
                lblCarrito.Text = cantidadCarrito.ToString();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
            //Response.Redirect("OrdenVenta.aspx");
        }
        public void colorEmpresa(string company)
        {
            try
            {
                if (company == "ADE001")
                {

                }
                else if (company == "FBD001")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "colorCompany", "cambiaColorEmpresa('" + company + "');", true);
                }
                else if (company == "BBY002")
                {

                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void gvCaritoCompras_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[3].Text = "Total: " + CarroDeCompras.CapturarProducto().SubTotal().ToString("C");
            }
        }
        protected void gvCaritoCompras_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacionEliminarLinea');", true);
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void txtCantidad_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txt = sender as TextBox;
                GridViewRow row4 = txt.NamingContainer as GridViewRow;
                int index = row4.RowIndex;
                int RowIndex = ((GridViewRow)((TextBox)sender).NamingContainer).RowIndex;
                GridViewRow row2 = gvCaritoCompras.Rows[index];
                double precio = Convert.ToDouble(row2.Cells[3].Text);
                string listaPrecio = row2.Cells[4].Text; //---->29112019
                string descripcion = row2.Cells[0].Text;
                string productoId = Convert.ToString(gvCaritoCompras.DataKeys[row2.RowIndex].Value);
                int cantidad = int.Parse(txt.Text);
                string company = this.Session["CurComp"].ToString();
                string client = this.Session["Cliente"].ToString();
                string PlantID = this.Session["PlantID"].ToString();// 19122019
                DataTable Cds = new DataTable();
                csProductosN prodN = new csProductosN();
                Cds = prodN.ObtenerProdcutoById(Convert.ToString(productoId), company, listaPrecio, PlantID);
                if (Cds.Rows.Count > 0)
                {
                    CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
                    int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                    string limiteCredito = lblLimiteCredito.Text.Replace("$", "").Replace(",", "");
                    string CreditoDisponible = lblCreditoDisponible.Text.Replace("$", "").Replace(",", "");
                    //if (cantidad > Convert.ToInt16(Cds.Rows[0]["Disponible"].ToString()) && uxCheckBoxCotizacion.Checked==false)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('" + "No puede vender mas de : " + Convert.ToInt16(Cds.Rows[0]["Disponible"].ToString()) + "');", true);
                    //    foreach (GridViewRow row in gvCaritoCompras.Rows)
                    //    {
                    //        int valor = int.Parse(((TextBox)row.Cells[1].FindControl("txtCantidad")).Text);
                    //        ((TextBox)row.FindControl("txtCantidad")).Text = "1";
                    //        ((TextBox)row.FindControl("txtCantidad")).Focus();
                    //    }
                    //}
                    //else
                    //{
                    foreach (GridViewRow row in gvCaritoCompras.Rows)
                        {
                            if (row.RowType == DataControlRowType.DataRow)
                            {
                                CarroDeCompras.CapturarProducto().CantidadDeProductos(productoId, cantidad, precio, descripcion, listaPrecio);
                            }
                        }
                        double totalCarrito = 0;
                        for (int i = 0; i < cantidadCarrito; i++)
                        {
                            totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                        }
                        if (Convert.ToDouble(limiteCredito) == 0)
                        {
                            CargarSaldosDisponible();
                        }
                        else
                        {
                            if (totalCarrito > Convert.ToDouble(CreditoDisponible) && uxCheckBoxCotizacion.Checked == false)
                            {
                                CarroDeCompras.CapturarProducto().EliminarProductos(productoId, precio, descripcion, listaPrecio);
                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('8', 'precaucion', true,false,'es-mx');", true);
                            }
                        }
                    //}
                    if (cantidadCarrito > 0)
                    {
                        totalCarrito = 0;
                        for (int i = 0; i < cantidadCarrito; i++)
                        {
                            totalCarrito += Convert.ToDouble(carrito.ListaProductos[i].Total.ToString());
                        }
                        lblTotalPedido.Text = totalCarrito.ToString("C");
                    }
                }
                BindData();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void btnSeguirComprando_Click(object sender, EventArgs e)
        {
            try
            {
                this.Page.Response.Redirect(@"OrdenVenta.aspx");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonLimpiarCarrito_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacionLimpiarCarrito');", true);
            string company = this.Session["CurComp"].ToString();
            colorEmpresa(company);
        }
        protected void uxButtonFinalizarOrdenVenta_Click(object sender, EventArgs e)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacionOrdenVenta');", true);
            string company = this.Session["CurComp"].ToString();
            colorEmpresa(company);

        }
        protected void BtnFinalizarCotiza_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacionCotizacion');", true);
            string company = this.Session["CurComp"].ToString();
            colorEmpresa(company);
        }

        public int CrearOrdenVenta(string company, int custNum, string tersmCode, string shipToNum, string PONum, string comentarios,
           int salesRep, string taxRegionCode, int consigna, string fechareNecesidad, string fechaRequerida)
        {
            try
            {
                int shipViaCode = 1;
                bool resultado = false;

                // CREA LA SESSION
                csOrdenVentaN cOrden = new csOrdenVentaN();
                string direcConfig = WebConfigurationManager.AppSettings["direcionConfig"];
                string usuario = Session["sessionUsr"].ToString();
                string pass = Session["sessionPass"].ToString();
                Session episesion = new Session(usuario, pass, Ice.Core.Session.LicenseType.Default, @"" + direcConfig);

                episesion.CompanyID = company;//---->  cambio de empresa

                ILauncher oTran = new ILauncher(episesion);
                SalesOrderAdapter adapterOrder = new SalesOrderAdapter(oTran);
                string HoraEntrega = txtshipByTime.Text;

                int hora = Convert.ToInt32(HoraEntrega.Substring(0, 2));
                int minutos = Convert.ToInt32(HoraEntrega.Substring(3, 2));
                int horaInt = (hora * 3600) + (minutos * 60);

                // Se conecta
                adapterOrder.BOConnect();

                // CREA HEAD
                adapterOrder.GetNewOrderHed();

                // ACTUALIZA DATOS DEL REGISTRO CREADO
                DataRow order = adapterOrder.SalesOrderData.OrderHed[adapterOrder.SalesOrderData.OrderHed.Rows.Count - 1];
                order["CustNum"] = custNum; // ---->Requerido    PARA EL USUARIO MOSTRAR CUSTID y para EL SISTEMA MOSTRAR CUSTNUM no hacer relacion con CUSTID
                order["Company"] = company;
                order["Plant"] = Session["PlantID"].ToString();//Se agrego la planta a la orden de venta
                order["TermsCode"] = tersmCode;// ---->Requerido  Tabla del cliente (plazo de pago de factura)
                order["BTCustNum"] = custNum;// ---->Requerido   (se selecciona por si tiene un cliente Facturacion alternativa selecciona otro cliente) 
                order["ShipToCustNum"] = custNum;// ---->Requerido     Agregar el mismo numero de cliente
                order["PONum"] = PONum; // Texto para el cliente
                order["ShipToNum"] = shipToNum; // Domicilio de embarque
                order["ShipViaCode"] = shipViaCode; // En la tabla ShipVia mostrar y seleccionar descripcion por compañia (posteriormente se mostrar rutas ahi)
                order["NeedByDate"] = fechareNecesidad;
                order["RequestDate"] = fechaRequerida;
                order["TaxRegionCode"] = taxRegionCode;
                order["OrderComment"] = comentarios;
                order["InvoiceComment"] = comentarios;
                order["ShipComment"] = comentarios;
                order["ReadyToCalc"] = false;
                order["ShipByTime"] = horaInt;
                order["ReadyToFulfill"] = true;
                adapterOrder.Update();
                adapterOrder.Dispose();

                // OBTIENE PK DEL REGISTRO CREADO
                int OrderNum = Convert.ToInt32(order["OrderNum"].ToString());

                //Actualizar vendedor en el encabezado de la orden de ventamo
                resultado = cOrden.ActualizaVendedorEnOrdenVenta(company, OrderNum, salesRep);
                cOrden.ActualizaVentasMobile(company, OrderNum);
                if (consigna == 1)
                {
                    if (cOrden.ActualizaConsignaOrdenVenta(company, OrderNum) == true && resultado == true)
                    {
                        return OrderNum;
                    }
                    else
                        return 0;
                }
                else
                {
                    if (resultado == true)
                        return OrderNum;
                    else
                        return 0;
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('" + ex.Message.ToString().Replace("'", " ") + "');", true);
                return 0;
            }
        }
        public bool CrearLineasOrdenVenta(string company, int OrderNum, int custNum, string partNum, string IUM, string salesUm, string lineDesc,
            string taxCatID, string priceListCode, string breakListCode, string lineEstatus, int sellinQuantity, int orderQty,
            string prodCode, int warranty, double precioUnit, double precioExt)
        {
            try
            {
                // CREA LA SESSION
                csClientesN cClientes = new csClientesN();
                csOrdenVentaN cOrden = new csOrdenVentaN();
                DataTable datosCliente = new DataTable();
                string direcConfig = WebConfigurationManager.AppSettings["direcionConfig"];
                string usuario = Session["sessionUsr"].ToString();
                string pass = Session["sessionPass"].ToString();
                Session episesion = new Session(usuario, pass, Ice.Core.Session.LicenseType.Default, @"" + direcConfig);

                episesion.CompanyID = company;//---->  cambio de empresa

                ILauncher oTran = new ILauncher(episesion);
                SalesOrderAdapter adapterOrder = new SalesOrderAdapter(oTran);
                // Se conecta
                adapterOrder.BOConnect();

                //CREA LINEAS
                adapterOrder.GetByID(OrderNum);
                adapterOrder.GetNewOrderDtl(OrderNum);

                DataRow ship = adapterOrder.SalesOrderData.OrderDtl[adapterOrder.SalesOrderData.OrderDtl.Rows.Count - 1];
                ship["Company"] = company;// ---->Requerido
                ship["CustNum"] = custNum;// ---->Requerido
                ship["PartNum"] = partNum;// ---->Requerido
                ship["SalesUM"] = salesUm;// ---->Requerido
                ship["LineDesc"] = lineDesc;// ---->Requerido
                ship["SellingQuantity"] = sellinQuantity;
                ship["OrderQty"] = orderQty;
                ship["IUM"] = IUM;
                ship["ProdCode"] = prodCode;
                ship["TaxCatID"] = taxCatID;
                ship["PriceListCode"] = priceListCode;
                ship["BreakListCode"] = breakListCode;
                ship["Warranty"] = warranty;
                ship["UnitPrice"] = precioUnit;
                ship["DocUnitPrice"] = precioUnit;
                ship["LineStatus"] = lineEstatus;
                adapterOrder.Update();
                adapterOrder.Dispose();

                return true;

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('" + ex.Message.ToString().Replace("'", " ") + "');", true);
                return false;
            }
        }        
        public int CrearCotizacion(string company,int custID, int custNum, string tersmCode, string shipToNum, string PONum,
            string comentarios, int salesRep, string taxRegionCode,string TerritoryID)
        {
            try
            {
                int shipViaCode = 1;
                csCotizacionN cCotizacion = new csCotizacionN();
                // CREA LA SESSION
                string direcConfig = WebConfigurationManager.AppSettings["direcionConfig"];
                string usuario = Session["sessionUsr"].ToString();
                string pass = Session["sessionPass"].ToString();
                Session episesion = new Session(usuario, pass, Ice.Core.Session.LicenseType.Default, @"" + direcConfig);

                episesion.CompanyID = company;//---->  cambio de empresa

                ILauncher oTran = new ILauncher(episesion);
                QuoteAdapter adapterQuote = new QuoteAdapter(oTran);
                // Se conecta
                adapterQuote.BOConnect();

                // CREA HEAD
                adapterQuote.GetNewQuoteHed();

                // ACTUALIZA DATOS DEL REGISTRO CREADO
                DataRow Quote = adapterQuote.QuoteData.QuoteHed[adapterQuote.QuoteData.QuoteHed.Rows.Count - 1];
                Quote["Company"] = company;
                Quote["CustomerCustID"] = custID; // ---->Requerido  
                Quote["CustNum"] = custNum; // ---->Requerido 
                Quote["BTCustID"] = custID;// ---->Requerido   
                Quote["BTCustNum"] = custNum;// ---->Requerido    
                Quote["TermsCode"] = tersmCode;// ---->Requerido  
                Quote["ShipToCustNum"] = custNum;// ---->Requerido    
                Quote["PONum"] = PONum; // Texto para el cliente
                Quote["ShipToNum"] = shipToNum; // Domicilio de embarque
                Quote["ShipViaCode"] = shipViaCode; // En la tabla ShipVia mostrar y seleccionar descripcion por compañia (posteriormente se mostrar rutas ahi)
                Quote["NeedByDate"] = Convert.ToDateTime(txtNeedBy.Text);
                Quote["RequestDate"] = Convert.ToDateTime(txtshipBy.Text);
                Quote["TaxRegionCode"] = taxRegionCode;
                Quote["QuoteComment"] = comentarios;
                Quote["TerritoryID"] = TerritoryID;//Cliente
                Quote["MktgEvntSeq"] = "1";// es por compañia
                adapterQuote.Update();
                adapterQuote.Dispose();
                // OBTIENE PK DEL REGISTRO CREADO
                int QuoteNum = Convert.ToInt32(Quote["QuoteNum"].ToString());
                //----------------------------->>>>>  agregado 25092019
                
                //Actualizar ventasMobile Epicor
                if (cCotizacion.ActualizaVentasMobile(company, QuoteNum) == true)
                {
                    return QuoteNum;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('" + ex.Message.ToString().Replace("'", " ") + "');", true);
                return 0;
            }
        }
        public bool CrearLineasCotizacion(string company, int QuoteNum, int custNum, string partNum, string IUM, string salesUm, string lineDesc,
            string taxCatID, string priceListCode, string breakListCode, string lineEstatus, int sellinQuantity, int orderQty,
            string prodCode, int warranty, double precioUnit,double precioExt)
        {
            try
            {
                // CREA LA SESSION
                csClientesN cClientes = new csClientesN();
                csOrdenVentaN cOrden = new csOrdenVentaN();
                DataTable datosCliente = new DataTable();
                // CREA LA SESSION
                string direcConfig = WebConfigurationManager.AppSettings["direcionConfig"];
                string usuario = Session["sessionUsr"].ToString();
                string pass = Session["sessionPass"].ToString();
                Session episesion = new Session(usuario, pass, Ice.Core.Session.LicenseType.Default, @"" + direcConfig);

                episesion.CompanyID = company;//---->  cambio de empresa

                ILauncher oTran = new ILauncher(episesion);
                QuoteAdapter adapterQuote = new QuoteAdapter(oTran);
                // Se conecta
                adapterQuote.BOConnect();

                //CREA LINEAS
                adapterQuote.GetByID(QuoteNum);
                adapterQuote.GetNewQuoteDtl(QuoteNum);

                DataRow ship = adapterQuote.QuoteData.QuoteDtl[adapterQuote.QuoteData.QuoteDtl.Rows.Count - 1];
                ship["Company"] = company;// ---->Requerido
                ship["CustNum"] = custNum;// ---->Requerido
                ship["PartNum"] = partNum;// ---->Requerido
                ship["LineDesc"] = lineDesc;// ---->Requerido
                ship["SellingExpectedUM"] = salesUm;// ---->Requerido
                ship["SellingExpectedQty"] = sellinQuantity;
                ship["OrderQty"] = orderQty;
                ship["DspExpectedUM"] = IUM;
                ship["PartNumIUM"] = IUM;
                ship["ProdCode"] = prodCode;//prodCode;
                ship["TaxCatID"] = taxCatID;
                ship["PriceListCode"] = priceListCode;//priceListCode;
                ship["BreakListCode"] = breakListCode;//breakListCode;
                ship["Warranty"] = warranty;
                ship["ListPrice"] = precioUnit;
                ship["DocListPrice"] = precioUnit;
                ship["OrdBasedPrice"] = precioUnit;
                ship["DocOrdBasedPrice"] = precioUnit;
                ship["ExtPriceDtl"] = precioExt;
                ship["DocExtPriceDtl"] = precioExt;
                ship["LineStatus"] = lineEstatus;
                adapterQuote.Update();
                adapterQuote.Dispose();

                return true;

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('" + ex.Message.ToString().Replace("'", " ") + "');", true);
                return false;
            }
        }

        protected void uxButtonConfirmacionOrdenVenta_Click(object sender, EventArgs e)
        {
            try
            {
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                string company = Session["CurComp"].ToString();
                string planta = Session["PlantID"].ToString();
                if (cantidadCarrito != 0)
                {
                    if (Session["Grupo"] != null && Session["Domicilio"] != null && Session["Cliente"] != null)
                    {
                        if (Convert.ToDateTime(txtNeedBy.Text) > System.DateTime.Today)
                        {
                            if (Convert.ToDateTime(txtshipBy.Text) >= (System.DateTime.Today))
                            {
                                string fechareNecesidad = Convert.ToDateTime(txtNeedBy.Text).ToString("yyyy-MM-dd");
                                string fechaRequerida = Convert.ToDateTime(txtshipBy.Text).ToString("yyyy-MM-dd");
                                //Clase cliente NEGOCIO
                                csClientesN cClientes = new csClientesN();
                                csOrdenVentaN cOrden = new csOrdenVentaN();
                                DataTable datosCliente = new DataTable();
                                //Variables encabezado
                                string tersmCode = "", comentarios = "", taxRegionCode = "", shipToNum = "", PONum = "";
                                int custNum = 0, salesRep = 0;
                                //Variables lineas
                                string partNum = "", grupoCliente = "", prodCode = "", salesUm = "",
                                    lineDesc = "", IUM = "", taxCatID = "IVA16", priceListCode = "", breakListCode = "", lineEstatus = "OPEN";
                                int sellinQuantity = 0, orderQty = 0, warranty = 1, consigna = 0;
                                double precioUnit = 0, precioExt = 0;


                                custNum = Convert.ToInt32(Session["Cliente"].ToString());
                                grupoCliente = Session["Grupo"].ToString();
                                if (Session["Domicilio"].ToString() != "")
                                {
                                    shipToNum = Session["Domicilio"].ToString();
                                }
                                string user = this.Session["DcdUserID"].ToString();
                                datosCliente = cClientes.ObtenerDatosCliente(company, grupoCliente, custNum.ToString(), user, shipToNum);
                                //Asignar valores a variables encabezado
                                foreach (DataRow row in datosCliente.Rows)
                                {
                                    tersmCode = row["TermsCode"].ToString();
                                    salesRep = Convert.ToInt32(row["SalesRepCode"].ToString());
                                    taxRegionCode = row["TaxRegionCode"].ToString();
                                }
                                PONum = txtCodigoCliente.Text;
                                comentarios = txtComentario.Text;

                                if (cboxConsigna.Checked == true)
                                    consigna = 1;
                                else
                                    consigna = 0;

                                int OrderNum = CrearOrdenVenta(company, custNum, tersmCode, shipToNum, PONum,
                                    comentarios, salesRep, taxRegionCode, consigna, fechareNecesidad, fechaRequerida);

                                if (OrderNum != 0)
                                {
                                    bool resultado = false;
                                    int totalRegistros = gvCaritoCompras.Rows.Count;
                                    foreach (GridViewRow row in gvCaritoCompras.Rows)
                                    {
                                        if (row.RowIndex == totalRegistros - 1)
                                        {
                                            cOrden.ActualizaReadyToCal(company, OrderNum);
                                        }

                                        partNum = gvCaritoCompras.DataKeys[row.RowIndex].Values["IdProducto"].ToString();
                                        lineDesc = row.Cells[0].Text;
                                        if (Session["ListaPrecios"] != null)
                                        {
                                            priceListCode = row.Cells[4].Text;//this.Session["ListaPrecios"].ToString();
                                            breakListCode = row.Cells[4].Text;//this.Session["ListaPrecios"].ToString();
                                        }
                                        DataTable datosProducto = new DataTable();
                                        csProductosN cproduc = new csProductosN();
                                        datosProducto = cproduc.ObtenerProdcutoById(partNum, company, priceListCode, planta);
                                        foreach (DataRow row2 in datosProducto.Rows)
                                        {
                                            IUM = row2["IUM"].ToString();
                                            salesUm = row2["IUM"].ToString();
                                            prodCode = row2["CodigoProd"].ToString();
                                        }

                                        string txtCantidd = ((TextBox)gvCaritoCompras.Rows[row.RowIndex].FindControl("txtCantidad")).Text;
                                        sellinQuantity = Convert.ToInt32(txtCantidd);
                                        orderQty = Convert.ToInt32(txtCantidd);

                                        precioUnit = Convert.ToDouble(row.Cells[2].Text.Replace("$", ""));
                                        precioExt = Convert.ToDouble(row.Cells[3].Text.Replace("$", ""));

                                       

                                        resultado = CrearLineasOrdenVenta(company, OrderNum, custNum, partNum, IUM, salesUm, lineDesc, taxCatID,
                                            priceListCode, breakListCode, lineEstatus, sellinQuantity, orderQty, prodCode, warranty, precioUnit, precioExt);

                                        if (resultado == false)
                                            break;
                                    }

                                    if (resultado == true)
                                    {
                                        Session["NumeroFolio"] = OrderNum;
                                        Session["EsOrden"] = true;
                                        Response.Redirect("EnvioCorreo");
                                        //ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('" + "Número de Orden: " + OrderNum.ToString() + "');", true);
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'error', true,false,'es-mx');", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'error', true,false,'es-mx');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('13', 'precaucion', true,false,'es-mx');", true);

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('12', 'precaucion', true,false,'es-mx');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'precaucion', true,false,'es-mx');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'precaucion', true,false,'es-mx');", true);
                }
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonConfirmacionCotizacion_Click(object sender, EventArgs e)
        {

            try
            {
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                string planta = Session["PlantID"].ToString();
                string       company = Session["CurComp"].ToString();
                if (cantidadCarrito != 0)
                {
                    if (cboxConsigna.Checked == false)
                    {
                        if (Session["Domicilio"] != null && Session["Domicilio"] != null && Session["Domicilio"] != null)
                        {
                            //Clase cliente NEGOCIO
                            csClientesN cClientes = new csClientesN();
                            csOrdenVentaN cOrden = new csOrdenVentaN();
                            DataTable datosCliente = new DataTable();
                            //Variables encabezado
                            string tersmCode = "", comentarios = "", taxRegionCode = "", shipToNum = "", PONum = "";
                            int custNum = 0, custID = 0, salesRep = 0;
                            //Variables lineas
                            string partNum = "", grupoCliente = "", salesUm = "", lineDesc = "", prodCode = "",
                                IUM = "", taxCatID = "IVA16", priceListCode = "", breakListCode = "", lineEstatus = "OPEN", TerritoryID = "";
                            int sellinQuantity = 0, orderQty = 0, warranty = 1;
                            double precioUnit = 0, precioExt = 0;


                            custNum = Convert.ToInt32(Session["Cliente"].ToString());
                            grupoCliente = Session["Grupo"].ToString();
                            if (Session["Domicilio"].ToString() != "")
                            {
                                shipToNum = Session["Domicilio"].ToString();
                            }
                            string user = this.Session["DcdUserID"].ToString();
                            datosCliente = cClientes.ObtenerDatosCliente(company, grupoCliente, custNum.ToString(), user, shipToNum);
                            //Asignar valores a variables encabezado
                            foreach (DataRow row in datosCliente.Rows)
                            {
                                tersmCode = row["TermsCode"].ToString();
                                salesRep = Convert.ToInt32(row["SalesRepCode"].ToString());
                                custNum = Convert.ToInt32(row["CustNum"].ToString());
                                custID = Convert.ToInt32(row["CustID"].ToString());
                                taxRegionCode = row["TaxRegionCode"].ToString();
                                TerritoryID = row["TerritoryID"].ToString();
                            }
                            PONum = txtCodigoCliente.Text;
                            comentarios = txtComentario.Text;

                            int QuoteNum = CrearCotizacion(company, custID, custNum, tersmCode, shipToNum, PONum, comentarios, salesRep, taxRegionCode, TerritoryID);

                            if (QuoteNum != 0)
                            {
                                bool resultado = false;
                                foreach (GridViewRow row in gvCaritoCompras.Rows)
                                {

                                    partNum = gvCaritoCompras.DataKeys[row.RowIndex].Values["IdProducto"].ToString();
                                    lineDesc = row.Cells[0].Text;
                                    if (Session["ListaPrecios"] != null)
                                    {
                                        priceListCode = row.Cells[4].Text;//this.Session["ListaPrecios"].ToString();
                                        breakListCode = row.Cells[4].Text;// this.Session["ListaPrecios"].ToString();
                                    }
                                    DataTable datosProducto = new DataTable();
                                    csProductosN cproduc = new csProductosN();
                                    datosProducto = cproduc.ObtenerProdcutoById(partNum, company, priceListCode, planta); //------>
                                    foreach (DataRow row2 in datosProducto.Rows)
                                    {
                                        IUM = row2["IUM"].ToString();
                                        salesUm = row2["IUM"].ToString();
                                        prodCode = row2["CodigoProd"].ToString();
                                    }

                                    string txtCantidd = ((TextBox)gvCaritoCompras.Rows[row.RowIndex].FindControl("txtCantidad")).Text;
                                    sellinQuantity = Convert.ToInt32(txtCantidd);
                                    orderQty = Convert.ToInt32(txtCantidd);

                                    precioUnit = Convert.ToDouble(row.Cells[2].Text.Replace("$", ""));
                                    precioExt = Convert.ToDouble(row.Cells[3].Text.Replace("$", ""));

                                  

                                    resultado = CrearLineasCotizacion(company, QuoteNum, custNum, partNum, IUM, salesUm, lineDesc, taxCatID,
                                        priceListCode, breakListCode, lineEstatus, sellinQuantity, orderQty, prodCode, warranty, precioUnit, precioExt);

                                    if (resultado == false)
                                        break;
                                }
                                if (resultado == true)
                                {
                                    Session["EsOrden"] = false;
                                    Session["NumeroFolio"] = QuoteNum;
                                    Response.Redirect("EnvioCorreo");
                                    //ScriptManager.RegisterStartupScript(this, GetType(), "", "alert('" + "Número de corización: " + QuoteNum.ToString() + "');", true);
                                }
                                else
                                {

                                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'error', true,false,'es-mx');", true);
                                }
                            }
                            else
                            {

                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'error', true,false,'es-mx');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'precaucion', true,false,'es-mx');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('7', 'precaucion', true,false,'es-mx');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'precaucion', true,false,'es-mx');", true);
                }
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void ButtonVaciarC_Click(object sender, EventArgs e)
        {
            try
            {
                int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;

                if (cantidadCarrito != 0)
                {
                    Session["ASPCarroDeCompras"] = null;
                    DataTable carritoVacio = new DataTable();
                    gvCaritoCompras.DataSource = carritoVacio;
                    gvCaritoCompras.DataBind();

                    this.Response.Redirect("DetalleOrdenVenta");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('3', 'precaucion', true,false,'es-mx');", true);
                }
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
        protected void uxButtonEliminarLinea_Click(object sender, EventArgs e)
        {
            try
            {
                int index = Convert.ToInt32(gvCaritoCompras.SelectedIndex);
                GridViewRow row = gvCaritoCompras.Rows[index];
                double precio = Convert.ToDouble(row.Cells[3].Text.Replace("$", ""));
                string descripcion = row.Cells[0].Text;
                string listaPrecio = row.Cells[4].Text;
                string productId = gvCaritoCompras.SelectedDataKey.Value.ToString();

                CarroDeCompras.CapturarProducto().EliminarProductos(productId, precio, descripcion, listaPrecio);

                BindData();
                actualizaCarrito();
                string company = this.Session["CurComp"].ToString();
                colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxCheckBoxCotizacion_CheckedChanged(object sender, EventArgs e)
        {
            if (uxCheckBoxCotizacion.Checked == true)
            {
                btnImgFinalizarOrden.Enabled = false;
                btnImgFinalizarCotiza.Enabled = true;
                Session["EsCotizacion"] = 1;
            }
            else
            {
                btnImgFinalizarOrden.Enabled = true;
                btnImgFinalizarCotiza.Enabled = false;
                decimal tPedido = ( Convert.ToDecimal(lblTotalPedido.Text));
                decimal CredDisp = Convert.ToDecimal(lblCreditoDisponible.Text);
                if (  CredDisp < tPedido )
                {
                    int cantidadCarrito = CarroDeCompras.CapturarProducto().ListaProductos.Count;
                    if (cantidadCarrito > 0)
                    {
                        Session["EsCotizacion"] = 0;
                        Session["Cambio"] = "1";
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalConfirm", "ModalConfirmacionMostrar('ModalConfirmacion');", true);
                    }
                }
            }
        }
        protected void uxButtonConfirmacionCotizacionCancelar_Click(object sender, EventArgs e)
        {
            try
            {

                Session["EsCotizacion"] = 1;
                Response.Redirect("DetalleOrdenVenta");

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
        protected void uxButtonConfirmacionCotizacionC_Click(object sender, EventArgs e)
        {
            try
            {
                Session["ASPCarroDeCompras"] = null;
                Session["Grupo"] = null;
                Session["Cliente"] = null;
                Session["Domicilio"] = null;
                Session["DomicilioE"] = null;
                Session["ListaPrecios"] = null;
                Session["NombreCliente"] = null;
                Session["Departamento"] = null;
                Session["Marca"] = null;
                Session["Linea"] = null;
                Session["Familia"] = null;
                Response.Redirect("DetalleOrdenVenta");                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
    }
}
