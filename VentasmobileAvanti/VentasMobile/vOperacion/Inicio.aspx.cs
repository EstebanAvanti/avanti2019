﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using Ice.Core;
using Erp.Common;
using Erp.BO;
using Erp.Adapters;
using Ice.Lib.Framework;
using System.Web.Configuration;
using VentasMobile.cNegocio;

namespace VentasMobile.vOperacion
{
    public partial class Inicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string company = "";
                if (Session["CurComp"] != null)
                    company = this.Session["CurComp"].ToString();
                string categoria = "";
                if (Session["Categorias"] != null)
                {
                    categoria = Convert.ToString(Session["Categorias"]);
                    string PlantID = this.Session["PlantID"].ToString();// 02082019
                    csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                    DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                    if (catego.Rows.Count > 0)
                    {
                        Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                        uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                        Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                        uxLabelCategoriaL.Visible = true;
                    }
                    else
                    {
                        Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                        uxLabelCategoriaVM.Text = "No se encontro categoría.";
                        Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                        uxLabelCategoriaL.Visible = true;
                    }
                }

                if (Session["CurComp"]!=null)
                {
                  
                }
            }

            if (String.IsNullOrEmpty((String)Session["SessionID"]))
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("~/Acceso.aspx");
            }
        }
        
    }
}