﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EnvioCorreo.aspx.cs" Inherits="VentasMobile.vOperacion.EnvioCorreo" %>
<asp:Content ID="EnvioCorreo" ContentPlaceHolderID="MainContent" runat="server">   
    <div  id="ModalMensaje" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered " role="document">
            <div id="DivMensaje" class="modal-content">      
            </div>
          </div>
      </div>  
    <asp:UpdatePanel ID="uxUpdatePanelTabla" runat="server">
        <ContentTemplate>
            <div class="bg-white card p-3 shadow container-fluid w-carrito">
                <h2 class="text-center mb-3"> Envio de correo</h2>
                <div class="row">
                    <div class="col-12">
                        <p class="text-right font-weight-bold">Folio: <asp:Label ID="lblNumeroOrden" runat="server" ></asp:Label></p>
                        <p class=" font-weight-bold mb-0"> Cliente:</p>
                        <p class="text-muted">
                            <span>#</span>
                            <asp:Label ID="lblNumeroCliente" runat="server" ></asp:Label>
                            <i class="fa fa-arrow-right"></i>
                            <asp:Label ID="lblNombreCliente" runat="server"  ></asp:Label>
                        </p>
                        <p class=" font-weight-bold mb-0"> Domicilio embarque:</p>
                        <p class="text-muted">
                            <asp:Label ID="lblDomicilioEmbarque" runat="server"  ></asp:Label>
                        </p>
                    </div>
                    <div class="col-12 border-top pt-3">
                        <div class="row">
                            <div class="col-6">
                                <p class=" font-weight-bold mb-0"> Lista de precios</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblListaPrecio" runat="server"  ></asp:Label>
                                </p>
                            </div>
                            <div class="col-6">
                                <p class=" font-weight-bold mb-0"> Limite de crédito</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblLimiteCredito" Text="0" runat="server" ></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <p class=" font-weight-bold mb-0"> Crédito disponible</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblCreditoDisponible" Text="0" runat="server" ></asp:Label>
                                </p>
                            </div>
                            <div class="col-6">
                                <p class=" font-weight-bold mb-0"> Total pedido:</p>
                                <p class="text-muted">
                                    <asp:Label ID="lblTotalPedido" Text="0" runat="server" ></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table table-responsive">
                    <asp:GridView runat="server" ID="gvCaritoCompras" AutoGenerateColumns="false" EmptyDataText="No hay nada en su carrito de compras."
                        GridLines="None" CssClass="table" CellPadding="5" ShowFooter="true" DataKeyNames="IdProducto"
                        OnRowDataBound="gvCaritoCompras_RowDataBound" >
                        <HeaderStyle HorizontalAlign="Left" BackColor="Gray" ForeColor="AliceBlue" />
                        <FooterStyle HorizontalAlign="Right" BackColor="Gray" ForeColor="White" />
                        <AlternatingRowStyle BackColor="Azure" />
                        <Columns>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center"/>
                            <asp:BoundField DataField="PrecioUnitario" HeaderText="Precio" ItemStyle-HorizontalAlign="Right"
                                HeaderStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-HorizontalAlign="Right"
                                HeaderStyle-HorizontalAlign="Center"  />
                            <asp:BoundField DataField="ListaPrecio" HeaderText=" listPrecio" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Right"  />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p class=" font-weight-bold"> Orden de compra del cliente (OC):</p>
                    </div>
                    <div class="col-6">
                        <asp:TextBox ID="txtCodigoCliente" runat="server" class="form-control" ReadOnly></asp:TextBox>
                    </div>
                    <div class="col-6">
                        <%--<p class=" font-weight-bold"> Orden para consigna:</p>--%>
                    </div>
                    <div class="col-6">
                        <asp:CheckBox ID="cboxConsigna" Visible="false" runat="server" />
                    </div>
                    <div class="col-6">
                        <p class=" font-weight-bold"> Fecha requerida del cliente:</p>
                    </div>
                    <div class="col-6">
                        <asp:TextBox ID="txtNeedBy" runat="server" class="form-control" ReadOnly></asp:TextBox>
                    </div>
                    <div class="col-6">
                        <p class=" font-weight-bold"> Fecha de embarque:</p>
                    </div>
                    <div class="col-6">
                        <asp:TextBox ID="txtshipBy" runat="server" class="form-control" ReadOnly></asp:TextBox>
                    </div>
                      <div class="col-6">
                        <p class="font-weight-bold"> Hora de embarque:</p>
                    </div>
                    <div class="col-6">    
                        <asp:TextBox ID="txtshipByTime"  runat="server" class="form-control " ReadOnly></asp:TextBox>      
                    </div>
                    <div class="col-12 mt-2">
                        <p class=" font-weight-bold"> Comentarios:</p>
                        <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine" class="form-control" Height="110px" ReadOnly></asp:TextBox>
                    </div>
                    <div class="col-12 mt-2">
                        <p class=" font-weight-bold"> Correos a enviar:</p><p style="color:red;">(NO DISPONIBLE)</p><%--Correos deben estar separados por , --%>
                        <asp:TextBox ID="uxTextBoxCorreos" runat="server" TextMode="MultiLine" class="form-control" Height="120px"></asp:TextBox>
                    </div>
                    <div class="col-12 mt-2">
                        <p class=" font-weight-bold"> Comentario para correo:</p>
                        <asp:TextBox ID="uxTextBoxComentarioCorreo" runat="server" TextMode="MultiLine" class="form-control" Height="120px"></asp:TextBox>
                    </div>
                    <%--<div class="col-6 text-center mt-3">
                        <asp:ImageButton ID="ImgBtnEnvioCorreo" runat="server" ImageUrl="~/images/png/sendMail.png" OnClick="btnEnviaCorreo_Click" />  
                    </div>--%>
                    <div class="col-12 text-center mt-3">
                        <asp:ImageButton ID="ImgBtnTerminar" runat="server" ImageUrl="~/images/png/finish.png" OnClick="ImgBtnTerminar_Click"  />  
                    </div>
                </div>
    </div>                   
            <div id="ModalConfirmacionEnvioCorreo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-gradient-bb">
                    <h5 class="modal-title text-white text-center m-0 m-auto w-100" id="ModalCenterTitle1">Mensaje</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                   <asp:Label runat="server" ID="uxLabelFacturasAnteriores" 
                        CssClass="jqtransform sin_formato size_table" Text="¿Finalizar proceso de envío de correo?" Font-Bold="True" ></asp:Label>      
                  </div>
                  <div class="modal-footer">                    
                        <asp:Button ID="uxButtonConfirmacionEnvioCorreo" CausesValidation="False" 
                            class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar"
                            OnClick="uxButtonConfirmacionEnvioCorreo_Click" OnClientClick="ModalConfirmacionOcultarOVC('ModalConfirmacionEnvioCorreo')"/>  
                  </div>
                </div>
              </div>
            </div>            
           <div id="centralModalSuccess" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true">
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                  <div class="modal-body">                    
                        <div class="text-center">
                          <i class="fas fa-check fa-4x mb-3 animated rotateIn agregado-carrito"></i>
                            <p class="heading lead">Se envió correctamente</p>
                        </div>
                      <asp:Button ID="uxButtonAgregarC" class="btn btn-lg bg-gradient-bb btn-block text-white mt-0" runat="server" Text="Aceptar" 
                            OnClick="uxButtonAgregarC_Click"  />  
                  </div>
                </div>
              </div>
            </div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>