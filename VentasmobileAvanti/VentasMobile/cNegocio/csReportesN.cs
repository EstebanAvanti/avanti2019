﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csReportesN
    {
        csReportesD cReportesD;
        public DataTable ObtenerReporte(string UserId, string company, string claveReporte)
        {
            try
            {
                cReportesD = new csReportesD();
                return cReportesD.ObtenerReporte(UserId, company, claveReporte);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}