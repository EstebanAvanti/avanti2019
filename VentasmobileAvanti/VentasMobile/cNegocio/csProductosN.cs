﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csProductosN
    {
        csProductosD ProductoD;
        public DataTable ObtenerExistenciaProductos(string company, string planta, string categoria, string codigoBarras, string codigoProducto, string descripcion, string depto, string almacen)
        {
            try
            {
                ProductoD = new csProductosD();
                return ProductoD.ObtenerExistenciaProductos(company, planta, categoria, codigoBarras, codigoProducto, descripcion,depto, almacen);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable ObtenerProductos(bool filtros, string company, string categoria, string IdPrecioLista, string codigoBarras, string codigoProductos, string descripcion, string depto, string marca, string linea, string familia, string planta)
        {
            try
            {
                ProductoD = new csProductosD();
                return ProductoD.ObtenerProductos(filtros, company, categoria, IdPrecioLista, codigoBarras, codigoProductos, descripcion,  depto, marca, linea, familia, planta);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerProdcutoById(string PartNum, string company, string lista, string planta)
        {
            try
            {
                ProductoD = new csProductosD();
                return ProductoD.ObtenerProdcutoById(PartNum,company,  lista, planta);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
    }
}