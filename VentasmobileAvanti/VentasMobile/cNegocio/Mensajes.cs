﻿using System;
using System.Web.UI;

public class Mensajes {
        String btnCerrar = String.Empty;
        private Control _control;
        private Type _type;

        /// <summary>
        /// Se incializa la clase mensajes para crear un mensaje con estilo en JavaScript
        /// </summary>
        /// <param name="control">Epecificación del control, regularmente [this]</param>
        /// <param name="type">Especificación del tipo de control, regularmente [GetType()] </param>
        public Mensajes(Control control, Type type) {
            _control = control;
            _type = type;
        }

        //ERROR

        /// <summary>
        /// Devuelve un mensaje con estilo de error con botón cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar.</param>
        public void Error(String mensaje) {
            Error(mensaje, true);
        }

        /// <summary>
        /// Devuelve un mensaje con estilo de error y opción de un botón de cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar</param>
        /// <param name="cerrar">[Boolean] Si lleva botón de cerrar o no</param>
        public void Error(String mensaje, Boolean cerrar) {
            ScriptManager.RegisterStartupScript(_control, _type, "msjCorrecto", "mostrarMensaje('" + decode(mensaje) + "','error'," + cerrar.ToString().ToLower() + ");", true);
        }

        //INFORMACION

        /// <summary>
        /// Devuelve un mensaje con estilo de información con botón de cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar</param>
        public void Informacion(String mensaje) {
            Informacion(mensaje, true);
        }

        /// <summary>
        /// Devuelve un mensaje con estilo de información y opción de un botón de cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar</param>
        /// <param name="cerrar">[Boolean] Si lleva botón de cerrar o no</param>
        public void Informacion(String mensaje, Boolean cerrar) {
            ScriptManager.RegisterStartupScript(_control, _type, "msjInformacion", "mostrarMensaje('" + decode(mensaje) + "','informacion'," + cerrar.ToString().ToLower() + ");", true);
        }

        //CORRECTO
        /// <summary>
        /// Devuelve un mensaje con estilo 'correcto' con botón de cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar</param>
        public void Correcto(String mensaje) {
            Correcto(mensaje, true);
        }

        /// <summary>
        /// Devuelve un mensaje con estilo 'correcto' y opción de un botón de cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar</param>
        /// <param name="cerrar">[Boolean] Si lleva botón de cerrar o no</param>
        public void Correcto(String mensaje, Boolean cerrar) {
            ScriptManager.RegisterStartupScript(_control, _type, "msjCorrecto", "mostrarMensaje('" + decode(mensaje) + "','correcto'," + cerrar.ToString().ToLower() + ");", true);
        }

        //PRECAUCIÓN

        /// <summary>
        /// Devuelve un mensaje con estilo de alerta y un botón de cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar</param>
        public void Precaucion(String mensaje) {
            Precaucion(mensaje, true);
        }

        /// <summary>
        /// Devuelve un mensaje con estilo de alerta y opción de un botón de cerrar.
        /// </summary>
        /// <param name="mensaje">[String] Mensaje a mostrar</param>
        /// <param name="cerrar">[Boolean] Si lleva botón de cerrar o no</param>
        public void Precaucion(String mensaje, Boolean cerrar) {
            ScriptManager.RegisterStartupScript(_control, _type, "msjPrecaucion", "mostrarMensaje('" + decode(mensaje) + "','precaucion'," + cerrar.ToString().ToLower() + ");", true);
        }

        public void limpiar() {
            ScriptManager.RegisterStartupScript(_control, _type, "msjLimpiar", "mostrarMensaje('',0,'false','limpiar','en');", true);
        }

        private string decode(String cadena) {
            cadena = cadena.Replace('"', '\"');
            cadena = cadena.Replace("'", "\"");
            cadena = cadena.Replace("\r", "<br/>");
            cadena = cadena.Replace("\n", "<br/>");
            return cadena;
        }
    }
