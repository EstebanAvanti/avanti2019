﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csAlmacenesN
    {
        csAlmacenesD cAlmacenes;
        public DataTable ObtenerAlmacenes(string company, string planta)
        {
            try
            {
                cAlmacenes = new csAlmacenesD();
                return cAlmacenes.ObtenerAlmacenes(company, planta);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}