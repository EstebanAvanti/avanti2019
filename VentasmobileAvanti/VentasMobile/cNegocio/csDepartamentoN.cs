﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csDepartamentoN
    {
        csDepartamentoD cDepartamentos;

        public DataTable ObtenerDepartamentos(string company, string categoria)
        {
            try
            {
                cDepartamentos = new csDepartamentoD();
                return cDepartamentos.ObtenerDepartamentos(company, categoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }    
        public DataTable ObtenerMarca(string company, string categoria)
        {
            try
            {
                cDepartamentos = new csDepartamentoD();
                return cDepartamentos.ObtenerMarca(company, categoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerLinea(string company, string categoria)
        {
            try
            {
                cDepartamentos = new csDepartamentoD();
                return cDepartamentos.ObtenerLinea(company, categoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerFamilia(string company, string categoria)
        {
            try
            {
                cDepartamentos = new csDepartamentoD();
                return cDepartamentos.ObtenerFamilia(company, categoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}