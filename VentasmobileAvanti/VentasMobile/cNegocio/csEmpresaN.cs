﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csEmpresaN
    {
        csEmpresaD empresaD;
        public DataTable ObtenerEmpresa(string cliente)
        {
            try
            {
                empresaD = new csEmpresaD();
                return empresaD.ObtenerEmpresa(cliente);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}