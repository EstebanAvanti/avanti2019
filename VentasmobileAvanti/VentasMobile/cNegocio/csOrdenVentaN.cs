﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csOrdenVentaN
    {
        csOrdenVentaD cOrdenVenta;

        public bool ActualizaVendedorEnOrdenVenta(string company,int OrderNum,int SalesRep)
        {
            try
            {
                cOrdenVenta = new csOrdenVentaD();
                return cOrdenVenta.ActualizaVendedorEnOrdenVenta(company, OrderNum, SalesRep);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaReadyToCal(string company, int OrderNum)
        {
            try
            {
                cOrdenVenta = new csOrdenVentaD();
                return cOrdenVenta.ActualizaReadyToCal(company, OrderNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaConsignaOrdenVenta(string company, int OrderNum)
        {
            try
            {
                cOrdenVenta = new csOrdenVentaD();
                return cOrdenVenta.ActualizaConsignaOrdenVenta(company, OrderNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaVentasMobile(string company, int OrderNum)
        {
            try
            {
                cOrdenVenta = new csOrdenVentaD();
                return cOrdenVenta.ActualizaVentasMobile(company, OrderNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerOrdenVentaPDF(string company, int OrderNum)
        {
            try
            {
                cOrdenVenta = new csOrdenVentaD();
                return cOrdenVenta.ObtenerOrdenVentaPDF(company, OrderNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerOrdenVentaXcliente(string company, int cliente, string  fechaInicioOrd, string  fechaFinOrd)
        {
            try
            {
                cOrdenVenta = new csOrdenVentaD();
                return cOrdenVenta.ObtenerOrdenVentaXcliente(company, cliente, fechaInicioOrd, fechaFinOrd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}