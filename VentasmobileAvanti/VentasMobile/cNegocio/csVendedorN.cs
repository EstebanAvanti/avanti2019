﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csVendedorN
    {
        csVendedorD cVendedor;

        public DataTable ObtenerDatosCliente(string company, string vendedor)
        {
            try
            {
                cVendedor = new csVendedorD();
                return cVendedor.ObtenerEmailVendedor(company,vendedor);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}