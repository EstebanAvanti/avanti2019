﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using VentasMobile.cDatos;

namespace VentasMobile.cNegocio
{
    public class csClaseCategoriaN
    {


        csClaseCategoriaD cClseCategoria;
        public DataTable ObtenerCategorias(string company, string PlantID)
        {
            try
            {
                cClseCategoria = new csClaseCategoriaD();
                return cClseCategoria.ObtenerCategorias(company, PlantID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerCategoriaID(string company, string classID,string PlantID)
        {
            try
            {
                cClseCategoria = new csClaseCategoriaD();
                return cClseCategoria.ObtenerCategoriaID(company,classID, PlantID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}