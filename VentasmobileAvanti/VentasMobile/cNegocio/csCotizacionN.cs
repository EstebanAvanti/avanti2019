﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VentasMobile.cDatos;
using System.Data;

namespace VentasMobile.cNegocio
{
    public class csCotizacionN
    {
        csCotizacionD OD;

        public DataTable ObtenerCotizacion(string company, int quoteNum)
        {
            try
            {
                OD = new csCotizacionD();
                return OD.ObtenerCotizacion(company, quoteNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool ActualizaVentasMobile(string company, int OrderNum)
        {
            try
            {
                OD = new csCotizacionD();
                return OD.ActualizaVentasMobile(company, OrderNum);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        // ObtenerCotizacionXcliente
        public DataTable ObtenerCotizacionXcliente(string company, int cliente, string fechaInicioCot, string fechaFinCot)
        {
            try
            {
                OD = new csCotizacionD();
                return OD.ObtenerCotizacionXcliente(company, cliente, fechaInicioCot,  fechaFinCot);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}