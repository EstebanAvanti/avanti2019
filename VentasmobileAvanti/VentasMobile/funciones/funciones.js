﻿//// Funciones Js, Jesus Jacome
/// Fecha de creacion 06/03/2019


function mostrarMenu() {

    var navMenu = document.getElementById("accordionSidebar");
    var divContenedor = document.getElementById("content-wrapper");

    if (navMenu.classList.contains('toggled-menu')) {
        navMenu.classList.remove('toggled-menu');

        divContenedor.classList.remove('div-desbloquear');
        divContenedor.classList.add('div-bloquear');
    }
    else {
        navMenu.classList.add('toggled-menu');
        divContenedor.classList.remove('div-bloquear');
        divContenedor.classList.add('div-desbloquear');
    }    
}

function ModalConfirmacionMostrar(modal) {
    $('#' + modal).modal('show');
}
function ModalConfirmacionOcultar() {
    var div = document.getElementsByClassName("modal-backdrop fade show");
    div[0].remove();    
}
function ModalConfirmacionOcultarOVC(div) {
    var div2 = document.getElementsByClassName("modal-backdrop fade show");
    div2[0].remove();    
    var div = document.getElementById(div);
    div.style.display = "none";
    
}
function goToDiv(idName) {    
    top.location.href = "#buscarProductos";    
}

function irArriba() {
    top.location.href = "#Inicio";  
}

function irBuscarProducto() {
    top.location.href = "#buscarProductos";      
}

function ocultarNavFiltros(div) {

    var div = document.getElementById(div);
    div.style.display = "none";
}


function mostrarNavFiltros(div) {

    var div = document.getElementById(div);
    div.style.display = "block";
}