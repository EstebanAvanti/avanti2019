﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csReportesD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csReportesD()
        {
            conexion = new csConexionD();
        }
        #endregion


        #region Metodos
        public DataTable ObtenerReporte(string UserId, string company, string claveReporte)
        {
            try
            {
                string consulta = "select UrlReporte,rv.Company,Usuario from VentasMobile_ReporteVendedor as rv " +
                                    " left join VentasMobile_SeccionReporte as sr on sr.Company=rv.Company and sr.IDSeccionReporte=rv.IDSeccionReporte " +
                                    " where rv.Company=@company and Usuario=@userId and Clave=@clave and sr.Activo=1; ";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@userId",UserId),
                                                    new SqlParameter("@clave",claveReporte)

                                      };
                conexion.ConsultaReturnTabla(consulta, param, 1);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}