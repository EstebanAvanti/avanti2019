﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;


namespace VentasMobile.cDatos
{
    public class csGrupoClientesD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csGrupoClientesD()
        {
            conexion = new csConexionD();
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Metodo Obetiene Choferes
        /// </summary>
        /// jjcome
        /// <returns></returns>
        public DataTable ObtenerGrupClienteXuser(string UserId, string company)
        {
            try
            {
                string consulta = "select  cg.GroupCode,cg.GroupDesc " +
                         " from Erp.ShipTo st " +
                         " left join Erp.Customer cu   on cu.CustNum = st.CustNum and cu.Company = st.Company " +
                         " left join Erp.CustGrup cg on cg.GroupCode = cu.GroupCode  and cg.Company = cu.Company " +
                         " left join  Erp.SalesRep sr on sr.SalesRepCode = st.SalesRepCode and sr.Company = st.Company " +
                        " where cg.Company = @Company " +
                        " and st.SalesRepCode = " +
                        " (Select top 1  SalesRepCode from erp.SaleAuth as sa " +
                        " left join erp.UserFile as uf on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp " +
                        " where sa.DcdUserID = @UserId and Company = @Company)   " +
                        " group by cg.GroupCode,cg.GroupDesc "+
                        " order by cg.GroupDesc ASC ";

                SqlParameter[] param ={
                                                    new SqlParameter("@UserId",UserId),
                                                    new SqlParameter("@Company",company)
                                               };

                //Consulta pasa bd ITDev
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerDomicilioCliente(string UserId, string custId, string company)
        {
            try
            {
                string consulta = "  SELECT distinct st.ShipToNum,st.Name "/*, pl.EndDate,cpl.ListCode */
                         + " from Erp.ShipTo st "
                        + " left "
                         + " join Erp.Customer cu   on cu.CustNum = st.CustNum and cu.Company = st.Company "
                         //+ " left join erp.CustomerPriceLst cpl on cpl.ShipToNum = st.ShipToNum and cpl.CustNum = cu.CustNum  and cpl.Company = st.Company "
                         //+ " left join erp.PriceLst pl on pl.Company = cpl.Company and pl.ListCode = cpl.ListCode "
                         + " left join Erp.CustGrup cg on cg.GroupCode = cu.GroupCode  and cg.Company = cu.Company "
                         + " left join  Erp.SalesRep sr on sr.SalesRepCode = st.SalesRepCode and sr.Company = st.Company "
                         + " where st.Company = @Company "
                         + " and st.CustNum = @custId "/*and pl.EndDate >= GETDATE() */
                          + " and st.SalesRepCode = "
                         + " (Select SalesRepCode from erp.SaleAuth as sa "
                          + " left join erp.UserFile as uf  on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp   where sa.DcdUserID = @UserId and Company = st.Company)  "
                          + " or st.CustNum = @custId and st.SalesRepCode = "
                         + " (Select SalesRepCode from erp.SaleAuth as sa "
                          + " left join erp.UserFile as uf  on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp   where sa.DcdUserID = @UserId and Company = st.Company)   "/*and cpl.ListCode is null*/
                          + " group by st.Company,st.ShipToNum,st.Name,st.CustNum "/*,cpl.ListCode,cpl.SeqNum,cpl.ShipToNum, pl.EndDate*/
                          + " ORDER BY st.Name ASC;";

                SqlParameter[] param ={
                                                    new SqlParameter("@UserId",UserId),
                                                    new SqlParameter("@custId",custId),
                                                    new SqlParameter("@Company",company)
                                               };
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerClienteXgrupXuser(string UserId, string GroupId, string company)
        {
            try
            {
                string consulta = "select  cu.CustNum,cu.CustID+' - '+cu.name as Name "
                         + " from Erp.ShipTo st left "
                         + " join Erp.Customer cu   on cu.CustNum = st.CustNum and cu.Company = st.Company "
                        + " left join Erp.CustGrup cg on cg.GroupCode = cu.GroupCode  and cg.Company = cu.Company "
                        + " left join  Erp.SalesRep sr on sr.SalesRepCode = st.SalesRepCode and sr.Company = st.Company "
                            + " where cg.Company = @Company  and cg.GroupCode = @GroupId "
                            + " and st.SalesRepCode = "
                           + " (Select SalesRepCode from erp.SaleAuth as sa left join erp.UserFile as uf on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp "
                        + " where sa.DcdUserID = @UserId and Company = @Company ) "
                            + " and not cu.CreditLimit = 1 "
                           + " group by cu.CustNum,cu.Name,cu.CustId "
                           + " Order by cu.name ASC";

                SqlParameter[] param ={
                                                    new SqlParameter("@UserId",UserId),
                                                    new SqlParameter("@Company",company),
                                                     new SqlParameter("@GroupId",GroupId)
                                               };
                //Consulta pasa bd ITDev
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;

            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }     
         public DataTable ObtenerClienteXgrupXCust(string UserId, string CustID, string company)
        {
            try
            {
                string consulta = "select  cg.GroupCode " +
                        " from Erp.ShipTo st " +
                        " left join Erp.Customer cu   on cu.CustNum = st.CustNum and cu.Company = st.Company " +
                        " left join Erp.CustGrup cg on cg.GroupCode = cu.GroupCode  and cg.Company = cu.Company " +
                        " left join  Erp.SalesRep sr on sr.SalesRepCode = st.SalesRepCode and sr.Company = st.Company " +
                       " where cg.Company = @Company " +
                       " and  cu.CustNum=@CustID "+
                       " and st.SalesRepCode = " +
                       " (Select top 1  SalesRepCode from erp.SaleAuth as sa " +
                       " left join erp.UserFile as uf on uf.DcdUserID = sa.DcdUserID and sa.Company = uf.CurComp " +
                       " where sa.DcdUserID = @UserId and Company = @Company)   " +
                       " group by cg.GroupCode,cg.GroupDesc " +
                       " order by cg.GroupDesc ASC ";

                SqlParameter[] param ={
                                                    new SqlParameter("@UserId",UserId),
                                                    new SqlParameter("@Company",company),
                                                   new SqlParameter("@CustID", CustID)
                                               };

                //Consulta pasa bd ITDev
                conexion.ConsultaReturnTabla(consulta, param, 2);

                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
}
        }
        #endregion
    }
}