﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
namespace VentasMobile.cDatos
{
    public class csAlmacenesD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csAlmacenesD()
        {
            conexion = new csConexionD();
        }
        #endregion
        ///----cabos---->3
        public DataTable ObtenerAlmacenes(string company, string planta)
        {
            try
            {// Avanti para MfgSys el sareusecpde =1 
                
                    string consulta = "select distinct WarehouseCode,Description from erp.Warehse " +
                                  " where  Company = @company and plant='MfgSys' ";
                    SqlParameter[] param ={
                                                    new SqlParameter("@company",company)

                                               };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                
               
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
    }
}