﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;


namespace VentasMobile.cDatos
{
    public class csProductosD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        public csProductosD()
        {
            conexion = new csConexionD();
        }
        #endregion

        #region Metodos

        public DataTable ObtenerExistenciaProductos(string company, string planta, string categoria, string codigoBarras, string codigoProducto, string descripcion, string depto, string almacen)
        {
            try
            {
                SqlParameter[] param ={
                                            new SqlParameter("@company",company),
                                            new SqlParameter("@Planta",planta),
                                            new SqlParameter("@categoria",categoria),
                                            new SqlParameter("@codigoBarras",codigoBarras)
                                           ,new SqlParameter("@codigoProducto",codigoProducto)
                                           ,new SqlParameter("@descripcion",descripcion)
                                           ,new SqlParameter("@depto",depto)
                                           ,new SqlParameter("@almacen",almacen)
                                       };
                conexion.ConsultaReturnTablasp("SP_ObtenerExistenciaProductos", param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        // Se obtiene el producto son disponible 0 y con cantidad ademas de los filtros necesarios---------------------->
        public DataTable ObtenerProductos(bool filtros, string company, string categoria, string IdPrecioLista, string codigoBarras, string codigoProductos, string descripcion, string depto, string marca, string linea, string familia, string planta)
        {
            try
            {
                SqlParameter[] param ={
                                            new SqlParameter("@company",company),
                                            new SqlParameter("@categoria",categoria),
                                            new SqlParameter("@IdPrecioLista",IdPrecioLista),
                                            new SqlParameter("@codigoBarras",codigoBarras)
                                           ,new SqlParameter("@codigoProducto",codigoProductos)
                                           ,new SqlParameter("@descripcion",descripcion)
                                           ,new SqlParameter("@depto",depto)
                                           ,new SqlParameter("@marca",marca)
                                           ,new SqlParameter("@linea",linea)
                                           ,new SqlParameter("@familia",familia)
                                           ,new SqlParameter("@Planta",planta)
                                       };
                conexion.ConsultaReturnTablasp("SP_ObtenerProductos", param, 2);
                return conexion.Table;
                
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }


        // se obtiene informacion del producto 
        public DataTable ObtenerProdcutoById( string PartNum, string company, string lista, string planta)// aqui no esta agregada la plata por que siempre sera la primera condicion
        {
            try
            {
                if (company == "ADE001" || company == "FBD001" || planta=="MfgSys" )
                {
                    string consulta = "Select e.Company, e.PartNum, e.PartDescription,isnull( ppc.ProdCode,'') as ProdCode,e.ProdCode as CodigoProd, e.ClassID, isnull (pc.Description,'Sin Clase') as Clase, " +
                                    " e.WarehouseCode,   CAST(sum (e.Exis) - sum (e.Asignado) as int) as Disponible ,e.IUM , ISNULL(e.ListDescription, '') as ListDescription, str(ISNULL(e.BasePrice, 0),8,9) as BasePrice " +
                                    " From( " +
                                            " Select p.Company, p.PartNum, p.PartDescription, p.IUM, p.ClassID,p.ProdCode, pw.WarehouseCode, pb.BinNum, wb.Description, isnull(pb.OnhandQty, 0) as Exis, " +
                                            " isnull(pb.AllocatedQty, 0) as Asignado, wb.NonNettable, wb.ZoneID, lp.ListDescription, lp.BasePrice " +
                                            " From Erp.Part as p " +
                                                " Left Join Erp.PartWhse as pw  On pw.Company = p.Company  and pw.PartNum = p.PartNum " +
                                                " left Join Erp.PartBin as pb  on  pw.Company = pb.Company  and pw.PartNum = pb.PartNum  and pw.WarehouseCode = pb.WarehouseCode " +
                                                " Left Join Erp.WhseBin as wb " +
                                                " on wb.Company = pb.Company  and wb.WarehouseCode = pb.WarehouseCode  and wb.BinNum = pb.BinNum  and(wb.ZoneID = 'R') and(wb.NonNettable = 0) " +
                                                " Left Join( " +
                                                            " Select pl.Company, pl.ListCode, pl.ListDescription, pl.StartDate, pl.EndDate, pp.PartNum, pp.BasePrice, pp.UOMCode From Erp.PriceLst pl " +
                                                              " Left Join Erp.PriceLstParts as pp   on pp.Company = pl.Company  and pp.ListCode = pl.ListCode   " +
                                                                " where pl.ListCode = @lista and pl.Company = @Company " +
                                                            " ) as lp " +
                                                        " on lp.Company = p.Company   and lp.PartNum = p.PartNum   and lp.UOMCode = p.IUM " +
                                        " ) as e Left Join Erp.PartClass as pc  on pc.Company = e.Company  and pc.ClassID = e.ClassID " +
                                            " Left Join Erp.PartPC as ppc  on ppc.Company = e.Company   and ppc.PartNum = e.PartNum    and ppc.UOMCode = e.IUM " +
                                    " where e.Company = @Company and e.warehousecode = '1' and e.PartNum=@partNum  " +
                                    " Group by e.Company,	e.PartNum, e.PartDescription, ppc.ProdCode,e.ProdCode, e.ClassID,e.IUM,pc.Description,e.WarehouseCode, e.ListDescription, e.BasePrice";
                    SqlParameter[] param ={
                                           new SqlParameter("@PartNum",PartNum),
                                             new SqlParameter("@Company",company),
                                             new SqlParameter("@lista",lista)

                                            };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
                else if (company == "BBY002" || planta == "MfgSys")
                {
                    string consulta = "Select e.Company, e.PartNum, e.PartDescription,isnull( ppc.ProdCode,'') as ProdCode,e.ProdCode as CodigoProd, e.ClassID, isnull (pc.Description,'Sin Clase') as Clase, " +
                                    " e.WarehouseCode,  CAST(sum (e.Exis) - sum (e.Asignado) as int) as Disponible,e.IUM , ISNULL(e.ListDescription, '') as ListDescription, str(ISNULL(e.BasePrice, 0),8,9) as BasePrice " +
                                    " From( " +
                                            " Select p.Company, p.PartNum, p.PartDescription, p.IUM, p.ClassID,p.ProdCode, pw.WarehouseCode, pb.BinNum, wb.Description, isnull(pb.OnhandQty, 0) as Exis, " +
                                            " isnull(pb.AllocatedQty, 0) as Asignado, wb.NonNettable, wb.ZoneID, lp.ListDescription, lp.BasePrice " +
                                            " From Erp.Part as p " +
                                                " Left Join Erp.PartWhse as pw  On pw.Company = p.Company  and pw.PartNum = p.PartNum " +
                                                " left Join Erp.PartBin as pb  on  pw.Company = pb.Company  and pw.PartNum = pb.PartNum  and pw.WarehouseCode = pb.WarehouseCode " +
                                                " Left Join Erp.WhseBin as wb " +
                                                " on wb.Company = pb.Company  and wb.WarehouseCode = pb.WarehouseCode  and wb.BinNum = pb.BinNum  and(wb.ZoneID = 'R') and(wb.NonNettable = 0) " +
                                                " Left Join( " +
                                                            " Select pl.Company, pl.ListCode, pl.ListDescription, pl.StartDate, pl.EndDate, pp.PartNum, pp.BasePrice, pp.UOMCode From Erp.PriceLst pl " +
                                                              " Left Join Erp.PriceLstParts as pp   on pp.Company = pl.Company  and pp.ListCode = pl.ListCode   " +
                                                                " where pl.ListCode = '' and pl.Company = @Company " +
                                                            " ) as lp " +
                                                        " on lp.Company = p.Company   and lp.PartNum = p.PartNum   and lp.UOMCode = p.IUM " +
                                        " ) as e Left Join Erp.PartClass as pc  on pc.Company = e.Company  and pc.ClassID = e.ClassID " +
                                            " Left Join Erp.PartPC as ppc  on ppc.Company = e.Company   and ppc.PartNum = e.PartNum    and ppc.UOMCode = e.IUM " +
                                    " where e.Company = @Company and e.warehousecode = '2' and e.PartNum=@partNum  " +
                                    " Group by e.Company,	e.PartNum, e.PartDescription, ppc.ProdCode,e.ProdCode, e.ClassID,e.IUM,pc.Description,e.WarehouseCode, e.ListDescription, e.BasePrice";

                    SqlParameter[] param ={
                                          new SqlParameter("@PartNum",PartNum),
                                            new SqlParameter("@Company",company)
                                            };
                    conexion.ConsultaReturnTabla(consulta, param, 2);
                    return conexion.Table;
                }
              
                else
                {
                    return new DataTable();
                }
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}