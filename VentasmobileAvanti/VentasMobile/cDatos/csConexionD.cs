﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VentasMobile.cDatos
{
    public class csConexionD
    {
        private string strConnStringITdev;
        private string strConnStringEpicor;
        private string error;
        private SqlCommand Query;
        private SqlConnection Conexion;
        private SqlDataAdapter consultar;
        private DataTable table;

        #region AtributosGetSet
        /// <summary>
        /// Atributo Error
        /// </summary>
        public string Error
        {
            get { return error; }
            set { error = value; }
        }

        /// <summary>
        /// Atributo del DataTable
        /// </summary>
        public DataTable Table
        {
            get { return table; }
            set { table = value; }
        }
        #endregion

        /// <summary>
        /// Constructor de la clase ConexionSql
        /// </summary>
        public csConexionD()
        {
            strConnStringITdev = ConfigurationManager.ConnectionStrings["Sql_cadena_ITDev"].ConnectionString;
            strConnStringEpicor = ConfigurationManager.ConnectionStrings["Sql_cadena_Epicor"].ConnectionString;
        }

        #region Metodos        
        /// <summary>
        /// Metodo para abrir la conexion a la base de datos
        /// </summary>
        /// <returns>True-False</returns>
        private bool Open(int conBD)
        {
            try
            {
                if (conBD == 1)
                    Conexion = new SqlConnection(strConnStringITdev);
                else if (conBD == 2)
                    Conexion = new SqlConnection(strConnStringEpicor);
                if (Conexion.State == System.Data.ConnectionState.Closed)
                {
                    Conexion.Open();
                    return true;
                }
            }
            catch { }
            return false;
        }

        /// <summary>
        /// Metodo para cerrar la conexion a la base de datos
        /// </summary>
        /// //jjcome
        /// <returns>true_flase</returns>
        private bool Close()
        {

            try
            {
                if (Conexion.State == System.Data.ConnectionState.Open)
                {
                    Conexion.Close();
                    SqlConnection.ClearAllPools();
                    return true;
                }

            }
            catch { }
            return false;

        }

        /// <summary>
        /// metodo que realiza consulta y regresa datos en un datatable
        /// </summary>
        /// jjcome
        /// <returns>true_flase</returns>        
        public DataTable ConsultaReturnTabla(string consulta, SqlParameter[] parametros, int baseDatos)
        {
            try
            {
                table = new DataTable();
                Open(baseDatos);
                Query = new SqlCommand();
                Query.Connection = Conexion;
                Query.CommandText = consulta;
                Query.CommandType = CommandType.Text;
                if (parametros != null) { Query.Parameters.AddRange(parametros); }

                consultar = new SqlDataAdapter(Query);
                consultar.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                error = ex.Message.ToString().Replace("'", " ");
                return table = new DataTable();
            }
            finally
            {
                Close();
            }
        }

        /// <summary>
        /// //metodo que realiza consulta y regresa datos en un datatable
        /// </summary>
        /// jjcome
        /// <returns>true_flase</returns>               
        public bool ConsultaInsertUpdate(string consulta, SqlParameter[] parametros, int baseDatos)
        {
            try
            {
                Open(baseDatos);
                Query = new SqlCommand();
                Query.Connection = Conexion;
                Query.CommandText = consulta;
                Query.CommandType = CommandType.Text;
                if (parametros != null) { Query.Parameters.AddRange(parametros); }
                Query.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                error = ex.Message.ToString().Replace("'", " ");
                return false;
            }
            finally
            {
                Close();
            }
        }


        /// <summary>
        /// metodo que realiza 2 consultas e inserta los datos en 2 tablas
        /// </summary>
        /// sdelvalle
        /// <returns>true_flase</returns>        
        public bool SQLEjecutaInsertaDosTablas(string consulta1, string consulta2, SqlParameter[] parametros, SqlParameter[] parametros2, int conBD)
        {
            bool valor = false;
            string cadena = "";
            if (conBD == 1)
                cadena = strConnStringITdev;
            else if (conBD == 2)
                cadena = strConnStringEpicor;
            using (SqlConnection connection = new SqlConnection(cadena))
            {
                connection.Open();
                SqlCommand Query = new SqlCommand(consulta1, Conexion);
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                long valorReturn;
                Query.Connection = connection;
                Query.Transaction = transaction;
                Query.CommandText = consulta1;
                Query.CommandType = CommandType.Text;
                try
                {
                    if (parametros != null)
                    {
                        Query.Parameters.AddRange(parametros);
                    }
                    valorReturn = Convert.ToInt64(Query.ExecuteScalar().ToString());

                    if (!valorReturn.Equals(""))
                    {
                        Query.CommandText = null;
                        Query.Parameters.Clear();
                        Query.CommandText = consulta2;

                        if (parametros != null)
                        {
                            Query.Parameters.AddRange(parametros2);
                        }
                        Query.Parameters.Add(new SqlParameter("@pkValorReturn", valorReturn));

                        Query.ExecuteNonQuery();
                        Query.Parameters.Clear();

                        transaction.Commit();
                        valor = true;
                    }
                    else
                        transaction.Rollback();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    valor = false;
                    try
                    {
                        transaction.Rollback();
                        valor = false;
                    }
                    catch (Exception exep)
                    {
                        error = exep.Message;
                        valor = false;
                    }
                }
                finally
                {
                    Close();
                }
                return valor;
            }
        }


        /// <summary>
        /// metodo que realiza insert y retorna pk
        /// </summary>
        /// sdelvalle
        /// <returns>true_flase</returns>        
        public int SQLEjecutaInserta(string consulta, SqlParameter[] parametros, int conBD)
        {
            int valor = 0;
            string cadena = "";
            if (conBD == 1)
                cadena = strConnStringITdev;
            else if (conBD == 2)
                cadena = strConnStringEpicor;
            using (SqlConnection connection = new SqlConnection(cadena))
            {
                connection.Open();
                SqlCommand Query = new SqlCommand(consulta, Conexion);
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                long valorReturn;
                Query.Connection = connection;
                Query.Transaction = transaction;
                Query.CommandText = consulta;
                Query.CommandType = CommandType.Text;
                try
                {
                    if (parametros != null)
                    {
                        Query.Parameters.AddRange(parametros);
                    }
                    valorReturn = Convert.ToInt64(Query.ExecuteScalar().ToString());

                    if (!valorReturn.Equals(""))
                    {
                        valor = Convert.ToInt32(valorReturn);
                        transaction.Commit();
                    }
                    else
                        transaction.Rollback();


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    valor = 0;
                    try
                    {
                        transaction.Rollback();
                        valor = 0;
                    }
                    catch (Exception exep)
                    {
                        error = exep.Message;
                        valor = 0;
                    }
                }
                finally
                {
                    Close();
                }
                return valor;
            }
        }
        public DataTable ConsultaReturnTablasp(string consulta, SqlParameter[] parameters, int baseDatos)
        {
            try
            {
                table = new DataTable();
                Open(baseDatos);
                Query = new SqlCommand();
                Query.Connection = Conexion;
                Query.CommandText = consulta;
                Query.CommandType = CommandType.StoredProcedure;
                if (parameters != null) { Query.Parameters.AddRange(parameters); }

                consultar = new SqlDataAdapter(Query);
                consultar.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                error = ex.Message.ToString().Replace("'", " ");
                return table = new DataTable();
            }
            finally
            {
                Close();
            }
        }
        #endregion

    }
}