﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile.cDatos
{
    public class csPlantasD
    {
        #region Objetos-Constructor
        csConexionD conexion;
        string company = WebConfigurationManager.AppSettings["company"];
        public csPlantasD()
        {
            conexion = new csConexionD();
        }
        #endregion

        #region Metodos
        public DataTable ObtenerPlantbyUserCompany(string UserId, string company)
        {
            try
            {
                string consulta = "select PlantList from erp.UserComp where DcdUserID =  @UserId " +
                    " AND company= @company ";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@UserId",UserId)
                                                  
                                      };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        public DataTable ObtenerPlantasById(string Plant, string company)
        {
            try
            {
                string consulta = "select Plant, Name from erp.Plant " +
                    " Where Plant= @Plant " +
                    " And Company=@company";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@Plant",Plant)

                                      };
                conexion.ConsultaReturnTabla(consulta, param, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }
        public DataTable ObtenerDepositosByPlanta(string company, string Plant)
        {
            try
            {
                string consulta = " SELECT DISTINCT  BinNum " +
                                  "  FROM erp.Part pt " +
                                  " INNER JOIN erp.PartBin pb ON pt.PartNum = pb.PartNum AND pb.Company = pt.Company " +
                                  " inner join erp.PartPlant pp on pp.PartNum = pb.PartNum AND pp.Company = pt.Company " +
                                  " WHERE    pt.Company = @company " +
                                  " AND pb.BinNum in ('DG', 'CB') " +
                                  " AND Plant =  @Plant " +
                                  " GROUP BY  pb.BinNum";
                SqlParameter[] param ={
                                                    new SqlParameter("@company",company),
                                                    new SqlParameter("@Plant",Plant)

                                      };
                conexion.ConsultaReturnTabla(consulta, null, 2);
                return conexion.Table;
            }
            catch (Exception ex)
            {
                return new DataTable();
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}