﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FichasTecnicas.aspx.cs" Inherits="VentasMobile.vCatalogo.FichasTecnicas" %>
<asp:Content ID="FichasTacticas" ContentPlaceHolderID="MainContent" runat="server">  
       <div  id="ModalMensaje" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered " role="document">
            <div id="DivMensaje" class="modal-content">      
            </div>
          </div>
      </div>
<asp:UpdatePanel ID="uxUpdatePanelFormulario" runat="server">
     <ContentTemplate>
         <div id="Inicio" class="container">
             <div class="row">
                <h4 class="mb-4 col-12"><i class="fas fa-clipboard fa-lg mr-2"></i>Fichas Tecnicas</h4>
                <div class="col-6 mt-2">
                    <asp:Label ID="uxLabelPlanta" runat="server" Text="Planta: " Visible ="false" ></asp:Label>
                    <asp:DropDownList ID="uxDropDownListPlantas" class="form-control" runat="server" AutoPostBack="True"  Visible ="false" >
                        </asp:DropDownList>
                </div>
                <asp:Label ID="uxLabelDeposito" Visible="false" runat="server" Text="Deposito: "  ></asp:Label>
                <asp:DropDownList ID="uxDropDownListDeposito" class="form-control" Visible="false" runat="server" AutoPostBack="True" >
                </asp:DropDownList>
                <div class="col-6 mt-2">
                    <asp:Label ID="uxLabelcategoria" runat="server" Text="Categoría: "  Visible ="false"></asp:Label> 
                    <asp:DropDownList ID="uxDropDownListCategoria" class="form-control" runat="server" AutoPostBack="True"  Visible ="false" >
                        </asp:DropDownList>  
                </div>
                  <div class="col-6 mt-2">
                    <asp:Label ID="Label1" runat="server" Text="Departamento: "  Visible ="false"></asp:Label> 
                    <asp:DropDownList ID="uxDropDownListDepartamento" class="form-control" runat="server" AutoPostBack="True"  Visible ="false">
                        </asp:DropDownList>  
                </div>
             </div>
             <div class="row mt-4">
                <div class="col-6 col-xl-4">
                     <asp:TextBox ID="uxTextBoxCodigoProducto" runat="server" class="form-control"  Visible ="false"></asp:TextBox>
                </div>
                <div class="col-6 col-xl-4">
                    <asp:TextBox ID="uxTextBoxCodigoBarras" runat="server" class="form-control"  Visible ="false"></asp:TextBox>
                </div>
                <div class="col-12 col-xl-4">
                     <asp:TextBox ID="uxTextBoxDescripcion"  runat="server" class="form-control"  Visible ="false" ></asp:TextBox>
                </div>
                <div class="col-12 text-center">
                    <asp:Button ID="BuscarProducto" runat="server" Text="Buscar" class="btn btn-primary btn-lg bg-gradient-bb mt-4"  Visible ="false"/>
                </div>
             </div>
             <div class="row mt-4">
                <div class="table table-responsive d-flex justify-content-center text-wrap">
                    <asp:GridView ID="uxGridViewProductos" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="PartNum" class="table table-responsive thead-dark text-wrap"    PageSize="50"   AllowPaging="True" OnPageIndexChanging="uxGridViewProductos_PageIndexChanging" OnRowCommand="uxGridViewProductos_RowCommand" >
                        <Columns>                           
                        <asp:BoundField DataField="PartNum" HeaderText="Clave Producto">
                        <ItemStyle Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProdCode" HeaderText="Cógido Barras">
                        <ItemStyle Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PartDescription" HeaderText="Descripción"  ItemStyle-HorizontalAlign="Center"  ItemStyle-Width="20%">
                        <ControlStyle Width="200px" />
                        <ItemStyle HorizontalAlign="Left" Width="55%"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Clase" HeaderText="Categoría">
                        </asp:BoundField>
                        <asp:BoundField DataField="Disponible" HeaderText="Disponible" >
                        <ItemStyle Width="15%" />
                        </asp:BoundField>
                        <asp:ButtonField HeaderText="PDF" ImageUrl="~/images/icons/pdf.png" CommandName="Seleccionar" ButtonType="Image"/>
                    </Columns>
                    </asp:GridView>
                </div>
             </div>
         </div>
            <asp:LinkButton ID="uxLinkButtonBotonArriba" runat="server"  class="ir-arriba fa-stack">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
            </asp:LinkButton>    
    </ContentTemplate>
      <Triggers>
                    <asp:PostBackTrigger ControlID="uxGridViewProductos" />                  
                    
    </Triggers>
</asp:UpdatePanel>
</asp:Content>

