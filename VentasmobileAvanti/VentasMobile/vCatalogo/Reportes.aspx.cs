﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cNegocio;
using System.Data;

namespace VentasMobile.vCatalogo
{
    public partial class Reportes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                    string company = "";
                    if (Session["CurComp"] != null)
                        company = this.Session["CurComp"].ToString();
                    string categoria = "";
                    if (Session["Categorias"] != null)
                    {
                        categoria = Convert.ToString(Session["Categorias"]);
                        string PlantID = this.Session["PlantID"].ToString();// 02082019
                        csClaseCategoriaN cCategoria = new csClaseCategoriaN();
                        DataTable catego = cCategoria.ObtenerCategoriaID(company, categoria, PlantID);
                        if (catego.Rows.Count > 0)
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = catego.Rows[0]["Description"].ToString();
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                        else
                        {
                            Label uxLabelCategoriaVM = (Label)this.Master.FindControl("uxLabelCategoriaV");
                            uxLabelCategoriaVM.Text = "No se encontro categoría.";
                            Label uxLabelCategoriaL = (Label)this.Master.FindControl("uxLabelCategoriaL");
                            uxLabelCategoriaL.Visible = true;
                        }
                    }


                    if (Session["CurComp"] != null)
                    {
                        csReportesN cReportes = new csReportesN();
                        string usuario = Session["sessionUsr"].ToString();
                        DataTable datosReporte = new DataTable();
                        datosReporte = cReportes.ObtenerReporte(usuario, company, "VREPORTES");

                        if (datosReporte.Rows.Count > 0)
                            iframeReportes.Attributes["src"] = datosReporte.Rows[0]["UrlReporte"].ToString();
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('3', 'informacion', true,false,'es-mx');", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                }
            }


            if (String.IsNullOrEmpty((String)Session["SessionID"]))
            {
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("~/Acceso.aspx");
            }

        }
    }
}