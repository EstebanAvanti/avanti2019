﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using VentasMobile.cNegocio;
using VentasMobile.cEntidad;
using System.Data;
using System.IO;
using System.Web.Configuration;
using System.Drawing;
using System.Web.Services;

namespace VentasMobile.vCatalogo
{
    public partial class FichasTecnicas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string company = this.Session["CurComp"].ToString();
            string PlantID = this.Session["PlantID"].ToString();
            string codigoBarras = "";
            string codigoProducto = "";
            string descripcion = "";
            string depto = "";
            string categoria = "";
           // string planta = this.Session["PlantID"].ToString();// 02082019
            string user = this.Session["DcdUserID"].ToString();
            //string categoria = "";
            //string depto = "";
            string marca = "";
            string linea = "";
            string familia = "";
            string preciolista = "";
            //CargarProductos(PlantID, categoria, codigoBarras, codigoProducto, descripcion, depto);
            dt = ObtenerProductos(true, company, categoria, preciolista, "", "", "", depto, marca, linea, familia, PlantID);
            uxGridViewProductos.DataSource = dt;
            uxGridViewProductos.DataBind();

        }
        private void CargarProductos(string planta, string categoria, string codigoBarras, string codigoProducto, string descripcion, string depto, string almacen)
        {
            try
            {
                DataTable uxTable = new DataTable();
                string usr = Session["DcdUserID"].ToString();
                string company = Session["CurComp"].ToString();
                cNegocio.csProductosN Productos = new cNegocio.csProductosN();

                uxTable = Productos.ObtenerExistenciaProductos(company, planta, categoria, codigoBarras, codigoProducto, descripcion, depto, almacen);
                if (uxTable.Rows.Count > 0)
                {
                    int c = 0;
                    foreach (DataRow rows in uxTable.Rows)
                    {
                        c = c + 1;
                        rows["secuencia"] = c;
                    }
                    uxGridViewProductos.DataSource = uxTable;
                    uxGridViewProductos.DataBind();
                }
                else
                {

                    uxGridViewProductos.DataSource = uxTable;
                    uxGridViewProductos.DataBind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('1', 'informacion', true,false,'es-mx');", true);

                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }

        }
        private DataTable ObtenerProductos(bool filtros, string company, string categoria, string listaPRecio, string codigoBarras, string codigoProductos, string descripcion, string depto, string marca, string linea, string familia, string planta)
        {

            try
            {
                DataTable uxTable = new DataTable();
                csProductosN sRuta = new csProductosN();
                uxTable = sRuta.ObtenerProductos(filtros, company, categoria, listaPRecio, codigoBarras, codigoProductos, descripcion, depto, marca, linea, familia, planta);
                DataColumn newcol = new DataColumn("Cantidad", typeof(int));
                newcol.AllowDBNull = true;
                uxTable.Columns.Add(newcol);
                DataColumn newcol2 = new DataColumn("Foto", typeof(string));
                newcol2.AllowDBNull = true;
                uxTable.Columns.Add(newcol2);

                string rutaImagenes = "", imagenDefault = "";
                if (company == "FBD001")
                    rutaImagenes = WebConfigurationManager.AppSettings["rutaImagenesProductosFBD"];
                else if (company == "BBY002")
                    rutaImagenes = WebConfigurationManager.AppSettings["rutaImagenesProductosBBY"];
                else if (company == "ADE001")
                    rutaImagenes = WebConfigurationManager.AppSettings["rutaImagenesProductosADE"];

                imagenDefault = WebConfigurationManager.AppSettings["rutaImagenProductoDefault"];

                foreach (DataRow row in uxTable.Rows)
                {
                    string ruta = rutaImagenes + company + "-" + row["PartNum"].ToString().Replace("/", "-") + ".jpg";
                    row["Cantidad"] = 1;
                    if (File.Exists(Server.MapPath(ruta)))
                    {
                        row["Foto"] = ruta;
                    }
                    else
                    {
                        row["Foto"] = imagenDefault;
                    }
                }


                return uxTable;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
                return null;
            }
        }

        protected void uxGridViewProductos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                uxGridViewProductos.PageIndex = e.NewPageIndex;
                DataTable dt = new DataTable();
                string company = this.Session["CurComp"].ToString();
                string PlantID = this.Session["PlantID"].ToString();
                string codigoBarras = "";
                string codigoProducto = "";
                string descripcion = "";
                string depto = "";
                string categoria = "";
                // string planta = this.Session["PlantID"].ToString();// 02082019
                string user = this.Session["DcdUserID"].ToString();
                //string categoria = "";
                //string depto = "";
                string marca = "";
                string linea = "";
                string familia = "";
                string preciolista = "";
                //CargarProductos(PlantID, categoria, codigoBarras, codigoProducto, descripcion, depto);
                dt = ObtenerProductos(true, company, categoria, preciolista, "", "", "", depto, marca, linea, familia, PlantID);
                uxGridViewProductos.DataSource = dt;
                uxGridViewProductos.DataBind();                  
                //colorEmpresa(company);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }

        protected void uxGridViewProductos_RowCommand(object sender, GridViewCommandEventArgs e)
        {           
            try
            {
                if (e.CommandName == "Seleccionar")
                {                   
                    string company = this.Session["CurComp"].ToString();
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow row = uxGridViewProductos.Rows[index];
                    //NumCot = row.Cells[1].Text;
                    string rutaPDF =WebConfigurationManager.AppSettings["rutaFichasPDF"] + "Cotización BBY002 24.pdf";
                    if (File.Exists(rutaPDF))
                    {             //OpenFile(rutaPDF);                  

                        System.IO.FileInfo toDownload = new System.IO.FileInfo(rutaPDF);
                        if (File.Exists(rutaPDF))
                        {
                            Response.Clear();
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content - Disposition", "attachment; filename =" + toDownload.Name);
                            Response.AddHeader("Content - Length", toDownload.Length.ToString());
                            Response.WriteFile(rutaPDF);
                            Response.Flush();
                            Response.End();

                            Response.Close();

                        }
                    }


                }
               
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "", "mostrarMensaje('" + ex.Message.ToString().Replace("'", " ") + "', 'error', true,false,'es-mx');", true);
            }
        }
    }
}