﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Inventario.aspx.cs" Inherits="VentasMobile.vCatalogo.Inventario" %>

<asp:Content ID="Inventario" ContentPlaceHolderID="MainContent" runat="server">  
       <div  id="ModalMensaje" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered " role="document">
            <div id="DivMensaje" class="modal-content">      
            </div>
          </div>
      </div>
<asp:UpdatePanel ID="uxUpdatePanelFormulario" runat="server">
     <ContentTemplate>
         <div id="Inicio" class="container">
             <div class="row">
                <h4 class="mb-4 col-12"><i class="fas fa-clipboard fa-lg mr-2"></i>Inventario</h4>
                <div class="col-dropbox mt-2">
                    <asp:Label ID="uxLabelPlanta" runat="server" Text="Planta: "></asp:Label>
                    <asp:DropDownList ID="uxDropDownListPlantas" class="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListPlantas_SelectedIndexChanged">
                        </asp:DropDownList>
                </div>
                <div class="d-none">
                    <asp:Label ID="uxLabelDeposito" Visible="false" runat="server" Text="Deposito: "></asp:Label>
                    <asp:DropDownList ID="uxDropDownListDeposito" class="form-control" Visible="false" runat="server" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListDeposito_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="col-dropbox mt-2">
                    <asp:Label ID="uxLabelcategoria" runat="server" Text="Categoría: "></asp:Label> 
                    <asp:DropDownList ID="uxDropDownListCategoria" class="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListCategoria_SelectedIndexChanged">
                        </asp:DropDownList>  
                </div>
                <div class="col-dropbox mt-2">
                    <asp:Label ID="Label1" runat="server" Text="Departamento: "></asp:Label> 
                    <asp:DropDownList ID="uxDropDownListDepartamento" class="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="uxDropDownListDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>  
                </div>
                <div class="col-dropbox mt-2">
                        <asp:Label ID="uxlabelAlmacen" Visible="true" runat="server" Text="Almacen: "></asp:Label>
                        <asp:DropDownList ID="DropDownListAlmacen" class="form-control"  runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListAlmacen_SelectedIndexChanged">
                        </asp:DropDownList>
                  </div>
             </div>
             <div class="row mt-4">
                <div class="col-dropbox">
                    Código Producto: <asp:TextBox ID="uxTextBoxCodigoProducto" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-dropbox">
                    Código Barras: <asp:TextBox ID="uxTextBoxCodigoBarras" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="col-dropbox">
                    Descripción: <asp:TextBox ID="uxTextBoxDescripcion"  runat="server" class="form-control" ></asp:TextBox>
                </div>
                <div class="col-12 text-center">
                    <asp:Button ID="BuscarProducto" runat="server" Text="Buscar" class="btn btn-primary btn-lg bg-gradient-av mt-4" OnClick="BuscarProducto_Click"/>
                    <asp:Button ID="ButtonLimpiar" runat="server" Text="Limpiar" class="btn btn-primary btn-lg bg-gradient-av mt-4" OnClick="ButtonLimpiar_Click"/>
                </div>
             </div>
             <div class="row mt-4">
                <div class="table table-responsive justify-content-center text-wrap">
                    <asp:GridView ID="uxGridViewProductos" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="PartNum" class="table table-responsive thead-dark text-wrap" 
                    OnPageIndexChanging="uxGridViewProductos_PageIndexChanging"   PageSize="50"   AllowPaging="True" >
                        <Columns>                           
                            <%--<asp:BoundField DataField="secuencia" HeaderText="No" />--%>
                        <asp:BoundField DataField="PartNum" HeaderText="Clave Producto">
                            <ItemStyle Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Disponible" HeaderText="Disponible" >
                            <ItemStyle Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProdCode" HeaderText="Cógido Barras">
                            <ItemStyle Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PartDescription" HeaderText="Descripción"  ItemStyle-HorizontalAlign="Center"  ItemStyle-Width="20%">
                            <ControlStyle Width="200px" />
                            <ItemStyle HorizontalAlign="Left" Width="55%"></ItemStyle>
                        </asp:BoundField>
                            <asp:BoundField DataField="Clase" HeaderText="Categoría">
                        </asp:BoundField>
                    </Columns>
                    </asp:GridView>
                </div>
             </div>
         </div>
            <asp:LinkButton ID="uxLinkButtonBotonArriba" runat="server" OnClick="uxLinkButtonBotonArriba_Click" OnClientClick="irArriba()" class="ir-arriba-av fa-stack">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
            </asp:LinkButton>    
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
