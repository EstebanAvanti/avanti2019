﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VentasMobile.cNegocio;
using System.Data;
using Ice.Core;
using Erp.Common;
using Erp.BO;
using Erp.Adapters;
using Ice.Lib.Framework;
using System.Configuration;
using System.Web.Configuration;

namespace VentasMobile
{
    public partial class Acceso : System.Web.UI.Page
    {

        string Empresa;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                //string userName;
                //userName = User.Identity.Name;
                //greetingLabel.Text = "Welcome " + userName;
                //HttpClientCertificate cert = Request.ClientCertificate;
                //if (cert.IsPresent)
                //    certDataLabel.Text = cert.Get("SUBJECT O");
                //else
                //    certDataLabel.Text = "No certificate was found.";
            }
        }

        public bool ValidaEmpresaUsuario(string Usuario, string Empresa)
        {
            try
            {
                bool resultado = false; string valorEmpresas = "";
                if (Empresa != "")
                { 
                    // consultar si tiene acceso a esa empresa
                    DataTable uxtablaEmp = new DataTable();
                    csEmpresaN emp = new csEmpresaN();

                    uxtablaEmp = emp.ObtenerEmpresa(uxTextBoxUserName.Text);

                    if (uxtablaEmp.Rows.Count > 0)
                    {
                        // hay que checar si tiene acceso a las tres empresas o a dos o a una
                        valorEmpresas = uxtablaEmp.Rows[0]["complist"].ToString();
                        string[] emps = valorEmpresas.Split('~');
                        foreach (string empr in emps)

                            if (Empresa == empr)
                            {
                                resultado = true;
                            }
                    }

                    string direcConfig = WebConfigurationManager.AppSettings["direcionConfig"];                    
                    Session obj = new Session(Usuario, uxTextBoxPassword.Text, Ice.Core.Session.LicenseType.Default, @""+ direcConfig); 
                    if (obj != null)
                    {
                        obj.CompanyID = Empresa;//---->  cambio de empresa
                        obj.Dispose();
                        Session["sessionUsr"] = Usuario;
                        Session["sessionPass"] = uxTextBoxPassword.Text;
                        Session["DcdUserID"] = obj.UserID;
                        Session["Name"] = obj.UserName;
                        Session["CurComp"] = obj.CompanyID;
                        Session["SessionID"] = obj.SessionID;
                        Session["PlantID"] = obj.PlantID;
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                        mesajeError.Attributes["style"] = "display:block;";
                        uxLabelErrorLogin.Text = "Usuario sin acceso o los datos son incorrectos";
                    }

                }
                else
                {

                    mesajeError.Attributes["style"] = "display:block;";
                    uxLabelErrorLogin.Text = "Seleccione una empresa";
                }
                return resultado;
            }
            catch (Exception error)
            {

                mesajeError.Attributes["style"] = "display:block;";
                uxLabelErrorLogin.Text = error.Message.ToString();
                return false;
            }
        }

        protected void uxImageButtonAvanti_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Empresa = "ADE001";
                if (ValidaEmpresaUsuario(uxTextBoxUserName.Text, Empresa) == true)
                {

                    this.Page.Response.Redirect(@"vOperacion\Inicio.aspx");
                }
            }
            catch (Exception error)
            {
                uxLabelErrorLogin.Text = error.Message.ToString();
            }

        }

        protected void uxImageButtonFrancobolli_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                Empresa = "FBD001";
                if (ValidaEmpresaUsuario(uxTextBoxUserName.Text, Empresa) == true)
                {
                    this.Page.Response.Redirect(@"vOperacion\Inicio.aspx");
                }
            }
            catch (Exception error)
            {
                uxLabelErrorLogin.Text = error.Message.ToString();
            }
        }

        protected void uxImageButtonBananabay_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                Empresa = "BBY002";
                if (ValidaEmpresaUsuario(uxTextBoxUserName.Text, Empresa) == true)
                {
                    this.Page.Response.Redirect(@"vOperacion\Inicio.aspx");
                }
            }
            catch (Exception error)
            {

                mesajeError.Attributes["style"] = "display:block;";
                uxLabelErrorLogin.Text = error.Message.ToString();
            }
        }
        
    }
}